
function notyError(message) {
    var options = { "text": message, "layout": "topLeft", "type": "error","timeout" : 5000 };
    noty(options);
}

function notySuccess(message) {
    var options = { "text": message, "layout": "topLeft", "type": "success","timeout" : 5000 };
    noty(options);
}
