;
/**
 * Codeigniter Form Validation via Jquery. 
 *
 * For all the functions for validations
 *  - TRUE  = success
 *  - FALSE = error and stop
 *
 * @author A H Abid
 */
// -------------------------------------------------------------------

// Show Errors
function CI_form_show_error(DOMobject, message) {
    DOMobject.siblings('.help-block').text(message);
    DOMobject.closest('.form-group').addClass('has-error');
}

// Hide Errors
function CI_form_hide_error(DOMobject) {
    DOMobject.siblings('.help-block').text('');
    DOMobject.closest('.form-group').removeClass('has-error');
}

// -------------------------------------------------------------------

// Trim
function CI_form_validate_trim(value) {
    return value.trim();
}


// Required
function CI_form_validate_required(DOMobject) {
     var message = (DOMobject.attr('data-ci-validation-message-required'))
                    ? DOMobject.attr('data-ci-validation-message-required') : "Please fill up this field.";
    if(DOMobject.val() != ''){
        CI_form_hide_error(DOMobject)
        return true;
    } else {
        CI_form_show_error(DOMobject, message);
        return false;
    }
}

// Min length
function CI_form_validate_min_length(DOMobject, minlength) {
    minlength = parseInt(minlength);
    if(!$.isNumeric(minlength)) return false;
    var message = (DOMobject.attr('data-ci-validation-message-min-length'))
                ? DOMobject.attr('data-ci-validation-message-min-length') : "Must have minimum length of "+minlength+".";
    if(DOMobject.val().length >= minlength){
        CI_form_hide_error(DOMobject)
        return true;
    } else {
        CI_form_show_error(DOMobject, message);
        return false;
    }
}

// Max length
function CI_form_validate_max_length(DOMobject, maxlength) {
    maxlength = parseInt(maxlength);
    if(!$.isNumeric(maxlength)) return false;
    var message = (DOMobject.attr('data-ci-validation-message-min-length'))
                ? DOMobject.attr('data-ci-validation-message-min-length') : "Length must be lower than "+maxlength+".";
    if(DOMobject.val().length <= maxlength){
        CI_form_hide_error(DOMobject)
        return true;
    } else {
        CI_form_show_error(DOMobject, message);
        return false;
    }
}


// Valid Username
function CI_form_validate_valid_username(DOMobject, maxlength) {
    var message = (DOMobject.attr('data-ci-validation-message-valid-username'))
                ? DOMobject.attr('data-ci-validation-message-valid-username') : "Username is not valid";
    var pattern = new RegExp(/^[a-zA-Z][a-zA-Z0-9_]+$/);

    if(pattern.exec(DOMobject.val()) != null) {
        CI_form_hide_error(DOMobject)
        return true;
    } else {
        CI_form_show_error(DOMobject, message);
        return false;
    }
}

// Is Unique (AJAX Call)
function CI_form_validate_is_unique(DOMobject, params) {

    var Url = SiteURL + 'ajax_form_validation/is_unique';
    var PrimaryIDvalue = (DOMobject.closest('form').find('input[class="primary-id-value"]') != undefined)
                        ? DOMobject.closest('form').find('input[class="primary-id-value"]').val() : '';
  
    data = getCSRFtoken();
    data["value"] = DOMobject.val();
    data["params"] = params;
    data["primary_id"] = PrimaryIDvalue;

    $.ajax({
        type: 'post',
        url: Url,
        data: data,
        dataType: 'json',
        success: function(resp){
            console.log(resp);
            if(resp.status == true) {
                CI_form_hide_error(DOMobject)
                return true;
            } else {
                CI_form_show_error(DOMobject, resp.message);
                return false;
            }
        },
        error: function(xhr, type, msg){
            onAjaxError(xhr, type, msg);
        },
        complete: function(xhr, ts) {
            onAjaxCompleteCSRF(xhr, ts);
        }
    });
}


// =======================================================================================

$(document).ready(function(){

    var InputNotSelectors = ':not([type="checkbox"]):not([type="radio"])';
    $(document).off('blur','input[data-ci-validation]'+ InputNotSelectors);
    $(document).on('blur','input[data-ci-validation]' + InputNotSelectors,function(e){

        var InputObject         = $(this);
        var InputValue          = $(this).val();
        var Rules               = $(this).attr('data-ci-validation').split('|');
        var FunctionNamePrefix  = 'CI_form_validate_';
        if(Rules == '' || Rules.length == 0) return false;
        var pattern = new RegExp(/^(.*?)\[(.*)\]$/);

        var NonValidRules = ['trim'];

        // Loop through Rules and execute functions if exists
        $.each(Rules, function(i,rule) {

            var param = null;

            // Extract current rule for parameters if there any
            var match = pattern.exec(rule);
            if(match) {
                rule = match[1];
                param = match[2];
            }

            // Execute non validation rules for input value setter
            var function_name = FunctionNamePrefix + rule;
            if($.inArray(rule,NonValidRules) > -1) {
                if(typeof window[function_name] == 'function') {
                    InputValue = window[function_name](InputValue, param);
                    InputObject.val(InputValue);
                }
            } else {
                if(typeof window[function_name] == 'function') {
                    // Break if returns false/fails;
                    if(window[function_name](InputObject, param) === false) return false;
                } else {
                    console.log(function_name + ' not found.');
                }
            }

        }); // End $.each

    }); // End Input types validation

}); // End Doc Ready
