;

window['csrf_cookie'] = '';

function onAjaxCompleteCSRF(xhr, ts) {
  
	if(xhr.responseJSON !== undefined  && xhr.responseJSON[window.CSRFCookieName] !== undefined) {
		setCSRFcookie(xhr.responseJSON[window.CSRFCookieName]);
	} else if (xhr[window.CSRFCookieName] !== undefined) {
		setCSRFcookie(xhr[window.CSRFCookieName]);
	}
}

function setCSRFcookie(cookie_value) {
	window.csrf_cookie = cookie_value;
	
	if($('meta[name="csrf_cookie"]').length)
	{
		$('meta[name="csrf_cookie"]').attr('content',cookie_value);
	}

	if($('input[type=hidden][name='+window["CSRFTokenName"]+']').length)
	{
		$('input[type=hidden][name='+window["CSRFTokenName"]+']').val(cookie_value)	;
	}
}

function getCSRFtoken() {
	var json = {};
	json[window.CSRFTokenName] = window.csrf_cookie;
	return json;
}

function getCSRFcookie() {
	if($('meta[name="csrf_cookie"]').length)
	{
		return $('meta[name="csrf_cookie"]').attr('content');
	}
	else
	{
		return $.cookie(window.CSRFCookieName);
	}
}

// ---------------------------------------------------------------------------------------------------

$(document).ready(function(){

	window["CSRFTokenName"] = $('meta[name="csrf_token_name"]').attr('content');
	window["CSRFCookieName"] = $('meta[name="csrf_cookie_name"]').attr('content');
	
	if(typeof window.CSRFTokenName === 'undefined' || typeof window.CSRFCookieName === 'undefined') {
		console.log('CSRFCookieName || CSRFTokenName not defined. jQuery.cookie will not work.');
		return false;
	}

	if(typeof $.cookie !== undefined) {
		window.csrf_cookie = getCSRFcookie();
	} else {
		console.log('jQuery.cookie is not found. Cannot set CSRF Cookie');
	}

}); // End Doc Ready
