// ==========================================================================================================
// FUNCTIONS FOR GENERAL STUFFS
// ==========================================================================================================


/**
 * String replace
 * 
 * @param  {[type]} search  [description]
 * @param  {[type]} replace [description]
 * @param  {[type]} subject [description]
 * @param  {[type]} count   [description]
 * @return {[type]}         [description]
 */
function str_replace(search, replace, subject, count) {
  //  discuss at: http://phpjs.org/functions/str_replace/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Gabriel Paderni
  // improved by: Philip Peterson
  // improved by: Simon Willison (http://simonwillison.net)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // bugfixed by: Anton Ongson
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Oleg Eremeev
  //    input by: Onno Marsman
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: Oleg Eremeev
  //        note: The count parameter must be passed as a string in order
  //        note: to find a global variable in which the result will be given
  //   example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
  //   returns 1: 'Kevin.van.Zonneveld'
  //   example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
  //   returns 2: 'hemmo, mars'

  var i = 0,
    j = 0,
    temp = '',
    repl = '',
    sl = 0,
    fl = 0,
    f = [].concat(search),
    r = [].concat(replace),
    s = subject,
    ra = Object.prototype.toString.call(r) === '[object Array]',
    sa = Object.prototype.toString.call(s) === '[object Array]';
  s = [].concat(s);
  if (count) {
    this.window[count] = 0;
  }

  for (i = 0, sl = s.length; i < sl; i++) {
    if (s[i] === '') {
      continue;
    }
    for (j = 0, fl = f.length; j < fl; j++) {
      temp = s[i] + '';
      repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
      s[i] = (temp)
        .split(f[j])
        .join(repl);
      if (count && s[i] !== temp) {
        this.window[count] += (temp.length - s[i].length) / f[j].length;
      }
    }
  }
  return sa ? s : s[0];
}

/**
 * Returns true if the user hit Esc or navigated away from the
 * current page before an AJAX call was done. (The response
 * headers will be null or empty, depending on the browser.)
 *
 * NOTE: this function is only meaningful when called from
 * inside an AJAX "error" callback!
 *
 * The 'xhr' param is an XMLHttpRequest instance.
 *
 * Source:
 * http://ilikestuffblog.com/2009/11/30/how-to-distinguish-a-user-aborted-ajax-call-from-an-error/
 * 
 */
function onAjaxUserAborted(xhr) {
	return xhr.getAllResponseHeaders();
}

/**
 * Error messages if ajax fails
 * 
 * @param  {object} xhr XHRObject
 * @param  {string} txt Error Type
 * @param  {string} m   Error Message
 * @return {void}
 * @author A. H. Abid <a_h_abid@hotmail.com>
 */
function onAjaxError(xhr,txt,m) {

	var msg = '';

	if (xhr.status == 0) {
		msg = 'You are offline!!\n Please Check Your Network.';
	} else if (xhr.status == 404) {
		msg = 'Requested URL not found.';
	} else if (xhr.status == 500) {
		msg = 'Internal Server Error.';
	} else if (txt == 'parsererror') {
		msg = 'Error. Parsing JSON Request failed.';
	} else if (txt == 'timeout') {
		msg = 'Request Time out.';
	} else {
		msg = 'Unknown Error.\n' + xhr.responseText;
	}
	return (msg);
}

/**
 * Delay function for keypress delay for AJAX
 *
 * Usable with Jquery
 *
 * Example: 
 * $('input').keyup(function() {
 *  keyDelay(function(){
 *    alert('Time elapsed!');
 *  }, 1000 );
 * });
 * 
 * @param  {object} xhr XHRObject
 * @param  {string} txt Error Type
 * @param  {string} m   Error Message
 * @return {void}
 * @author A. H. Abid <a_h_abid@hotmail.com>
 */
var keyDelay = (function(){
	var timer = 0;
	return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	};
})();


/**
 * Convert numbers to bangla digits
 *
 * @param {numeric} [number]
 * @return {string}
 */
function convert_to_bangla_number(number) {
	var bn_arr = ['০','১','২','৩','৪','৫','৬','৭','৮','৯'];
	var en_arr = ['0','1','2','3','4','5','6','7','8','9'];

	return str_replace(en_arr, bn_arr, number);
}

/**
 * Submit a get request with display limit no. 
 * 
 * @param  string number
 * @author  A. H. Abid <a_h_abid@hotmail.com>
 */
function form_submit_display_limit(obj) {

	var number = parseInt(obj.value);

	if (typeof number == 'NaN')
	{
		console.log('Selected value is not a number. :: ', number);
		return false;
	}

	if (number < 1)
	{
		console.log('Selected value cannot be less than 1. :: ', number);
		return false;
	}

	var attrib = obj.getAttribute('name'); 
	var query;

	if (window.location.search == '') {
		query = ['?',attrib,'=',number].join('');
	} else {
		query = ['&',attrib,'=',number].join('');
	}

	var newURL = [window.location.href,query].join('');

	window.location.href = newURL;
}

/**
 * Submit a get request with display limit no. 
 * 
 * @param	string path -- url path
 * @param	json params -- data to send with
 * @param	string method -- [post,get]
 * @author	Rakesh Pai
 * @author	mikemaccana
 * @link	http://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit
 */
function submitAsPost(path, params, method) {
	method = method || "post"; // Set method to post by default if not specified.

	// The rest of this code assumes you are not using a library.
	// It can be made less wordy if you use one.
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);

	for(var key in params) {
		if(params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		}
	}

	// Add CSRF Token
	if (window.csrf_cookie != '')
	{
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", window.CSRFTokenName);
		hiddenField.setAttribute("value", window.csrf_cookie);

		form.appendChild(hiddenField)
	}

	document.body.appendChild(form);
	form.submit();
}
