$(document).ready(function(){
	/**
	 * iCloner
	 *
	 * A simple system to duplicate a li and easily remove it,
	 * @author A.H.Abid
	 */
	if($('.icloner-add').length)
	{
		$(document).off('click','.icloner-add');
		$(document).on('click','.icloner-add',function(e){
			e.preventDefault();
			
			var thisElm			= $(this);
			var targetID		= thisElm.attr('data-icloner-target');
			var targetObj		= $('#'+targetID);
			var targetElement   = targetObj.get(0).tagName;

			var addAfterCallback = targetObj.attr('data-icloner-add-after-callback');

			var removeButton = '<button class="btn btn-danger btn-xs icloner-remove" data-icloner-target="'+targetID+'">-</button>';

			switch (targetElement) {

				case 'TABLE' :
					var copy = targetObj.find('tbody tr:first-child').html()
					                    .replace('<td></td>','<td>'+removeButton+'</td>')
					                    .replace('checked="checked"','')
					                    ;
					targetObj.find('tbody').append('<tr>' + copy + '</tr>');
					break;

				default : 
					console.log('This element not supported. ',targetElement);
					return false;
					break;
			}

			if(addAfterCallback !== undefined) {
				if(typeof window[addAfterCallback] == 'function') {
					window[addAfterCallback](targetObj);
                }
			}

		}); // .icloner-add

		$(document).off('click','.icloner-remove');
		$(document).on('click','.icloner-remove',function(e){
			e.preventDefault();

			var thisElm			= $(this);
			var targetID		= thisElm.attr('data-icloner-target');
			var targetObj		= $('#'+targetID);
			var targetElement   = targetObj.get(0).tagName;			
			var removeAfterCallback = targetObj.attr('data-icloner-remove-after-callback');

			switch (targetElement) {

				case 'TABLE' :
					thisElm.closest('tr').remove();
					break;

				default : 
					console.log('This element not supported. ',targetElement);
					return false;
					break;
			}

			if(removeAfterCallback !== undefined) {
				if(typeof window[removeAfterCallback] == 'function') {
					window[removeAfterCallback](targetObj);
                }
			}

		}); // .icloner-remove
		
	}// End iCloner
});
