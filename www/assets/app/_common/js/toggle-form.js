;
/**
 * Form Toggle JS
 *
 * To toggle between form and view mode
 *
 * @author  A. H. Abid <a_h_abid@hotmail.com>
 * @date 2014-Jun-03
 */

$(document).ready(function(){


	// OnClicking to show View mode
	$(document).off('click','.toggle-form-go-view');
	$(document).on('click','.toggle-form-go-view',function(e){

		e.preventDefault();
		var targetBoxID = $(this).attr('data-toggle-form-target'),
			targetBox	= $(['#',targetBoxID].join(''));

		targetBox.removeClass('toggle-form-edit-mode').addClass('toggle-form-view-mode');

	});


	// OnClicking to show Edit mode
	$(document).off('click','.toggle-form-go-edit');
	$(document).on('click','.toggle-form-go-edit',function(e){

		e.preventDefault();
		var thisEl		= $(this),
			targetBoxID = thisEl.attr('data-toggle-form-target'),
			targetBox	= $(['#',targetBoxID].join(''));

		targetBox.removeClass('toggle-form-view-mode').addClass('toggle-form-edit-mode');

	});


	// OnClicking Save
	$(document).off('click','.toggle-form-go-save');
	$(document).on('click','.toggle-form-go-save',function(e){

		e.preventDefault();
		var thisEl		= $(this),
			thidElIcon	= $(this).find('i.fa');
			thisElForm	= thisEl.closest('form'),
			targetBoxID = thisEl.attr('data-toggle-form-target'),
			targetBox	= $(['#',targetBoxID].join('')),
			errorBlock	= targetBox.find('.error-block'),
			remoteURL	= thisElForm.attr('action');

		if (remoteURL == undefined) {
			console.log('No Form or Form Action Attibute Not Found.');
			return false;
		}

		var formData = thisElForm.serialize();

		// Clear Error Text Before Submit
		errorBlock.text('');

		// Show Ajax Loader
		thidElIcon.removeClass('fa-save').addClass('fa-spinner').addClass('fa-spin');

		$.ajax({
			type : 'post',
			url : remoteURL,
			data: formData,
			dataType: 'json'
		}).done(function(resp){
			console.log(resp);
			if (resp.status == true) {
				targetBox.find('.toggle-form-view .toggle-form-view-text').html(resp.data.html);
				targetBox.find('.toggle-form-go-view').click();
			} else {
				errorBlock.text( resp.message );
			}
		}).fail(function(x,t,m){
			errorBlock.text(onAjaxError(x,t,m));
		}).always(function(xhr, ts){
			thidElIcon.addClass('fa-save').removeClass('fa-spinner').removeClass('fa-spin');
			onAjaxCompleteCSRF(xhr, ts);
		});


	});


}); // End DocReady
