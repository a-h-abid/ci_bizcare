$(document).ready(function(){
    $('.event-conf-next').show();
    $('.event-conf-prev').hide();

    $('.event-conf-next').on('click', function(e){
        e.preventDefault();

        $('.conf-csr-imgs').first().removeClass('active');
        $('.conf-csr-imgs').last().addClass('active');

        $('.event-conf-next').hide();
        $('.event-conf-prev').show();
    });

    $('.event-conf-prev').on('click', function(e){
        e.preventDefault();

        $('.conf-csr-imgs').last().removeClass('active');
        $('.conf-csr-imgs').first().addClass('active');

        $('.event-conf-next').show();
        $('.event-conf-prev').hide();
    });
});