<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Get avatar by user_id. If not available will use a general one.
 *
 * @param  int		$user_id
 * @param  string	$user_type
 * @return string
 */
function get_avatar($user_id, $user_type)
{
	if(file_exists(get_avatar_path($user_id)))
	{
		return get_avatar_url($user_id).'?t='.filemtime(get_avatar_path($user_id));
	}

	// If not then get the default one
	return base_url('assets/app/public/img/avatars/no-avatar-'.$user_type.'.png');
}

/**
 * Get the avatar file path
 * 
 * @param  int $id
 * @return string
 */
function get_avatar_path($id)
{
	return FCPATH.'avatars'.DS.$id.'.png';
}

/**
 * Get the avatar url path
 * @param  int $id
 * @return string
 */
function get_avatar_url($id)
{
	return base_url('avatars/'.$id.'.png');
}

/* End of avatar_helper.php */
/* Location: ./application/helpers/avatar_helper.php */