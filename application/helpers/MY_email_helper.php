<?php if(!defined('BASEPATH')) die('Sorry Mate, Cannot show you this.');

/*--------------------------------------------------------------
| Email Helper
-----------------------------------------------------------------
|
*/


/**
 * Get the email with the current domain name
 * 
 * @param string $name
 * @return  string
 */
function email_with_domain($name = '')
{
	$domain = str_replace(array('www.','api.'),'',$_SERVER['SERVER_NAME']);
	return $name.'@'.$domain;
}


/* End of file MY_email_helper.php */
/* Location: ./application/helpers/MY_email_helper.php */