<?php if(!defined('BASEPATH')) die('Sorry Mate, Cannot show you this.');

/*--------------------------------------------------------------
| MY LANGUAGE HELPER
-----------------------------------------------------------------
|
*/


/**
 * Translate a line for given language
 * 
 * @param string $name
 * @param string $idiom
 * @param bool $log_errors
 * @return  string
 */
function translate($line, $idiom = '', $log_errors = TRUE)
{
	$CI =& get_instance();

	return $CI->lang->translate($line, $idiom, $log_errors);
}

/**
 * Translate Numeric Value to Current or Given Language
 * 
 * @param string $name
 * @param string $idiom
 * @param bool $log_errors
 * @return  string
 */
function translate_numeric($number, $idiom = '', $log_errors = FALSE)
{
	$CI =& get_instance();

	$translated_number = '';
	$number = (string) $number;

	for ($i=0, $c = strlen($number); $i < $c; $i++)
	{ 
		if (is_numeric($number[$i]))
			$translated_number .= $CI->lang->translate('numeric.'.$number[$i], $idiom, $log_errors);
		else
			$translated_number .= $number[$i];
	}

	return $translated_number;
}

/* End of file MY_email_helper.php */
/* Location: ./application/helpers/MY_email_helper.php */