<?php if(!defined('BASEPATH')) die('Shinnee');

/**
 * Get bootstrap styled pagination
 *
 * @param  string $base_url
 * @param  int $total_rows
 * @param  int $limit
 * @return string
 * @author  A. H. Abid <a_h_abid@hotmail.com>
 */
function pagination_bootstrap($base_url, $total_rows, $limit)
{
	$CI =& get_instance();

	$CI->load->library('pagination');

	$paginaton_config = array();
	$paginaton_config['base_url']           = site_url($base_url);
	$paginaton_config['total_rows']         = $total_rows;
	$paginaton_config['per_page']           = $limit; 
	$paginaton_config['num_links']          = 5;
	$paginaton_config['use_page_numbers']   = TRUE; 
	$paginaton_config['page_query_string']  = TRUE;
	$paginaton_config['reuse_query_string'] = TRUE;
	$paginaton_config['query_string_segment'] = 'page';
	
	$paginaton_config['full_tag_open']   = '<ul class="pagination">';
	$paginaton_config['full_tag_close']  = '</ul>';
	$paginaton_config['first_tag_open']  = '<li>';
	$paginaton_config['first_tag_close'] = '</li>';
	$paginaton_config['last_tag_open']   = '<li>';
	$paginaton_config['last_tag_close']  = '</li>';
	$paginaton_config['next_tag_open']   = '<li>';
	$paginaton_config['next_tag_close']  = '</li>';
	$paginaton_config['prev_tag_open']   = '<li>';
	$paginaton_config['prev_tag_close']  = '</li>';
	$paginaton_config['cur_tag_open']    = '<li class="active"><a href="#">';
	$paginaton_config['cur_tag_close']   = '</a></li>';
	$paginaton_config['num_tag_open']    = '<li>';
	$paginaton_config['num_tag_close']   = '</li>';
	
	$CI->pagination->initialize($paginaton_config);
	return $CI->pagination->create_links();
}

/* End of file pagination_helper.php */
/* Location: ./application/helpers/pagination_helper.php */