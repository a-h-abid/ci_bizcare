<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|------------------------------------------------------------------------
| MARKDOWN HELPER
|------------------------------------------------------------------------
| For making quick markdown calls
|
*/


/**
 * Parses (base_url:) syntax to server url
 * 
 * @param  string $text [description]
 * @return string       [description]
 * @author A. H. Abid <a_h_abid@hotmail.com>
 */
function md_base_url($text)
{
	return preg_replace('/\(base_url:(.+)\)/', '('.base_url("$1").')', $text);
}

/**
 * Parses (site_url:) syntax to server url
 * 
 * @param  string $text [description]
 * @return string       [description]
 * @author A. H. Abid <a_h_abid@hotmail.com>
 */
function md_site_url($text)
{
	return preg_replace('/\(site_url:(.+)\)/', '('.site_url("$1").')', $text);
}


/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */