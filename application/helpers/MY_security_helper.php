<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|------------------------------------------------------------------------
| MY SECURITY HELPER
|------------------------------------------------------------------------
| SECURITY helper extending the core functionality.
|
*/


/**
 * Encrypt String using PHPass
 * 
 * @param string $string
 * @return string
 */
function encrypt_string_phpass($string)
{
	$CI =& get_instance();
	$CI->load->library('phpass');
	
	return $CI->phpass->hash($string);
}

/**
 * Encrypt String using CI Encyption
 * 
 * @param string $string
 * @param string $key
 * @param boolean $url
 * @return string
 */
function encrypt_string_ci($string, $key = '', $url = FALSE)
{
	$CI =& get_instance();
	$CI->load->library('encrypt');
	
	$encoded = ($url === TRUE) ? $CI->encrypt->encode_into_url($string, $key) : $CI->encrypt->encode($string, $key);
	return $encoded;
}

/**
 * Decrypt String using CI Encyption
 * 
 * @param string $string
 * @param string $key
 * @param boolean $url
 * @return string
 */
function decrypt_string_ci($string, $key = '', $url = FALSE)
{
	$CI =& get_instance();
	$CI->load->library('encrypt');
	
	$decoded = ($url === TRUE) ? $CI->encrypt->decode_from_url($string, $key) : $CI->encrypt->decode($string, $key);
	return $decoded;
}

/* End of file MY_security_helper.php */
/* Location: ./application/helpers/MY_security_helper.php */