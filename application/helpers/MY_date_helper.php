<?php if (!defined('BASEPATH')) die('Die');

/*
|------------------------------------------------------------------------
| MY DATE HELPER
|------------------------------------------------------------------------
|
|
*/

/**
 * Get MySQL Date
 * 
 * @param  integer $timestamp - Timestamp
 * @return string
 */
function mysql_date($timestamp = 0)
{
	if ($timestamp < 1) $timestamp = time();

	return date('Y-m-d',$timestamp);
}

/**
 * Get MySQL Time
 * 
 * @param  integer $timestamp - Timestamp
 * @return string
 */
function mysql_time($timestamp = 0)
{
	if ($timestamp < 1) $timestamp = time();

	return date('H:i:s',$timestamp);
}

/**
 * Get MySQL DateTime
 * 
 * @param  integer $timestamp - Timestamp
 * @return string
 */
function mysql_datetime($timestamp = 0)
{
	if ($timestamp < 1) $timestamp = time();

	return date('Y-m-d H:i:s',$timestamp);
}


/* End of file MY_date_helper.php */
/* Location: ./application/helpers/MY_date_helper.php */