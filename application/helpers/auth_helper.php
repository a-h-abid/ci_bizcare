<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Check if logged user has permission
 * 
 * @param  string  $key
 * @return boolean
 */
function has_permission($key)
{
	$CI =& get_instance();

	$auth_key = $CI->get_auth_key();
	if ($auth_key == '') return FALSE;

	// For Master Admin, All Permissions are TRUE
	if ( isset($CI->data[$auth_key]['usergroups']['master-admin']) )
	{
		return TRUE;
	}

	return (isset($CI->data[$auth_key]['permissions'][$key]));
}

/**
 * Check user loggedin
 * 
 * @return boolean
 */
function is_loggedin()
{
	$CI =& get_instance();

	$auth_key = $CI->get_auth_key();
	if($auth_key == '') return FALSE;

	return $CI->session->has_userdata($auth_key);
}


/**
 * Get Current User Type
 * 
 * @return string
 */
function current_user_type()
{
	if (!is_loggedin())
		return NULL;

	$CI =& get_instance();

	if( !isset($CI->data[$CI->get_auth_key()]['user_type']) )
		return NULL;

	return $CI->data[$CI->get_auth_key()]['user_type'];
}

/**
 * Check current user type matches with provided one
 * 
 * @param  string  $name
 * @return boolean
 */
function is_user_type($name)
{
	return current_user_type() === $name;
}


/* End of auth_helper.php */
/* Location: ./application/helpers/auth_helper.php */