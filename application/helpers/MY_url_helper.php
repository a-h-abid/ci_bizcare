<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Parse {site_url:} tags with site_url()
 * 
 * @param  string $text
 * @return string
 */
function parse_siteurl($text)
{
	return preg_replace('/{site_url:(.+)}/', site_url("$1"), $text);
}

/**
 * Parse {base_url:} tags with base_url()
 * 
 * @param  string $text
 * @return string
 */
function parse_baseurl($text)
{
	return preg_replace('/{base_url:(.+)}/', base_url("$1"), $text);
}

/**
 * Parse {route_to:route-name::param:1,param:2} tags with route_to()
 * 
 * @param  string $text
 * @return string
 */
function parse_routeurl($text)
{
	if (preg_match('/{route_to:(.+)::(.+)?}/', $text, $matches))
	{
		if (!isset($matches[2])) $matches[2] = array();
		$data = array();
		$params = explode(',', $matches[2]);
		foreach ($params as $param)
		{
			$param_split = explode(':', $param);
			if ( isset($param_split[0]) && isset($param_split[1]) )
			{
				$data[$param_split[0]] = $param_split[1];
			}
		}
		$text = preg_replace('/{route_to:(.+)::(.+)?}/', route_to($matches[1], $data), $text);
	}

	return $text;
}

/**
 * Parse {site_url:},{base_url:}.{route_to:} tags
 * 
 * @param  string $text
 * @return string
 */
function parse_rel_url($text)
{
	return parse_baseurl(parse_siteurl(parse_routeurl($text)));
}

/**
 * Get URL with Lang Code
 * 
 * @param  string $uri
 * @param  string $lang_code
 * @param  string $protocol
 * @return string
 */
function lang_url($uri = '', $lang_code = '', $protocol = NULL)
{
	$CI =& get_instance();

	$uri = $CI->lang->get_lang_uri($uri, $lang_code);

	return site_url($uri, $protocol);
}

/**
 * Get URL with Non Lang Code
 * 
 * @param  string $uri
 * @param  string $lang_code
 * @param  string $protocol
 * @return string
 */
function non_lang_url($uri = '', $lang_code = '', $protocol = NULL)
{
	$CI =& get_instance();

	$uri = $CI->lang->get_non_lang_uri($uri);

	return site_url($uri, $protocol);
}


/**
 * Checks if page is rerouted or not.
 * 
 * @return boolean
 * @author A H Abid
 */
function is_rerouted()
{
	$CI =& get_instance();
	return ($CI->uri->uri_string() !== $CI->uri->ruri_string());
}

/**
 * Get URL link by the named route. Optional provide array data
 * 
 * @param string $name
 * @param string|array $data
 * @return string
 */
function route_to($name, $data = array())
{
	$CI =& get_instance();
	return site_url( $CI->router->reverse_route($name, $data) );
}

/**
 * Get the current route name
 * 
 * @return string
 */
function route_name()
{
	return get_instance()->router->route_name();
}

/**
 * Check if remote URL Exist or not 
 * 
 * @param string $url
 * @return boolean
 * @author dangkhoaweb
 * @link http://stackoverflow.com/questions/1363925/check-whether-image-exists-on-remote-url
 */
function check_remote_url($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);

	// don't download content
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	return (curl_exec($ch) !== FALSE);
}

/**
 * Parse the link params to set provided parameters
 *
 * Example:
 * URL: http://sample.com/user/{:id}/{:username}
 * To: http://sample.com/user/1/someuser1
 * 
 * @param  string $link   The URL
 * @param  array $params Parameters to be set with the URL
 * @return string         Final URL
 */
function parse_link_params($link, $params)
{
	preg_match('/{:([a-zA-Z0-9_]+)}/i', $link, $matches);
	if(count($matches) < 2)
		return $link;

	if(is_object($params))
		$params = (array) $params;

	unset($matches[0]);
	foreach($matches as $param)
	{
		if(isset($params[$param]))
			$link = str_replace('{:'.$param.'}', $params[$param], $link);
	}

	return $link;
}

/**
 * Is the named route current url
 * 
 * @param string $route_name 
 * @param string $return_string
 * @return string|false
 */
function is_route_current($route_name, $return_string)
{
	$bool = (current_url() == route_to($route_name));
	return ($bool == TRUE) ? $return_string : '';
}

/**
 * Make lowercased Slug
 * 
 * @param  string $string
 * @return string        
 */
function slug($string)
{
	return url_title($string,'-',TRUE);
}

/**
 * Generate Unique Slug for DB
 * 
 * @param  string $string     string to slug
 * @param  string $table      table name in your database
 * @param  string $field      field name of the table
 * @param  array  $primary_keys - defined as "where" array. Ex. array('id' => 3) or array('id !=' => 3) 
 * @return string             returns the unique slug
 */
function unique_slug($string, $table, $field, $primary_keys = array())
{
	// Make it to slug 1st
	$slug = slug($string);

	// Now Check for slug unique in db table
	$number = 0;
	$CI =& get_instance();

	$is_unique = FALSE;

	while (!$is_unique )
	{
		$CI->db->select('COUNT(*) as count');
		$CI->db->from($table);
		$CI->db->where($field, $slug);

		$primary_keys = array_filter($primary_keys);
		if(count($primary_keys) > 0)
		{
			$CI->db->where($primary_keys);
		}
		
		$result = $CI->db->get()->row()->count;
		
		// If count found, means slug exists
		if($result > 0)
		{
			if(!preg_match('/\d+$/', $slug))
			{
				$slug = $slug.'-'.$number;
			}
			else
			{
				$slug = $slug.'-'.preg_replace('/\d+$/', $number++, $slug);
			}
		}
		else
		{
			$is_unique = TRUE;
		}
	}

	return $slug;
}

/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */