<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|------------------------------------------------------------------------
| MY ARRAY HELPER
|------------------------------------------------------------------------
| ARRAY helper extending the core functionality.
|
*/

/**
 * Merge between 2 arrays & returns only matched keys
 * 
 * @param array $original
 * @param array $changed
 * @return array
 * @author deceze
 * @source http://stackoverflow.com/questions/6562276/how-to-merge-two-arrays-by-taking-over-only-values-from-the-second-array-that-ha
 */
function array_merge_matched_keys($original, $changed)
{
	if(is_object($original))
		$original = (array) $original;
	
	$merged = array_intersect_key($changed, $original) + $original;
	ksort($merged);
	
	return $merged;
}


/**
 * Make a 2D multidimention array unique by key name
 * 
 * @param array $array
 * @param string $key
 * @return array
 * @author Brad Christie
 * @source http://stackoverflow.com/questions/4585208/how-can-you-make-a-multidimensional-array-unique
 */
function array_unique_2d_by_key($array, $key) 
{ 
	for ($e = 0; $e < count($array); $e++)
	{
		$duplicate = null;
		for ($ee = $e+1; $ee < count($array); $ee++)
		{
			if (strcmp($array[$ee][$key], $array[$e][$key]) === 0)
			{
				$duplicate = $ee;
				break;
			}
		}
		if (!is_null($duplicate)) array_splice($array,$duplicate,1);
	}
	return $array;
}

/**
 * Sort 2D array by Key Name
 * 
 * @param array $array
 * @param string $key
 * @param string $direction (asc|desc)
 * @return array
 * @author Lohoris
 * @author A. H. Abid (Added Direction)
 * @source http://stackoverflow.com/questions/2699086/sort-multi-dimensional-array-by-value
 */
function array_sort_by_key(&$array, $key, $direction = 'asc')
{
	$sorter	= array();
	$ret	= array();
	
	reset($array);
	foreach ($array as $ii => $va)
	{
		if(is_object($va)) $sorter[$ii] = $va->$key;
		else $sorter[$ii] = $va[$key];
	}
	
	if($direction == 'desc') arsort($sorter);
	else asort($sorter);
	
	foreach ($sorter as $ii => $va)
	{
		$ret[] = $array[$ii];
	}

	return $array = $ret;
}

/**
 * Make json_encode to Human Readable (For PHP 5.3 and less)
 * 
 * @param string $json
 * @return string
 * @author som
 * @source http://stackoverflow.com/questions/7393719/human-readable-json-aka-add-spaces-and-breaks-to-json-dump
 */
function json_2_human_readable($json)
{
	$tab_count = 0;			//tab count
	$result = '';			//result
	$quotes = false;		//quotes
	$tab_string = "\t";		//tab
	$new_line = "\n";		//new line

	for($i = 0; $i < strlen($json); $i++) 
	{
		$content = $json[$i];
		if($content == '"' && $json[$i-1] != '\\')
		{
			$quotes = !$quotes;
		}

		if($quotes)
		{
			$result .= $content;
			continue;
		}

		switch($content) {
			case '{':
			case '[':
				$result .= $content . $new_line . str_repeat($tab_string, ++$tab_count);
				break;
			case '}':
			case ']':
				$result .= $new_line . str_repeat($tab_string, --$tab_count) . $content;
				break;
			case ',':
				$result .= $content;
				if($json[$i+1] != '{' && $json[$i+1]!='[')
					$result .= $new_line . str_repeat($tab_string, $tab_count);
				break;
			case ':':
				$result .= $content . ' ';
				break;
			default:
				$result .= $content;
		}
	}

	return $result;
}


/**
 * Inserts values before or after specific key.
 *
 * @param string $before_or_after = 'before'|'after'
 * @param array $array
 * @param sting/integer $position
 * @param array $values
 * @throws Exception
 * @link http://andreas.glaser.me/2013/05/24/php-array-insert-item-beforeafter-array-index/
 * @author Andreas Glaser
 */
function array_insert_to($before_or_after, array &$array, $position, array $values)
{
	if(!in_array($before_or_after, array('before','after')))
	{
		throw new Exception('Array item set before or After not defined properly.');
	}

	// enforce existing position
	if (!isset($array[$position]))
	{
		throw new Exception(strtr('Array position does not exist (:1)', array(':1' => $position)));
	}

	// offset
	$offset = ($before_or_after == 'before') ? -1 : 0;

	// loop through array
	foreach ($array as $key => $value)
	{
		// increase offset
		++$offset;

		// break if key has been found
		if ($key == $position)
		{
			break;
		}
	}

	$array = array_slice($array, 0, $offset, TRUE) + $values + array_slice($array, $offset, NULL, TRUE);

	return $array;
}


/**
 * Checks array items are in the haystack.
 * 
 * If one item is FALSE, then all FALSE
 *
 * @param array $needle
 * @param array $haystack
 * @return bool
 * @author A H Abid
 */
function arrays_in_array($needle, $haystack)
{
	foreach($needle as $item)
	{
		if ( !in_array($item, $haystack) )
			FALSE;
	}
	
	return TRUE;
}


if (!function_exists('array_column')) {

	/**
	 * Returns the values from a single column of the input array, identified by
	 * the $columnKey.
	 *
	 * Optionally, you may provide an $indexKey to index the values in the returned
	 * array by the values from the $indexKey column in the input array.
	 *
	 * @param array $input A multi-dimensional array (record set) from which to pull
	 *                     a column of values.
	 * @param mixed $columnKey The column of values to return. This value may be the
	 *                         integer key of the column you wish to retrieve, or it
	 *                         may be the string key name for an associative array.
	 * @param mixed $indexKey (Optional.) The column to use as the index/keys for
	 *                        the returned array. This value may be the integer key
	 *                        of the column, or it may be the string key name.
	 * @return array
	 * @author Ben Ramsey <[email]>
	 * @link (https://github.com/ramsey/array_column/blob/master/src/array_column.php)
	 */
	function array_column($input = null, $columnKey = null, $indexKey = null)
	{
		// Using func_get_args() in order to check for proper number of
		// parameters and trigger errors exactly as the built-in array_column()
		// does in PHP 5.5.
		$argc = func_num_args();
		$params = func_get_args();

		if ($argc < 2) {
			trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
			return null;
		}

		if (!is_array($params[0])) {
			trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
			return null;
		}

		if (!is_int($params[1])
			&& !is_float($params[1])
			&& !is_string($params[1])
			&& $params[1] !== null
			&& !(is_object($params[1]) && method_exists($params[1], '__toString'))
		) {
			trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		if (isset($params[2])
			&& !is_int($params[2])
			&& !is_float($params[2])
			&& !is_string($params[2])
			&& !(is_object($params[2]) && method_exists($params[2], '__toString'))
		) {
			trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		$paramsInput = $params[0];
		$paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

		$paramsIndexKey = null;
		if (isset($params[2])) {
			if (is_float($params[2]) || is_int($params[2])) {
				$paramsIndexKey = (int) $params[2];
			} else {
				$paramsIndexKey = (string) $params[2];
			}
		}

		$resultArray = array();

		foreach ($paramsInput as $row) {

			$key = $value = null;
			$keySet = $valueSet = false;

			if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
				$keySet = true;
				$key = (string) $row[$paramsIndexKey];
			}

			if ($paramsColumnKey === null) {
				$valueSet = true;
				$value = $row;
			} elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
				$valueSet = true;
				$value = $row[$paramsColumnKey];
			}

			if ($valueSet) {
				if ($keySet) {
					$resultArray[$key] = $value;
				} else {
					$resultArray[] = $value;
				}
			}

		}

		return $resultArray;
	}

}

/**
 * Add an assoc array element to first
 * 
 * @param	array &$arr
 * @param	string $key
 * @param	mixed $val
 * @return	array
 */
function array_unshift_assoc(&$arr, $key, $val) 
{ 
	$arr = array_reverse($arr, true); 
	$arr[$key] = $val; 

	return array_reverse($arr, true); 
}

/* End of file MY_array_helper.php */
/* Location: ./application/helpers/MY_array_helper.php */