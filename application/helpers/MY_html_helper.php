<?php if (!defined('BASEPATH')) die('Die');

/*
|------------------------------------------------------------------------
| MY HTML HELPER
|------------------------------------------------------------------------
|
|
*/

/**
 * Simple html string with help block for Markdown Credit
 * @return string
 */
function html_powered_by_markdown()
{
	return '<small class="help-block info-text">Powered by <a href="http://daringfireball.net/projects/markdown/syntax" target="_blank">Markdown</a></small>';
}


function img_hotjob()
{
	return '<img class="img-sqr-sm" src="'.base_url('assets/app/public/img/jobs/hot-icon.gif').'" />';
}

/* End of file MY_html_helper.php */
/* Location: ./application/helpers/MY_html_helper.php */