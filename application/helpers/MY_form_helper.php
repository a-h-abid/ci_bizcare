<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|------------------------------------------------------------------------
| MY FORM HELPER
|------------------------------------------------------------------------
| FORM helper extending the core functionality.
|
*/

if ( ! function_exists('form_open'))
{
	/**
	 * Form Declaration
	 *
	 * Creates the opening portion of the form.
	 *
	 * @param	string	the URI segments of the form destination
	 * @param	array	a key/value pair of attributes
	 * @param	array	a key/value pair hidden data
	 * @return	string
	 */
	function form_open($action = '', $attributes = array(), $hidden = array())
	{
		$CI =& get_instance();

		// If no action is provided then set to the current url
		if ( ! $action)
		{
			$action = $CI->config->site_url($CI->uri->uri_string());
		}
		// If an action is not a full URL then turn it into one
		elseif (strpos($action, '://') === FALSE)
		{
			$action = $CI->config->site_url($action);
		}

		$attributes = _attributes_to_string($attributes);

		if (stripos($attributes, 'method=') === FALSE)
		{
			$attributes .= ' method="post"';
		}

		if (stripos($attributes, 'accept-charset=') === FALSE)
		{
			$attributes .= ' accept-charset="'.strtolower(config_item('charset')).'"';
		}

		$form = '<form action="'.$action.'"'.$attributes.">\n";

		// Add CSRF field, but leave it out for GET requests and requests to external websites
		if ( strpos($action, $CI->config->base_url()) !== FALSE && ! stripos($form, 'method="get"') )
		{
			$hidden[$CI->security->get_csrf_token_name()] = $CI->security->get_csrf_hash();
		}

		if (is_array($hidden))
		{
			foreach ($hidden as $name => $value)
			{
				$form .= '<input type="hidden" name="'.$name.'" value="'.form_prep($value).'" style="display:none;" />'."\n";
			}
		}

		return $form;
	}
}


/**
 * Generate dropdown from db object or array
 *  
 * @param array|object $data
 * @param string $name
 * @param string $opkey
 * @param string $opval
 * @param string $selected
 * @return string
 * @author A H Abid
 */
function form_dropdown_from_db_obj($data, $name, $opkey, $opval, $selected, $attr = '')
{
	$html = '<select name="'.$name.'" '.$attr . '>';
	$html .= '<option value=""></option>';
	foreach($data as $item)
	{
		if(is_object($item))
			$html .= '<option value="'.$item->$opkey.'" '.($item->$opkey == $selected ? 'selected': '').'>'.$item->$opval.'</option>';
		elseif(is_array($item))
			$html .= '<option value="'.$item[$opkey].'" '.($item[$opkey] == $selected ? 'selected': '').'>'.$item[$opval].'</option>';
		else
			trigger_error('Provided $data is neither array nor object, so dropdown not created.',E_USER_WARNING);
	}
	$html .= '</select>';
	return $html;
}


/**
 * Create a Display Limit Dropdown Form
 * 
 * @param  array  $numbers    [description]
 * @param  string $name       [description]
 * @param  array  $attributes [description]
 * @return string             [description]
 */
function form_dropdown_display_limit($numbers = array(), $name = '', $attributes = array())
{
	$CI =& get_instance();

	if(!is_array($numbers) || count($numbers) < 1)
	{
		$numbers = array(5,10,15,25,50,100);

		$numbers = array_combine(array_values($numbers), array_values($numbers));
	}

	if($name == '')
		$name = 'dl';

	if(!isset($attributes['style']))
		$attributes['style'] = 'width:auto';

	return form_dropdown($name, $numbers, $CI->input->get($name) ? $CI->input->get($name) : $CI->display_limit, $attributes );
}

/**
 * Select An Hour:Minute Time Combination from Dropdown
 * 
 * @param  string $name
 * @param  string $selected
 * @param  array  $attributes
 * @return string
 */
function form_dropdown_time_select($name, $selected = NULL, $attributes = array())
{
	$data = array(''=>'');
	for ($i=0; $i <= 23; $i++)
	{
		for ($j = 0; $j <= 30; $j = $j + 30)
		{
			$time = date('h:i A',strtotime($i.':'.$j));
			$data[$time] = $time;
		}
	}

	return form_dropdown($name, $data, $selected, $attributes );
}

/* End of file MY_form_helper.php */
/* Location: ./application/helpers/MY_form_helper.php */