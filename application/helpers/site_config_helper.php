<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|------------------------------------------------------------------------
| Site Config Helper Funcions
|------------------------------------------------------------------------
| Some of Site Config helper function to manage patterns, 
| quick read write and stuffs.
| 
| To Load this:
|    $this->load->helper('authentication');
|
| To Execute these functions
|    function_name();
|
| To use a CI Class method, you must make an instance of CI with a variable. 
| at the very start, Otherwise it wont work. Ex. $CI =& get_instance();
|
*/


/**
 * Get the CMS Config file in Stings
 * 
 * @return string
 */
function site_config_load_file()
{
	$CI =& get_instance();
	$CI->load->helper('file');
	return read_file(APPPATH.'config/cms_config.php');
}

/**
 * Save the CMS Config file
 * 
 * @param string $data
 * @return boolean
 */
function site_config_save_file($data)
{
	$CI =& get_instance();
	$CI->load->helper('file');
	return write_file(APPPATH.'config/cms_config.php',$data);
}


/**
 * Update Site Configuration Value
 * 
 * @param string $key
 * @param int|string|boolean $value
 * @return boolean
 */
function site_config_update($key, $value)
{
	// Check passed value type
	$in_correct_type = FALSE;
	if(is_string($value) || is_bool($value) || is_numeric($value) ) {
		$in_correct_type = TRUE;
	}
	
	if($in_correct_type == FALSE) {
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT,1);
		show_error('Incorrect value type "'.gettype($value).'" provided for CMS config update.' . "\n" . 'File:'.$backtrace[0]['file'] . ', Line:' . $backtrace[0]['line']);
		return FALSE;
	}
	
	if(is_string($value)) {
		$value = str_replace(array("\r\n","\r","\n"),'\r\n',addslashes($value)) ;
	} elseif(is_bool($value)) {
		$value = ($value === TRUE) ? 'TRUE':'FALSE';
	}
	
	// Check for pattern match
	$pattern = '/[\s\t]*\$config\[\''.$key.'\'\][\s\t]*=[\s\t]*([\'"]?).*[\'"]?[\s\t]*;/m';
	$subject = site_config_load_file();
	
	if(!preg_match($pattern,$subject,$matches)) {
		log_message('error','CMS Config item "'.$key.'" is not found or matched in the pattern.');
		fire_print('log','CMS Config item "'.$key.'" is not found or matched in the pattern.');
		return FALSE;
	}
	
	// Now do the replacement
	$replace = PHP_EOL.'$config[\''.$key.'\'] = '.$matches[1].$value.$matches[1].';'; // $matches[1] = Quotes grabbed from preg match
	$final_data = preg_replace($pattern, $replace, $subject);
	if($final_data != FALSE) {
		return site_config_save_file($final_data);
	} else {
		return FALSE;
	}
}

/* End of file site_config_helper.php */
/* Location: ./application/helpers/site_config_helper.php */