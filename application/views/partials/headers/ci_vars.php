<script type="text/javascript">

var CI = (function(){

	var BaseURL = '<?php echo BASEURL;?>',
		SiteURL = '<?php echo SITEURL;?>',
		LangCode = '<?php echo $this->lang->current_code(); ?>',
		DefaultLangCode = '<?php echo $this->lang->default_code(); ?>';

	// Add a forward slash at end of SiteURL if not there already
	if ( SiteURL.substr(SiteURL.length - 1) != '/') {
		SiteURL = [SiteURL,'/'].join();
	}

	// Added not default lang code at end with slash
	if(LangCode != DefaultLangCode) {
		SiteURL = [SiteURL,LangCode,'/'].join();
	}

	return {
		BaseURL : BaseURL,
		SiteURL : SiteURL,
		LangCode : LangCode,
		DefaultLangCode : DefaultLangCode,
	};
});

</script>