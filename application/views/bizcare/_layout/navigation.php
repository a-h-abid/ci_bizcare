<div class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav navbar-right">
			<li <?php if(uri_string() == '') echo 'class="active"'; ?>><a href="<?php echo site_url('') ?>"><?php echo 'Home' ?></a></li>
			<li <?php if(uri_string() == 'about') echo 'class="active"'; ?>><a href="<?php echo site_url('about') ?>"><?php echo 'About Us' ?></a></li>
			<li class="dropdown <?php if( in_array(uri_string(),array('communication','corporate-responsibility','human-resource')) ) echo ' active'; ?>">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo 'Services' ?> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo site_url('communication') ?>">Communication</a></li>
					<li><a href="<?php echo site_url('corporate-responsibility') ?>">Corporate Responsibility</a></li>
					<li><a href="<?php echo site_url('human-resource') ?>">Human Resource</a></li>
				</ul>
			</li>
			<li <?php if(uri_string() == 'clients') echo 'class="active"'; ?>><a href="<?php echo site_url('clients') ?>"><?php echo 'Clients' ?></a></li>
            <!-- <li <?php if(uri_string() == 'team') echo 'class="active"'; ?>><a href="<?php echo site_url('team') ?>"><?php echo 'Team' ?></a></li> -->
			<li <?php if(uri_string() == 'partners') echo 'class="active"'; ?>><a href="<?php echo site_url('partners') ?>"><?php echo 'Partners' ?></a></li>
			<li <?php if(uri_string() == 'contact') echo 'class="active"'; ?>><a href="<?php echo site_url('contact') ?>"><?php echo 'Contact Us' ?></a></li>
		</ul>
	</div><!--/.nav-collapse -->
</div>