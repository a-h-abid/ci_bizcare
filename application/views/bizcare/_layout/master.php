<!DOCTYPE html>
<html class="no-js" lang="<?php echo $this->lang->current_code() ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF -->
<meta name="csrf_token_name" content="<?php echo $this->security->get_csrf_token_name();?>"/>
<meta name="csrf_cookie_name" content="<?php echo $this->config->item('csrf_cookie_name');?>"/>
<meta name="csrf_cookie" content="<?php echo $this->security->get_csrf_hash();?>"/>

<!-- SEO META -->
<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">
<meta name="author_website" content="<?php echo 'https://a-h-abid.com'; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">

<link rel="canonical" href="<?php echo site_url(uri_string()) ;?>">

<link rel="shortcut icon" href="<?php echo base_url('favicon.ico'); ?>">
<link rel="apple-touch-icon" href="<?php echo base_url('apple-touch-icon.png'); ?>">

<title><?php echo $page_title .' | '. $this->config->item('site_name') ?></title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/compiled/css/frontend.min.css') ?>?t=20170526" />

<noscript>
	<div class="container">
		<div class="alert alert-danger">
			It seems Javascript is disabled in your browser. Please enable it to view this site.
		</div>
	</div>
</noscript>

<?php $this->load->view('partials/headers/ci_vars') ?>

<!--[if lt IE 9]>
	<div class="alert alert-danger">
		You are using an older Internet Explorer. Please upgrade to a latest version.
	</div>
<![endif]-->

<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient-ie9 {
       filter: none;
    }
  </style>
<![endif]-->

</head>
<body class="<?php echo $body_class ?>">

<div id="header-section" class="clearfix">

	<div class="top-gutter"></div>

	<div class="top-gradient gradient-ie9">

		<div class="container clearfix">

			<div id="site-logo-container" class="col-md-3 col-sm-4">
				<img class="site-logo" src="<?php echo base_url('assets/compiled/img/logos/bizcare-logo.png') ?>" alt="BizCare Logo" />
			</div>

			<div id="main-menu-container" class="col-md-9 col-sm-8">
				<?php $CI->theme_view('_layout/navigation') ?>
			</div>

		</div>

	</div>

</div>

<div id="main-container">
	<div class="container">
		<div id="main-contents" class="clearfix">
			<?php $CI->theme_view($sub_view) ?>
		</div>
	</div>
</div>

<div id="footer-section">

	<div class="container text-center">

		<div class="col-sm-offset-4 col-sm-4">
			<p class="copyright">Copyright &copy; <?php echo date('Y').' '.$this->config->item('site_alter_name') ?></p>
		</div>

		<div class="col-sm-4">
			<a class="sr-only" target="_blank" href="http://facebook.com/BizCare">Facebook.com</a>
			<a class="sr-only" target="_blank" href="https://twitter.com/bizcarebd">Twitter.com</a>
			<p class="developed-by pull-right">Developed by Banyan-Techologies</p>
		</div>
	</div>

</div>

<!-- Scripts at Bottom to load page faster -->
<script type="text/javascript" src="<?php echo base_url('assets/compiled/js/frontend.min.js') ?>?t=20170526"></script>

<!-- Social Share Scripts -->
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53dc72014fd1ed6c"></script>

<?php if($_SERVER['SERVER_NAME'] == 'bizcarebd.com') : ?>
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53685603-1', 'auto');
  ga('send', 'pageview');

</script>
<?php endif; ?>

<?php if (isset($scripts_extra)) echo $scripts_extra; ?>

</body>
</html>