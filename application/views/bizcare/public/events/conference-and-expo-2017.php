<br/>
<br/>

<div class="col-sm-2">

</div>

<div class="col-sm-10">

    <h1 class="sr-only">Conference &amp; Expo</h1>

    <div class="event-conf-wrapper clearfix">
        <img alt="BizCareBD Conference & Expo - Corporate Social Responsibility (CSR) 1" class="img-responsive conf-csr-imgs active" src="<?php echo base_url('assets/compiled/img/events/conference-and-expo-2017/csr-1.min.png') ?>" />
        <img alt="BizCareBD Conference & Expo - Corporate Social Responsibility (CSR) 2" class="img-responsive conf-csr-imgs" src="<?php echo base_url('assets/compiled/img/events/conference-and-expo-2017/csr-2.min.png') ?>" />
    </div>

    <br />
    <br />

    <div class="text-center">

        <a class="btn btn-warning event-conf-prev" href="#">Previous</a>
        <a class="btn btn-warning event-conf-next" href="#">Next</a>

    </div>

    <br />
    <br />

    <?php if (time() < strtotime('2017-11-23')) : ?>
    <div class="clearfix">
        <a class="btn btn-danger" href="#">Register Here</a>
        <a class="btn btn-primary pull-right" href="#">Visit Conference &amp; Expo website</a>
    </div>
    <?php endif; ?>

</div>
