<div class="home-description">

	<div id="carousel-slogan" class="carousel slide carousel-fade" data-ride="carousel">

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<h2 class="slogan">
					A Consulting Firm for Businesses through a Sustainable Approach
				</h2>
			</div>
			<div class="item">
				<h2 class="slogan">
					Businesses through a Sustainable Approach
				</h2>
			</div>
			<div class="item">
				<h2 class="slogan">
					A Sustainable Approach
				</h2>
			</div>
		</div>

	</div>

</div> <!-- /.home-description -->

<div class="home-text clearfix">

	<div class="col-md-8"> <!-- Content Area -->

		<br/>

		<h1 class="sr-only">Home</h1>

		<p>Greetings!</p>

		<p>It is our great pleasure to introduce BizCare, a consulting firm based in Dhaka and engaged in three broad areas - <strong>Communication, Corporate Responsibility and Human Resource.</strong></p>

		<p><strong>"Bridging gaps for sustainability"</strong> is our founding belief and commitment. Our aim is to explore opportunities and identify the strategic growth potentials of our clients. We, at BizCare believe in comprehensive solutions towards sustainable business management. </p>

		<hr/>

		<div class="home-services">

			<h3 class="here-with-you">Here are three questions for you...</h3>

			<ul class="service-panels clearfix">
				<li class="service-panel">

					<!-- <img class="img-responsive" src="<?php //echo base_url('assets/compiled/img/pages/home/communication.png') ?>" /> -->

					<small>Would your business sustain without effective <strong class="highlight-word">COMMUNICATION <br/> STRATEGY?</strong></small>

					<em class="communication-text">We are here to enable effective communication network...</em>

					<label class="readmore">
						<a href="<?php echo site_url('communication') ?>">
							Read More <i class="fa fa-chevron-circle-right"></i>
						</a>
					</label>
				</li>

				<li class="service-panel">

					<!-- <img class="img-responsive" src="<?php //echo base_url('assets/compiled/img/pages/home/human-resource.png') ?>" /> -->

					<small>How <strong class="highlight-word">Socially responsible</strong><br/> is your business?</small>

					<em class="social-text">We are here to design responsible and sustainable business...</em>

					<label class="readmore">
						<a href="<?php echo site_url('human-resource') ?>">
							Read More <i class="fa fa-chevron-circle-right"></i>
						</a>
					</label>
				</li>

				<li class="service-panel">

					<!-- <img class="img-responsive" src="<?php //echo base_url('assets/compiled/img/pages/home/corporate-responsibility.png') ?>" /> -->

					<small>How will you track your most <strong class="highlight-word">efficient resource?</strong></small>

					<em class="resource-text">We are here to provide customized human capital solutions...</em>

					<label class="readmore">
						<a href="<?php echo site_url('corporate-responsibility') ?>">
							Read More <i class="fa fa-chevron-circle-right"></i>
						</a>
					</label>
				</li>


			</ul>

		</div> <!-- /.home-services -->

        <hr />

        <div class="upcoming-events">

            <h3 class="here-with-you">Upcoming event</h3>

            <?php if (time() < strtotime('2017-11-23')) : ?>
            <a style="font-size:24px;font-weight: bold;color:#000;" href="<?php echo site_url('events/conference-and-expo-2017') ?>">Conference and Expo</a>
            <?php endif; ?>

        </div>

	</div>

	<div class="col-md-4"> <!-- Aside Column -->


		<div class="feeds-panel">

			<h4 class="panel-header-highlight">BizCare&trade; Live</h4>

			<div class="fb-like-box" data-href="https://www.facebook.com/BizCare" data-width="250px" data-height="375px" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="true"></div>

		</div>

		<div class="clearfix"></div>

		<div class="contact-panel">

			<h4 class="panel-header-highlight">Contact</h4>

			<p>
				Level 6, Plot 10, Road 2/2 <br/>
				Banani, Dhaka 1213 <br/>
				Bangladesh<br/>
				Query : +88 02 9855227<br/>
				<span style="margin-left:50px;">info@bizcarebd.com</span>
			</p>

		</div>

		<div class="clearfix"></div>

	</div>

</div> <!-- /.home-text -->

