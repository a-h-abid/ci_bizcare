<br/>
<br/>

<div class="col-sm-2">
	<img class="img-responsive" style="width:85%" src="<?php echo base_url('assets/compiled/img/pages/communication/communication-heading.png') ?>" alt="Communication" />
</div>


<div class="col-sm-10 favicon-placeholder">
	
	<h1 class="sr-only">Communication</h1>

	<h3 class="bizcare-question">
		Would your business<br/>sustain without effective<br/>communication strategy?
	</h3>
	
	<br/>
	<br/>

	<h3 class="here-with-you">We are here for you with...</h3>

	<div class="col-sm-5 vertical-divider-right">

		<strong>Public Relations Support</strong></br>
		<ul>
			<li>Media Relations</li>
			<li>Stakeholder Management</li>
			<li>Newsletter Publication</li>
			<li>Crisis Communication</li>
		</ul>

		<strong>Corporate Brand Management</strong></br>
		<ul>
			<li>Internal Brand Building</li>
			<li>External Brand Building</li>
		</ul>
		 
		<strong>Creative Communication</strong></br>
		<ul>
			<li>Production</li>
			<li>Graphics Design</li>
			<li>Infographics &amp; Data Visualization</li>
		</ul>

	</div>
	
	<div class="col-sm-5">

		<strong>Online Marketing</strong></br>
		<ul>
			<li>Web Development</li>
			<li>Web Marketing</li>
			<li>Social Media Engagement</li>
		</ul>
		 
		<strong>Events &amp; Activation</strong></br>
		<ul>
			<li>Brand Activation</li>
			<li>Corporate Events</li>
			<li>Product/Service Launch</li>
		</ul>

	</div>

	<img class="favicon-corner" src="<?php echo base_url('assets/compiled/img/icons/favicon.png') ?>" />

</div>