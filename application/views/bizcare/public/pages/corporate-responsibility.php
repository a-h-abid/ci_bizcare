<br/>
<br/>

<div class="col-sm-2">
	<img class="img-responsive" style="width:60%;" src="<?php echo base_url('assets/compiled/img/pages/corporate-responsibility/corporate-responsibility-heading.png') ?>" alt="Corporate Responsibility" />
</div>


<div class="col-sm-10 favicon-placeholder">
	
	<h1 class="sr-only">Corporate Responsibility</h1>

	<h3 class="bizcare-question">
		How responsible is your<br/>business towards<br/>the society?
	</h3>
	
	<br/>
	<br/>

	<h3 class="here-with-you">We are here for you with...</h3>

	<div class="col-sm-12">

		<strong>Social Responsibility</strong></br>
		<ul>
			<li>CSR Project Design &amp; Implementation</li>
			<li>Research and Training</li>
			<li>Employee Engagement</li>
			<li>Green Initatives</li>
		</ul>

		<strong>Occupational Health and Safety</strong><br/>
		<ul>
			<li>Workplace Hazard Identification</li>
			<li>OHS Training</li>
			<li>Safety Drill</li>
			<li>Safety Communication</li>
		</ul>
		 
		<strong>Sustainability Report</strong><br/>
		<ul>
			<li>Research and Analysis</li>
			<li>Publication</li>
		</ul>

	</div>
	
	<img class="favicon-corner" src="<?php echo base_url('assets/compiled/img/icons/favicon.png') ?>" />

</div>