<br/>
<br/>

<div class="col-sm-2">
	<img class="img-responsive" src="<?php echo base_url('assets/compiled/img/pages/about/about-heading.png') ?>" alt="About Us" />
</div>

<div class="col-sm-10">

	<h1 class="sr-only">About Us</h1>

	<!-- <p>BizCare is a Strategic Communications and PR Consultancy firm based in Dhaka, Bangladesh.</p>

	<h2 class="panel-header-highlight">Mission</h2>

	<p>At BizCare, our only mission is to engage businesses to guaranteed profit while ensuring a sustainable business future.

	<h2 class="panel-header-highlight">Description</h2> -->

	<p>Businesses can no longer solely rely on traditional theory for their growth; rather a lot more requires becoming a strong corporate entity in the market. With a small yet highly experienced team, BizCare in 2011 started with a vision to become a distinct strategic communications and consultancy firm in Bangladesh. We are now driven by best business practices and values, believing in long term success rather than short term results.</p>

	<ul class="about-lists">
		
		<li>
			<strong class="header-article">Our Concept</strong>
			<p>“Bridging gaps for sustainability” as our founding belief, our continuous aim is to explore opportunities and identify the strategic growth potentials for our clients. We, at BizCare, believe in comprehensive solutions towards sustainable business management.</p>
		</li>
		<li>
			<strong class="header-article">What BizCare Does?</strong>
			<p>BizCare provides corporate level consulting services for growing firms and businesses enabling them to improve their performance and meet the desired goals through a sustainable approach. It analyzes organizational strengths and weaknesses, helps draw the strategy of action for better productivity and evaluates performance.</p>
		</li>
		<li>
			<strong class="header-article">Why BizCare?</strong>
			<ul class="sub-lists">
				<li>Quick Feedback</li>
				<li>Realistic &amp; Cost Effective Solutions</li>
				<li>Flexibility &amp; Responsive Approach</li>
				<li>Team &amp; Expertise</li>
			</ul>
			<br/>
		</li>
		
		
	</ul>

</div>