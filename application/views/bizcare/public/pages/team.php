<br/>
<br/>

<div class="col-sm-2">
	<img class="img-responsive" src="<?php echo base_url('assets/compiled/img/pages/team/team-heading.png') ?>" alt="Team" />
</div>


<div class="col-sm-10">
	
	<h1 class="sr-only">Team</h1>

	<table class="team-members-list">
		<tbody>
			<tr>
				<td class="image-cell">
					<img alt="Mohiuddin Babor" class="img-responsive member-img" src="<?php echo base_url('assets/compiled/img/pages/team/babor.png') ?>" />
				</td>
				<td class="details-cell">
					<big>Mohiuddin Babor</big>
					<small>Chief Executive</small>
				</td>
			</tr>
			<tr>
				<td class="image-cell">
					<img alt="Talal Redwan Babor" class="img-responsive member-img" src="<?php echo base_url('assets/compiled/img/pages/team/talal.png') ?>" />
				</td>
				<td class="details-cell">
					<big>Talal Redwan Babor</big>
					<small>Head of Operations</small>
				</td>
			</tr>
			<tr>
				<td class="image-cell">
					<img alt="Khoadaker Riazul Haque" class="img-responsive member-img" src="<?php echo base_url('assets/compiled/img/pages/team/riazul.png') ?>" />
				</td>
				<td class="details-cell">
					<big>Khoadaker Riazul Haque</big>
					<small>Head of Communications</small>
				</td>
			</tr>
		</tbody>
	</table>

</div>