<br/>
<br/>

<div class="col-sm-2">
	<img class="img-responsive" style="width:75%" src="<?php echo base_url('assets/compiled/img/pages/human-resource/human-resource-heading.png') ?>" alt="Human Resource" />
</div>


<div class="col-sm-10 favicon-placeholder">
	
	<h1 class="sr-only">Human Resource</h1>

	<h3 class="bizcare-question">
		How would you turn your<br/>manpower into effective<br/>resource?
	</h3>
	
	<br/>
	<br/>

	<h3 class="here-with-you">We are here for you with...</h3>

	<div class="col-sm-5 vertical-divider-right">

		<strong>Find</strong><br/>
		<ul>
			<li>Executive Search</li>
			<li>Professional Staffing</li>
		</ul>
		 
		<strong>Develop</strong><br/>
		<ul>
			<li>Leadership Training</li>
			<li>Performance Management</li>
			<li>HR Assessment</li>
		</ul>
		 
		<strong>Retain</strong><br/>
		<ul>
			<li>Compensation Guidance</li>
			<li>Employee Opinion Surveys</li>
			<li>Diversity Training</li>
		</ul>
		 

	</div>
	
	<div class="col-sm-5">

		<strong>Transition</strong><br/>
		<ul>
			<li>Outplacement</li>
			<li>Succession Planning Strategies</li>
		</ul>
		 
		<strong>Support Services</strong><br/>
		<ul>
			<li>Job Description Designing</li>
			<li>Employee Engagement Programs</li>
			<li>Performance Mapping</li>
			<li>Employee Wellness</li>
		</ul>

	</div>

	<img class="favicon-corner" src="<?php echo base_url('assets/compiled/img/icons/favicon.png') ?>" />

</div>