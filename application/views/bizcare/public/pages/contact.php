<br/>
<br/>

<div style="width:350px;margin:0 auto;">
	<img class="img-responsive" src="<?php echo base_url('assets/compiled/img/pages/contact-us/contact-us-heading.png') ?>" alt="Contact Us">
	<h1 class="sr-only">Contact Us</h1>
</div>

<br/>
<br/>


<div style="width:125px;margin:0 auto;">
	<img class="img-responsive" src="<?php echo base_url('assets/compiled/img/pages/contact-us/contact-us-qrcode.png') ?>">
</div>

<br/>
<br/>
<br/>

<div style="text-align:center;">
	<p>
		<strong>Level 6, Plot 10, Road 2/2, Banani, Dhaka 1213, Bangladesh</strong><br/>
		+88 02 9855227 | info@bizcarebd.com | www.bizcarebd.com | facebook.com/BizCare
	</p>
</div>