<br/>
<br/>

<div class="col-sm-2">
	<img class="img-responsive" style="width:75%" src="<?php echo base_url('assets/compiled/img/pages/clients/clients-heading.png') ?>" alt="Clients" />
</div>


<div class="col-sm-10">
	
	<h1 class="sr-only">Clients</h1>

	<?php
		$logos = scandir(SITEROOT.'www/assets/compiled/img/pages/clients/logos');
		
		if (is_array($logos)) :

		unset($logos[0],$logos[1]);
		foreach ($logos as $logo)
		{
			echo '<img class="img-responsive col-sm-4" src="'.base_url('assets/compiled/img/pages/clients/logos/'.$logo).'"/>';
		}

		else :

		echo '<div class="alert alert-warning">No logo found</div>';

		endif;
	?>

</div>