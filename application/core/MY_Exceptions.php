<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Extended Exceptions Class
 *
 * @package        CodeIgniter
 * @subpackage    Core
 * @category    MY_Exceptions
 * @author        A. H. Abid <a_h_abid@hotmail.com>
 */
class MY_Exceptions extends CI_Exceptions {

	/**
	 * 404 Error Handler
	 *
	 * @uses    CI_Exceptions::show_error()
	 *
	 * @param    string    $page        Page URI
	 * @param     bool    $log_error    Whether to log the error
	 * @return    void
	 */
	public function show_404($page = '', $log_error = FALSE)
	{
		if (is_cli())
		{
			$heading = 'Not Found';
			$message = 'The controller/method pair you requested was not found.';
		}
		else
		{
			$heading = '404 Page Not Found';
			$message = 'The page you requested was not found.';
		}

		// if Ajax Request, send json
		if (IS_AJAX)
		{
			$json = array(
				'status' => FALSE,
				'message_header' => $heading,
				'message' => $message,
			);
			echo json_encode($json);
		}
		else
		{
			echo $this->show_error($heading, $message, 'error_404', 404);
		}
		exit(4); // EXIT_UNKNOWN_FILE
	}

	// --------------------------------------------------------------------

	/**
	 * General Error Page
	 *
	 * Takes an error message as input (either as a string or an array)
	 * and displays it using the specified template.
	 *
	 * @param    string        $heading    Page heading
	 * @param    string|string[]    $message    Error message
	 * @param    string        $template    Template name
	 * @param     int        $status_code    (default: 500)
	 *
	 * @return    string    Error page output
	 */
	public function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{
		if(IS_AJAX)
		{
			set_status_header(500);
			$json = array(
				'status' => FALSE,
				'message_header' => $heading,
				'message' => $message,
			);
			echo json_encode($json);
			return FALSE;
		}

		if (is_cli())
		{
			$message = "\t".(is_array($message) ? implode("\n\t", $message) : $message);
			$template = 'cli'.DIRECTORY_SEPARATOR.$template;
		}
		else
		{
			set_status_header($status_code);
			$message = '<p>'.(is_array($message) ? implode('</p><p>', $message) : $message).'</p>';
			$template = 'html'.DIRECTORY_SEPARATOR.$template;
		}

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}
		ob_start();
		include(VIEWPATH.'errors'.DIRECTORY_SEPARATOR.$template.'.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}

	// --------------------------------------------------------------------

	/**
	 * Native PHP error handler
	 *
	 * @param    int    $severity    Error level
	 * @param    string    $message    Error message
	 * @param    string    $filepath    File path
	 * @param    int    $line        Line number
	 * @return    string    Error page output
	 */
	public function show_php_error($severity, $message, $filepath, $line)
	{
		$severity = isset($this->levels[$severity]) ? $this->levels[$severity] : $severity;

		if(IS_AJAX)
		{
			set_status_header(500);

			$json = array();
			$json['status'] = FALSE;
			$json['message_header'] = $severity;
			$json['message'] = $message;
			if(ENVIRONMENT == 'development') $json['debug'] = array('file' => $filepath, 'line' => $line);

			echo json_encode($json);
			die();
		}

		// For safety reasons we don't show the full file path in non-CLI requests
		if ( ! is_cli())
		{
			$filepath = str_replace('\\', '/', $filepath);
			if (FALSE !== strpos($filepath, '/'))
			{
				$x = explode('/', $filepath);
				$filepath = $x[count($x)-2].'/'.end($x);
			}

			$template = 'html'.DIRECTORY_SEPARATOR.'error_php';
		}
		else
		{
			$template = 'cli'.DIRECTORY_SEPARATOR.'error_php';
		}

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}
		ob_start();
		include(VIEWPATH.'errors'.DIRECTORY_SEPARATOR.$template.'.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;
	}

}

/* End of file MY_Exceptions.php */
/* Location: ./application/core/MY_Exceptions.php */