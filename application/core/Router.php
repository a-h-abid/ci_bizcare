<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Router Class
 *
 * Parses URIs and determines routing
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/routing.html
 */
class CI_Router {

	/**
	 * Contants for route index
	 */
	CONST ARR_ROUTE_USES	= 'uses';
	CONST ARR_ROUTE_NAME	= 'as';
	CONST ARR_ROUTE_REGEX	= 'regex';
	CONST ARR_ROUTE_CSRF	= 'csrf';
	CONST ARR_ROUTE_VERB	= 'verb';

	/**
	 * CI_Config class object
	 *
	 * @var	object
	 */
	public $config;

	/**
	 * List of routes
	 *
	 * @var	array
	 */
	public $routes =	array();

	/**
	 * Reverse Routes are all assigned here
	 */
	private $_reverse_routes = NULL;

	/**
	 * Current Route Key
	 * 
	 * @var string
	 */
	public $current_route_key = '';

	/**
	 * Current Route Index
	 * 
	 * @var string
	 */
	public $current_route_uri = '';

	/**
	 * Current class name
	 *
	 * @var	string
	 */
	public $class =		'';

	/**
	 * Current method name
	 *
	 * @var	string
	 */
	public $method =	'index';

	/**
	 * Sub-directory that contains the requested controller class
	 *
	 * @var	string
	 */
	public $directory =	'';

	/**
	 * Default controller (and method if specific)
	 *
	 * @var	string
	 */
	public $default_controller;

	/**
	 * Translate URI dashes
	 *
	 * Determines whether dashes in controller & method segments
	 * should be automatically replaced by underscores.
	 *
	 * @var	bool
	 */
	public $translate_uri_dashes = FALSE;

	/**
	 * Enable query strings flag
	 *
	 * Determines wether to use GET parameters or segment URIs
	 *
	 * @var	bool
	 */
	public $enable_query_strings = FALSE;

	// --------------------------------------------------------------------

	/**
	 * Class constructor
	 *
	 * Runs the route mapping function.
	 *
	 * @return	void
	 */
	public function __construct($routing = NULL)
	{
		$this->config =& load_class('Config', 'core');
		$this->uri =& load_class('URI', 'core');

		$this->enable_query_strings = ( ! is_cli() && $this->config->item('enable_query_strings') === TRUE);
		$this->_set_routing();

		// Set any routing overrides that may exist in the main index file
		if (is_array($routing))
		{
			if (isset($routing['directory']))
			{
				$this->set_directory($routing['directory']);
			}

			if ( ! empty($routing['controller']))
			{
				$this->set_class($routing['controller']);
			}

			if ( ! empty($routing['function']))
			{
				$this->set_method($routing['function']);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Set route mapping
	 *
	 * Determines what should be served based on the URI request,
	 * as well as any "routes" that have been set in the routing config file.
	 *
	 * @return	void
	 */
	protected function _set_routing()
	{
		// Are query strings enabled in the config file? Normally CI doesn't utilize query strings
		// since URI segments are more search-engine friendly, but they can optionally be used.
		// If this feature is enabled, we will gather the directory/class/method a little differently
		if ($this->enable_query_strings)
		{
			$_d = $this->config->item('directory_trigger');
			$_d = isset($_GET[$_d]) ? trim($_GET[$_d], " \t\n\r\0\x0B/") : '';
			if ($_d !== '')
			{
				$this->set_directory($this->uri->filter_uri($_d));
			}

			$_c = $this->config->item('controller_trigger');
			if ( ! empty($_GET[$_c]))
			{
				$this->set_class(trim($this->uri->filter_uri(trim($_GET[$_c]))));

				$_f = $this->config->item('function_trigger');
				if ( ! empty($_GET[$_f]))
				{
					$this->set_method(trim($this->uri->filter_uri($_GET[$_f])));
				}

				$this->uri->rsegments = array(
					1 => $this->class,
					2 => $this->method
				);
			}
			else
			{
				$this->_set_default_controller();
			}

			// Routing rules don't apply to query strings and we don't need to detect
			// directories, so we're done here
			return;
		}

		// Load the routes.php file.
		if (file_exists(APPPATH.'config/routes.php'))
		{
			include(APPPATH.'config/routes.php');
		}

		if (file_exists(APPPATH.'config/'.ENVIRONMENT.'/routes.php'))
		{
			include(APPPATH.'config/'.ENVIRONMENT.'/routes.php');
		}

		// Validate & get reserved routes
		if (isset($route) && is_array($route))
		{
			isset($route['default_controller']) && $this->default_controller = $route['default_controller'];
			isset($route['translate_uri_dashes']) && $this->translate_uri_dashes = $route['translate_uri_dashes'];
			unset($route['default_controller'], $route['translate_uri_dashes']);
			$this->routes = $route;
		}

		// Is there anything to parse?
		if ($this->uri->uri_string !== '')
		{
			$this->_parse_routes();
		}
		else
		{
			$this->_set_default_controller();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Set request route
	 *
	 * Takes an array of URI segments as input and sets the class/method
	 * to be called.
	 *
	 * @used-by	CI_Router::_parse_routes()
	 * @param	array	$segments	URI segments
	 * @return	void
	 */
	protected function _set_request($segments = array())
	{
		$segments = $this->_validate_request($segments);
		// If we don't have any segments left - try the default controller;
		// WARNING: Directories get shifted out of the segments array!
		if (empty($segments))
		{
			$this->_set_default_controller();
			return;
		}

		if ($this->translate_uri_dashes === TRUE)
		{
			$segments[0] = str_replace('-', '_', $segments[0]);
			if (isset($segments[1]))
			{
				$segments[1] = str_replace('-', '_', $segments[1]);
			}
		}

		$this->set_class($segments[0]);
		if (isset($segments[1]))
		{
			$this->set_method($segments[1]);
		}
		else
		{
			$segments[1] = 'index';
		}

		array_unshift($segments, NULL);
		unset($segments[0]);
		$this->uri->rsegments = $segments;
	}

	// --------------------------------------------------------------------

	/**
	 * Set default controller
	 *
	 * @return	void
	 */
	protected function _set_default_controller()
	{
		if (empty($this->default_controller))
		{
			show_error('Unable to determine what should be displayed. A default route has not been specified in the routing file.');
		}

		// Is the method being specified?
		if (sscanf($this->default_controller, '%[^/]/%s', $class, $method) !== 2)
		{
			$method = 'index';
		}

		if ( ! file_exists(APPPATH.'controllers/'.$this->directory.ucfirst($class).'.php'))
		{
			// This will trigger 404 later
			return;
		}

		$this->set_class($class);
		$this->set_method($method);

		// Assign routed segments, index starting from 1
		$this->uri->rsegments = array(
			1 => $class,
			2 => $method
		);

		log_message('debug', 'No URI present. Default controller set.');
	}

	// --------------------------------------------------------------------

	/**
	 * Validate request
	 *
	 * Attempts validate the URI request and determine the controller path.
	 *
	 * @used-by	CI_Router::_set_request()
	 * @param	array	$segments	URI segments
	 * @return	mixed	URI segments
	 */
	protected function _validate_request($segments)
	{
		$c = count($segments);

		// Loop through our segments and return as soon as a controller
		// is found or when such a directory doesn't exist
		while ($c-- > 0)
		{
			$test = $this->directory
				.ucfirst($this->translate_uri_dashes === TRUE ? str_replace('-', '_', $segments[0]) : $segments[0]);

			if ( ! file_exists(APPPATH.'controllers/'.$test.'.php') && is_dir(APPPATH.'controllers/'.$this->directory.$segments[0]))
			{
				$this->set_directory(array_shift($segments), TRUE);
				continue;
			}

			return $segments;
		}

		// This means that all segments were actually directories
		return $segments;
	}

	// --------------------------------------------------------------------

	/**
	 * Parse Routes
	 *
	 * Matches any routes that may exist in the config/routes.php file
	 * against the URI to determine if the class/method need to be remapped.
	 *
	 * @return	void
	 */
	protected function _parse_routes()
	{
		// Turn the segment array into a URI string
		$uri = implode('/', $this->uri->segments);
		
		// Get HTTP verb
		$http_verb = isset($_SERVER['REQUEST_METHOD']) ? strtolower($_SERVER['REQUEST_METHOD']) : 'cli';

		// Is there a literal match?  If so we're done
		if (isset($this->routes[$uri]))
		{
			// Check default routes format
			if (is_string($this->routes[$uri]))
			{
				$this->_set_request(explode('/', $this->routes[$uri]));
				$this->current_route_uri = $uri;
				return;
			}
			// Is there a matching http verb?
			elseif (is_array($this->routes[$uri]))
			{
				if (isset($this->routes[$uri][$http_verb]))
				{
					$this->_set_request(explode('/', $this->routes[$uri][$http_verb]));
					$this->current_route_uri = $uri;
					return;
				}
				elseif (isset($this->routes[$uri][self::ARR_ROUTE_USES]))
				{
					if( isset($this->routes[$uri][self::ARR_ROUTE_VERB]) 
						&& $http_verb !== strtolower($this->routes[$uri][self::ARR_ROUTE_VERB]) )
					{
						show_404();
					}

					$this->_set_request( explode('/', $this->routes[$uri][self::ARR_ROUTE_USES]) );
					$this->current_route_uri = $uri;
					return;
				}
			}
		}

		// Loop through the route array looking for wildcards
		foreach ($this->routes as $key => $val)
		{
			// Save the current loop index key
			$route_index = $key;

			// CSRF Setting default for loop start
			$csrf = FALSE;

			// Check if route format is using http verb
			if (is_array($val))
			{
				if (isset($val[$http_verb]))
				{
					$val = $val[$http_verb];
				}
				elseif (isset($val[self::ARR_ROUTE_USES]))
				{
					// Check for Regex
					if (isset($val[self::ARR_ROUTE_REGEX]))
					{
						foreach($val[self::ARR_ROUTE_REGEX] as $paramKey => $paramRegex)
						{
							$key = str_replace(':'.$paramKey, $paramRegex, $key);
						}
					}

					// Check for CSRF
					if (isset($val[self::ARR_ROUTE_CSRF]))
					{
						$csrf = $val[self::ARR_ROUTE_CSRF];
					}

					$val = $val[self::ARR_ROUTE_USES];
				}
				else
				{
					continue;
				}
			}

			// Convert wildcards to RegEx
			$key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);
			
			// Does the RegEx match?
			if (preg_match('#^'.$key.'$#', $uri, $matches))
			{
				// Are we using callbacks to process back-references?
				if ( ! is_string($val) && is_callable($val))
				{
					// Remove the original string from the matches array.
					array_shift($matches);

					// Execute the callback using the values in matches as its parameters.
					$val = call_user_func_array($val, $matches);
				}
				// Are we using the default routing method for back-references?
				elseif (strpos($val, '$') !== FALSE && strpos($key, '(') !== FALSE)
				{
					$val = preg_replace('#^'.$key.'$#', $val, $uri);
				}

				// @TODO Check for CSRF
				if ($csrf === TRUE)
				{

				}


				$this->_set_request(explode('/', $val));
				$this->current_route_uri = $route_index;
				return;
			}
		}

		// Stop Autoguess Controller Method Route if set to TRUE
		if ($this->config->item('allow_autoguess_routes') === FALSE)
		{
			show_404();
		}

		// If we got this far it means we didn't encounter a
		// matching route so we'll set the site default route
		$this->current_route_uri = $uri;
		$this->_set_request(array_values($this->uri->segments));
	}

	// --------------------------------------------------------------------

	/**
	 * Set class name
	 *
	 * @param	string	$class	Class name
	 * @return	void
	 */
	public function set_class($class)
	{
		$this->class = str_replace(array('/', '.'), '', $class);
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch the current class
	 *
	 * @deprecated	3.0.0	Read the 'class' property instead
	 * @return	string
	 */
	public function fetch_class()
	{
		return $this->class;
	}

	// --------------------------------------------------------------------

	/**
	 * Set method name
	 *
	 * @param	string	$method	Method name
	 * @return	void
	 */
	public function set_method($method)
	{
		$this->method = $method;
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch the current method
	 *
	 * @deprecated	3.0.0	Read the 'method' property instead
	 * @return	string
	 */
	public function fetch_method()
	{
		return $this->method;
	}

	// --------------------------------------------------------------------

	/**
	 * Set directory name
	 *
	 * @param	string	$dir	Directory name
	 * @param	bool	$appent	Whether we're appending rather then setting the full value
	 * @return	void
	 */
	public function set_directory($dir, $append = FALSE)
	{
		if ($append !== TRUE OR empty($this->directory))
		{
			$this->directory = str_replace('.', '', trim($dir, '/')).'/';
		}
		else
		{
			$this->directory .= str_replace('.', '', trim($dir, '/')).'/';
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch directory
	 *
	 * Feches the sub-directory (if any) that contains the requested
	 * controller class.
	 *
	 * @deprecated	3.0.0	Read the 'directory' property instead
	 * @return	string
	 */
	public function fetch_directory()
	{
		return $this->directory;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Build reverse routes
	 * 
	 * @return void
	 * @author Kenny Katzgrau
	 * @link http://codefury.net/2010/09/easy-reverse-routing-with-codeigniter/
	 */
	protected function _build_reverse_routes()
	{
		$reverse_routes = array();

		foreach($this->routes as $route => $info)
		{
			// If this is a default route or scaffolding key, ignore it
			if(!is_array($info) || !isset($info[self::ARR_ROUTE_NAME])) continue;

			$reverse_routes[$info[self::ARR_ROUTE_NAME]] = $route;
		}

		return $this->_reverse_routes =& $reverse_routes;
	}

	// --------------------------------------------------------------------

	/**
	 * Get list of all reverse routes
	 * 
	 * @return array
	 */
	public function get_reverse_routes()
	{
		if($this->_reverse_routes === NULL)
			$this->_build_reverse_routes();

		return $this->_reverse_routes;
	}

	// --------------------------------------------------------------------

	/**
	 * Reverse routes
	 * 
	 * @param string $route_name
	 * @param array $args_keyval
	 * @return void
	 * @author Kenny Katzgrau
	 * @link http://codefury.net/2010/09/easy-reverse-routing-with-codeigniter/
	 */
	public function reverse_route($route_name, $args_keyval = array())
	{
		if($this->_reverse_routes === NULL)
			$this->_build_reverse_routes();

		if(!array_key_exists($route_name, $this->_reverse_routes))
		{
			trigger_error("No reverse route found for '$route_name'", E_USER_WARNING);
			return NULL;
		}

		$route = $this->_reverse_routes[$route_name];

		if(is_array($args_keyval) && count($args_keyval) > 0)
		{
			foreach($args_keyval as $key => $val)
			{
				$route = str_replace("(:$key)", $val, $route);
			}
		}

		// Remove missed arguments
		return preg_replace('/\(:[a-zA-Z]+\)/', '', $route); 
	}

	// --------------------------------------------------------------------

	/**
	 * Get Current Params values
	 * 
	 * @return array|NULL
	 */
	public function route_params()
	{
		// Turn the segment array into a URI string
		$uri = implode('/', $this->uri->segments);

		// Get the current route
		if( !isset($this->routes[$this->current_route_uri]) )
			return NULL;

		$route = $this->routes[$this->current_route_uri];
		$key = $this->current_route_uri;

		// Convert wild-cards to RegEx
		if(is_array($route) && isset($route[self::ARR_ROUTE_REGEX]))
		{
			foreach($route[self::ARR_ROUTE_REGEX] as $paramKey => $paramRegex)
			{
				$key = str_replace(':'.$paramKey, $paramRegex, $key);
			}
		}
		elseif (strpos($key, ':num') || strpos($key, ':any') )
		{
			$key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);
		}
		else
		{
			$key = preg_replace('/\:[a-z_]+/', '[^/]+', $key);
		}

		// Regex with current route and get params
		if(isset($route[self::ARR_ROUTE_REGEX]) && preg_match('#^'.$key.'$#', $uri, $matches))
		{
			$params = array();
			$i = 1;

			foreach($route[self::ARR_ROUTE_REGEX] as $paramKey => $paramRegex)
			{
				if(isset($matches[$i]))
					$params[$paramKey] = $matches[$i];
				$i++;
			}

			return $params;
		}
		else
		{
			return NULL;
		}

	}

}
/* End of file Router.php */
/* Location: ./system/core/Router.php */