<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extended CI_Input class
 * 
 * @package     CodeIgniter
 * @subpackage  Input
 * @category    Input
 * @author      Ellislab 
 */
class MY_Input extends CI_Input {

	/**
	 * Get only given keys from $_GET
	 * 
	 * @param  array $keys [description]
	 * @param  string $as - ['array','string']
	 * @return mixed
	 */
	public function get_only($keys, $as = 'array')
	{
		$gets = $this->get();

		$new_gets = array();
		foreach ($gets as $key => $value)
		{
			
			if ( in_array($key, $keys) )
			{
				switch ($as)
				{
					case 'string':
						$new_gets[] = $key.'='.$value;
						break;
					case 'array':
						return $new_gets[$key] = $value;
						break;
					default:
						return NULL;
						break;
				}
			}
		}

		switch ($as)
		{
			case 'string':
				return implode('&', $new_gets);
				break;
			case 'array':
				return $new_gets;
				break;
			default:
				return NULL;
				break;
		}

	}

	/**
	 * Get all except given keys from $_GET
	 * 
	 * @param  array $keys [description]
	 * @param  string $as - ['array','string']
	 * @return mixed
	 */
	public function get_expect($keys, $as = 'array')
	{
		$gets = $this->get();

		$new_gets = array();
		foreach ($gets as $key => $value)
		{			
			if ( ! in_array( $key, $keys ) )
			{
				switch ( $as )
				{
					case 'string':
						$new_gets[] = $key.'='.$value;
						break;
					case 'array':
						return $new_gets[$key] = $value;
						break;
					default:
						return NULL;
						break;
				}
			}
		}

		switch ( $as ) {
			case 'string':
				return implode( '&', $new_gets );
				break;
			case 'array':
				return $new_gets;
				break;
			default:
				return NULL;
				break;
		}
	}

}
/* End of file MY_Input.php */
/* Location: ./application/core/MY_Input.php */ 