<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Language Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Language
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/language.html
 */
class CI_Lang {

	/**
	 * List of available languages
	 *
	 * @var	array
	 */
	public $languages =	array();

	/**
	 * List of translations
	 *
	 * @var	array
	 */
	public $translations =	array();

	/**
	 * List of loaded language files
	 *
	 * @var	array
	 */
	public $is_loaded =	array();

	/**
	 * Current Language
	 * 
	 * @var string
	 */
	protected $current_language;

	/**
	 * Current Language Code
	 * 
	 * @var string
	 */
	protected $current_language_code;

	/**
	 * Default Language
	 * 
	 * @var string
	 */
	protected $default_language;

	/**
	 * Default Language Code
	 * 
	 * @var string
	 */
	protected $default_language_code;


	/**
	 * Set globals in properties
	 */
	protected $config;
	protected $uri;
	protected $router;


	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		global $CFG;
		global $URI;
		global $RTR;

		// Set Them in Property
		$this->config	=& $CFG;
		$this->uri		=& $URI;
		$this->router	=& $RTR;

		// Set the default langs
		$this->languages = $this->config->item('languages');
		$this->current_language = $this->config->item('language');
		$this->current_language_code = array_search($this->current_language, $this->languages);
		$this->default_language_code = $this->config->item('default_lang_code');
		$this->default_language = $this->languages[$this->default_language_code];
	}

	// --------------------------------------------------------------------

	/**
	 * Load a language file
	 *
	 * @param	mixed	$langfile	Language file name
	 * @param	string	$idiom		Language name (english, etc.)
	 * @param	bool	$return		Whether to return the loaded array of translations
	 * @param 	bool	$add_suffix	Whether to add suffix to $langfile
	 * @param 	string	$alt_path	Alternative path to look for the language file
	 *
	 * @return	void|string[]	Array containing translations, if $return is set to TRUE
	 */
	public function load($langfile, $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '')
	{
		$langfile = str_replace('.php', '', $langfile);

		if ($add_suffix === TRUE)
		{
			$langfile = str_replace('_lang', '', $langfile).'_lang';
		}

		$langfile .= '.php';

		if (empty($idiom) OR ! ctype_alpha($idiom))
		{
			$config =& get_config();
			$idiom = empty($config['language']) ? 'english' : $config['language'];
		}

		if (!isset($this->is_loaded[$idiom]))
		{
			$this->is_loaded[$idiom] = array();	
		}
		
		if ($return === FALSE && isset($this->is_loaded[$idiom][$langfile]) && $this->is_loaded[$idiom][$langfile] === $idiom)
		{
			return;
		}

		// Load the base file, so any others found can override it
		$basepath = BASEPATH.'language/'.$idiom.'/'.$langfile;
		if (($found = file_exists($basepath)) === TRUE)
		{
			include($basepath);
		}

		// Do we have an alternative path to look in?
		if ($alt_path !== '')
		{
			$alt_path .= 'language/'.$idiom.'/'.$langfile;
			if (file_exists($alt_path))
			{
				include($alt_path);
				$found = TRUE;
			}
		}
		else
		{
			foreach (get_instance()->load->get_package_paths(TRUE) as $package_path)
			{
				$package_path .= 'language/'.$idiom.'/'.$langfile;
				if ($basepath !== $package_path && file_exists($package_path))
				{
					include($package_path);
					$found = TRUE;
					break;
				}
			}
		}

		if ($found !== TRUE)
		{
			show_error('Unable to load the requested language file: language/'.$idiom.'/'.$langfile);
		}

		if ( ! isset($lang) OR ! is_array($lang))
		{
			log_message('error', 'Language file contains no data: language/'.$idiom.'/'.$langfile);

			if ($return === TRUE)
			{
				return array();
			}
			return;
		}

		if ($return === TRUE)
		{
			return $lang;
		}

		$this->is_loaded[$idiom][$langfile] = $idiom;

		if (!isset($this->translations[$idiom]))
		{
			$this->translations[$idiom] = array();
		}

		$this->translations[$idiom] = array_merge($this->translations[$idiom], $lang);

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Language line
	 *
	 * Fetches a single line of text from the language array
	 *
	 * @param	string	$line		Language line key
	 * @param	bool	$log_errors	Whether to log an error message if the line is not found
	 * @return	string	Translation
	 */
	public function line($line, $log_errors = TRUE)
	{
		$value = isset($this->translations[$this->current_language][$line])
					? $this->translations[$this->current_language][$line] : FALSE;

		// Because killer robots like unicorns!
		if ($value === FALSE && $log_errors === TRUE)
		{
			log_message('error', 'Could not find the language line "'.$line.'"');
		}

		return $value;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Translate a Language line
	 *
	 * @param	string	$line		Language line key
	 * @param	string	$idiom		Language name
	 * @param	bool	$log_errors	Whether to log an error message if the line is not found
	 * @return	string	Translation
	 * @author	A. H. Abid <a_h_abid@hotmail.com>
	 */
	public function translate($line, $idiom = '', $log_errors = TRUE)
	{
		if (empty($idiom) OR ! ctype_alpha($idiom))
		{
			$config =& get_config();
			$idiom = empty($config['language']) ? 'english' : $config['language'];
		}
		$value = isset($this->translations[$idiom][$line]) ? $this->translations[$idiom][$line] : FALSE;
		
		// Because killer robots like unicorns!
		if ($value === FALSE && $log_errors === TRUE)
		{
			log_message('error', 'Could not find the translation line "'.$line.'" for language "'.$idiom.'"');
		}

		return $value;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Current Language in Key/Value Pair
	 * 
	 * @return array
	 */
	public function current_pair()
	{
		return array($this->current_language_code => $this->current_language);
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Current Language
	 * 
	 * @return string
	 */
	public function current_language()
	{
		return $this->current_language;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Current Language Code
	 * 
	 * @return string
	 */
	public function current_code()
	{
		return $this->current_language_code;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Default Language in Key/Value Pair
	 * 
	 * @return array
	 */
	public function default_pair()
	{
		return array($this->default_language_code => $this->default_language);
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Default Language
	 * 
	 * @return string
	 */
	public function default_language()
	{
		return $this->default_language;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Default Language Code
	 * 
	 * @return string
	 */
	public function default_code()
	{
		return $this->default_language_code;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Change Current Lang via LangCode or Language name
	 *
	 * @param  string	$lang
	 */
	public function change($lang)
	{
		if( in_array($lang, $this->languages) )
		{
			$this->_change_language($lang);
		}
		elseif( in_array($lang, array_keys($this->languages)) )
		{
			$this->_change_language($this->languages[$lang]);
		}
		else
		{
			show_error('Unable to find your requested language. :: '.$lang);
		}
	}

	
	// --------------------------------------------------------------------
	
	/**
	 * Get Language Based URI, Optional provide lang_code
	 * 
	 * @param  string $uri
	 * @param  string $lang_code
	 * @return string
	 */
	public function get_lang_uri($uri = '', $lang_code = '')
	{
		if ($lang_code === '')
		{
			$lang_code = $this->current_language_code;
		}

		if ($uri == '')
		{
			return $lang_code;
		}

		$segments = explode('/', trim($uri,'/'));

		if ( !in_array($segments[0],array_keys($this->languages)) )
		{
			array_unshift($segments, $lang_code);
		}

		return implode('/', $segments);
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get Non-Language Based URI
	 * 
	 * @param  string $uri  [description]
	 * @param  string $lang [description]
	 * @return string       [description]
	 */
	public function get_non_lang_uri($uri = '')
	{
		if ($uri == '')
		{
			return $uri;
		}

		$segments = explode('/', trim($uri,'/'));

		if ( in_array($segments[0],array_keys($this->languages)) )
		{
			array_shift($segments);
		}

		return implode('/', $segments);
	}


	// --------------------------------------------------------------------
	
	/**
	 * Change the language
	 * 
	 * @param  string $language
	 */
	protected function _change_language($language)
	{
		// 1st change in config
		$this->config->set_item('language',$language);

		// Now Change in property
		$this->current_language			= $language;
		$this->current_language_code	= array_search($language, $this->languages);
	}



}
/* End of file Lang.php */
/* Location: ./system/core/Lang.php */