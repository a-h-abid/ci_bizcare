<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extend of the main CI Controller to have more functions and flexibility
 * 
 * @package     CodeIgniter
 * @subpackage  Controller
 * @category    Controller
 * @author      Phil Sturgeon
 * @link        http://philsturgeon.co.uk/blog/2010/02/CodeIgniter-base-Classes-Keeping-it-DRY
 */
class MY_Controller extends CI_Controller {

	/**
	 * Common Data Var
	 * 
	 * @var array
	 */
	public $data = array();
	
	/**
	 * Display Limit
	 * 
	 * @var array
	 */
	public $display_limit = 10;

	/**
	 * Name of theme
	 * 
	 * @var string
	 */
	protected $_theme = '';

	/**
	 * Master Layout Location
	 * 
	 * @var string
	 */
	protected $_layout = '_layout/master';

	/**
	 * Session Key
	 * 
	 * @var string
	 */
	protected $_auth_key = '';

	/**
	 * Constructor Function
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// ========================================================================================

	/**
	 * Render our basic template
	 * 
	 * @param  string $view_file  the view file
	 * @param  string $page_title page title
	 */
	public function render($view_file, $page_title = 'Untitled')
	{
		if( !isset($this->data['meta_description']))
			$this->data['meta_description'] = $this->config->item('meta_description');
		if( !isset($this->data['meta_author']))
			$this->data['meta_author'] = $this->config->item('meta_author');
		if( !isset($this->data['meta_keywords']))
			$this->data['meta_keywords'] = $this->config->item('meta_keywords');
		if( !isset($this->data['body_class']))
			$this->data['body_class'] = '';
		
		$this->data['page_title'] = $page_title;		
		$this->data['sub_view'] = $view_file;
		$this->data['CI']	=& $this;
		
		$this->load->view( ($this->_theme != '' ? $this->_theme.'/' : '') . $this->_layout, $this->data);
	}

	// ---------------------------------------------------------------------------------------
	
	/**
	 * Load a view file within theme folder
	 * 
	 * @param  string $view_file [description]
	 * @param  array  $data      [description]
	 */
	public function theme_view($view_file, $data = array())
	{
		$this->load->view($this->_theme.'/'.$view_file,$data);
	}


	// ---------------------------------------------------------------------------------------
		
	/**
	 * Add Extra scripts at botton of template
	 * 
	 * @param  string $string [description]
	 */
	public function extra_scripts($string)
	{
		if(!isset($this->data['scripts_extra']))
			$this->data['scripts_extra'] = '';

		$this->data['scripts_extra'] .= $string;
	}

	// ---------------------------------------------------------------------------------------

	/**
	 * Lazy Load Model and get it's object
	 * 
	 * @param  string $model_name
	 * @return CI_Model
	 */
	public function model($model_name)
	{
		$model_name = str_replace(array('_model'),'',$model_name).'_model';
		
		if( !class_exists(ucfirst($model_name)))
		{
			$this->load->model($model_name);
		}

		return $this->{$model_name};
	}

	// ---------------------------------------------------------------------------------------

	/**
	 * Override output for data analyzing
	 *
	 * - For sending the regenerated CSRF tokens with ajax
	 * 
	 * @param  string $output - the output to be sent to browser
	 * @return string
	 */
	public function _output($output)
	{
		if( $this->input->is_ajax_request() )
		{
			if($this->output->get_content_type() == 'application/json' 
				&& isset($_SERVER['REQUEST_METHOD'])
				&& strtolower($_SERVER['REQUEST_METHOD']) == 'post')
			{
				$outputArray = (array) json_decode($output,TRUE);

				// Get CSRF
				$outputArray[$this->config->item('csrf_cookie_name')] = $this->security->get_csrf_hash();

				$output = json_encode($outputArray);
			}
		}
		elseif ( !$this->input->is_cli_request() )
		{
			// Write Cache for normal HTTP Request if cache is set
			if ($this->output->cache_expiration > 0)
			{
				$this->output->_write_cache($output);
			}
		}

		return $output;
	}

	// ---------------------------------------------------------------------------------------

	/**
	 * Get the auth key for current instance
	 * 
	 * @return string
	 */
	public function get_auth_key()
	{
		return $this->_auth_key;
	}

}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */