<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MY Model for extended model funcitonality
 *	
 * @dependency CI_Form_validation & form_helper
 * @author A. H. Abid <a_h_abid@hotmail.com>
 */
class MY_Model extends CI_Model {

	/**
	 * Table fields with validation rules
	 * @var array
	 */
	public $fields = array();

	/**
	 * Table name is stored here
	 * @var string
	 */
	protected $_table;
	
	/**
	 * TBL Key defined at config/db_tables.php
	 * @var [type]
	 */
	protected $_tbl_key;

	/**
	 * Set if validation will validate all fields or only $_POST data.
	 * FALSE for POST only.
	 */
	protected $_validate_all_fields = TRUE;

	/**
	 * Primary key of this table. Array for multiple primary keys
	 * @var string|array
	 */
	protected $_primary_key = 'id';

	/**
	 * Just to contain query builder calls.
	 * @var array
	 */
	protected $_qb_container = array();


	/**
	 * Optionally skip the validation. Used in conjunction with
	 * skip_validation() to skip data validation for any future calls.
	 */
	protected $_skip_validation = FALSE;

	/**
	 * Should the table will timestamps or not? ("created_on","updated_on")
	 */
	protected $_timestamps = FALSE;


	/**
	 * Return as Object or Array
	 * @var string
	 */
	protected $_return_type = 'array';

	/**
	 * Flag for Return as QueryObject, SQL or JSON
	 * @var boolean
	 */
	protected $_return_query_object = FALSE;
	protected $_return_sql = FALSE;
	protected $_return_json = FALSE;


	/**
	 * Flag for Return insert ID for insert
	 * @var boolean
	 */
	protected $_return_insert_id = TRUE;

	/**
	 * Return as Key Value Pair
	 * @var array
	 */
	protected $_return_pair = array();
	
	/**
	 * Set pair type ['simple','group']
	 * @var string
	 */
	protected $_return_pair_type = 'simple';

	/**
	 * Reset State after Query
	 * @var boolean
	 */
	protected $_reset_after_query = TRUE;

	/**
	 * Return data with Pagination Setting
	 * @var array
	 */
	protected $_with_pagination = FALSE;

	/**
	 * Support for soft deletes and this model's 'deleted' key
	 */
	protected $_soft_delete = FALSE;
	protected $_soft_delete_key = 'deleted';
	protected $_temporary_with_deleted = FALSE;
	protected $_temporary_only_deleted = FALSE;

	/**
	 * The various callbacks available to the model. Each are
	 * simple lists of method names (methods will be run on $this).
	 */
	protected $_before_create = array();
	protected $_after_create = array();
	protected $_before_update = array();
	protected $_after_update = array();
	protected $_before_get = array();
	protected $_after_get = array();
	protected $_before_delete = array();
	protected $_after_delete = array();
	protected $_before_validation_rules = array();
	protected $_after_validation_rules = array();
	protected $_before_validation = array();
	protected $_after_validation = array();


	/**
	 * Created & Updated datetime field names
	 */
	protected $_created_on_key = 'created_on';
	protected $_updated_on_key = 'updated_on';


	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// ======================================================================================================
	// GENERAL USAGE METHODS
	// ======================================================================================================

	/**
	 * Get a row by primary value
	 * 
	 * @param  string|array $primary_value
	 * @param  string $alias table_name alias
	 * @return array|object
	 */
	public function get($primary_value, $alias = '')
	{
		if( is_string($this->_primary_key) )
		{
			$where = array( ($alias != '' ? $alias.'.': '' ).$this->_primary_key => $primary_value);
		}
		elseif( is_array($this->_primary_key) )
		{
			$where = array();
			$primary_value = is_string($primary_value) ? explode(',', $primary_value) : $primary_value;
			for ($i=0, $c = count($this->_primary_key); $i < $c; $i++)
			{ 
				if(isset($primary_value[$i]))
				{
					$where[($alias != '' ? $alias.'.': '' ).$this->_primary_key[$i]] = $primary_value[$i];
				}
			}
		}
		
		return $this->get_by($where, $alias);
	}

	/**
	 * Get a row by WHERE
	 * 
	 * @param  array $where
	 * @param  string $alias table_name alias
	 * @return array|object
	 */
	public function get_by($where, $alias = '')
	{
		$this->db->from($this->_table.' '.$alias);
		
		foreach ($where as $wkey => $wvalue)
		{
			if(is_array($wvalue))
			{
				$this->db->where_in($wkey, $wvalue);
			}
			else
			{
				$this->db->where($wkey, $wvalue);
			}
		}

		$this->limit(1);
		
		return $this->_get_data($alias,'row');
	}


	/**
	 * Get Many results by primary values 
	 * 
	 * @param  array $values [description]
	 * @param  string $alias  [description]
	 * @return array         [description]
	 */
	public function get_many($values, $alias = '')
	{
		if(!is_array($values))
			show_error('get_many() $values parameter must be in array');

		if(is_string($this->_primary_key))
		{
			$this->where_in(($alias != '' ? $alias.'.': '' ).$this->_primary_key, $values);
		}
		elseif(is_array($this->_primary_key))
		{
			for($i=0,$c=count($this->_primary_key);$i<$c;$i++)
			{
				if(isset($values[$this->_primary_key[$i]]))
				{
					$this->where_in(($alias != '' ? $alias.'.': '' ).$this->_primary_key,$values[$this->_primary_key]);
				}
			}
		}
		
		return $this->get_all($alias);
	}

	/**
	 * Get many by provided where condition
	 * @param  array $where
	 * @param  string $alias
	 * @return array
	 */
	public function get_many_by($where, $alias = '')
	{
		$this->where($where);
		return $this->get_all($alias);
	}


	/**
	 * Get all table data. Can be combiled with other queries
	 * 
	 * @param  string $alias - table name alias
	 * @return array
	 */
	public function get_all($alias = '')
	{
		$this->db->from($this->_table.' '.$alias);
		return $this->_get_data($alias,'result');
	}

	/**
	 * Get a single column data as array
	 * 
	 * @param  string $field
	 * @param  string $alias
	 * @return array
	 */
	public function get_column($field, $alias = '')
	{
		$this->db->select($field)->from($this->_table.' '.$alias);

		$data = $this->_get_data($alias,'result');

		if (!is_array($data) || count($data) < 1)
			return array();

		// Extract field name from table.field
		$field = explode('.', $field);
		$field = end($field);

		$column = array();
		foreach ($data as $item)
		{
			if (is_object($item))
			{
				$column[] = $item->$field;
			}
			else
			{
				$column[] = $item[$field];
			}
		}

		return $column;
	}

	/**
	 * Insert a new row into the table. $data should be an associative array
	 * of data to be inserted. Returns newly created ID.
	 *
	 * @param array $data
	 * @param bool $escape
	 * @return int|bool
	 */
	public function insert($data, $escape = NULL)
	{
		$data = $this->validate($data);

		if( $data === FALSE )
		{
			return FORM_VALIDATION_ERROR;
		}

		$data = $this->trigger('before_create', $data);

		if($this->_timestamps == TRUE)
		{
			$data = $this->created_on($data);
		}

		$this->db->set($data, '', $escape);

		if ($this->_return_sql)
		{
			// Return an SQL statement as a result.
			return $this->get_compiled_insert($this->_table);
		}

		$this->db->insert($this->_table);

		$insert_id = $this->db->insert_id();

		$this->trigger('after_create', $insert_id);

		if($this->db->affected_rows() > 0)
		{
			return ($this->_return_insert_id) ? $insert_id : TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Insert multiple rows into the table. Returns an array of multiple IDs.
	 *
	 * @param array $data
	 * @param bool $escape
	 * @return array - 'result' => int|bool (return value from $this->insert()), 'errors' => array (validation errors)
	 */
	public function insert_many($data, $escape = NULL)
	{
		$ids = array();

		foreach ($data as $key => $row)
		{
			$ids[] = array(
				'result' => $this->insert($row, $escape),
				'errors' => $this->form_validation->error_array(),
			);
		}

		return $ids;
	}

	/**
	 * Insert in Batch. No validation or Event Trigger will work here
	 * @param  array $data [description]
	 * @return int - affected_rows
	 */
	public function insert_batch($insert_set)
	{
		if( !(isset( $insert_set[0] ) && is_array( $insert_set[0] )) )
		{
			show_error('insert_batch() data must be multi-duimentional array');
		}

		$validation_status = array();
		if ($this->_skip_validation === FALSE)
		{
			foreach($insert_set as $key => $row)
			{
				$insert_set[$key] = $this->validate($row);
				if($insert_set[$key] === FALSE)
				{
					$validation_status[$key] = $this->form_validation->error_array();
				}
			}
		}

		if(count($validation_status) > 0)
		{
			return $validation_status;
		}

		foreach($insert_set as $key => $row)
		{
			$row = $this->trigger('before_create', $row);
			
			if($this->_timestamps == TRUE)
			{
				$row = $this->created_on($row);
				$row = $this->updated_on($row);
				
				$insert_set[$key] = $row;
			}
			
		}

		$result = $this->db->insert_batch($this->_table,$insert_set);

		return $result;
	}


	/**
	 * Insert data and on uplicate key update them.
	 * Form validation is not used here.
	 * Single set or Multi bulk set, both of them works.
	 * 
	 * @param array $update_set
	 * @param array $insert_set
	 * @return mixed
	 * @author A H Abid
	 */
	public function insert_duplicate_update($update_set, $insert_set)
	{
		if(!is_array($update_set) || !is_array($insert_set))
		{
			trigger_error('Insert & Update set must be array',E_USER_ERROR);
			return NULL;
		}

		$multi_duimentional = ( isset( $insert_set[0] ) && is_array( $insert_set[0] ) );

		$validation_status = array();
		if ($this->_skip_validation === FALSE)
		{
			if($multi_duimentional === TRUE)
			{
				foreach($insert_set as $key => $row)
				{
					$insert_set[$key] = $this->validate($row);
					if($insert_set[$key] === FALSE)
					{
						$validation_status[$key] = $this->form_validation->error_array();
					}
				}
			}
			else
			{
				$insert_set = $this->validate($insert_set);
				if($insert_set === FALSE)
				{
					$validation_status[] = $this->form_validation->error_array();
				}
			}
			
			if(count($validation_status) > 0)
			{
				return $validation_status;
			}
		}


		if($this->_timestamps == TRUE)
		{
			if($multi_duimentional === TRUE)
			{
				foreach($insert_set as $key => $row)
				{
					$row = $this->trigger('before_create', $row);

					$row = $this->created_on($row);
					$row = $this->updated_on($row);
					
					$row = $this->trigger('after_create', $row);

					$insert_set[$key] = $row;
				}
			}
			else
			{
				$insert_set = $this->trigger('before_create', $insert_set);
				
				$insert_set = $this->created_on($insert_set);
				$insert_set = $this->updated_on($insert_set);  
				
				$insert_set = $this->trigger('after_create', $insert_set);
			}
		}

		return $this->db->on_duplicate_update($this->_table, $update_set, $insert_set, $this->_return_sql);
	}

	/**
	 * Save data to DB, Will Insert of Update
	 * 
	 * @param  int $id   [description]
	 * @param  arrat $data [description]
	 * @return mixed       int if saved, FALSE or FORM_VALIDATION_ERROR if failed
	 */
	public function save($id, $data)
	{
		return ($id == '') ? $this->insert($data) : $this->update($id,$data);
	}

	/**
	 * Updated single or multiple rows by primary keys with similar data.
	 * 
	 * @param mixed $primary_value
	 * @param array $data
	 * @param boolean $skip_validation
	 * @param boolean $sql_string
	 * @return boolean
	 * @author A H Abid
	 */
	public function update($primary_value, $data)
	{
		return $this->update_by(array($this->_primary_key => $primary_value), $data);	
	}


	/**
	 * Updated a record based on an arbitrary WHERE clause.
	 *
	 * @param array $where
	 * @param array $data
	 * @param boolean $skip_validation
	 * @param boolean $sql_string
	 * @return boolean
	 * @author A H Abid
	 */
	public function update_by($where, $data)
	{
		if ($this->_skip_validation === FALSE)
		{
			$data = $this->validate($data);
		}

		if ($data === FALSE)
		{
			return FORM_VALIDATION_ERROR;
		}

		$data = $this->trigger('before_update', $data);

		if ($this->_timestamps == TRUE)
		{
			$data = $this->updated_on($data);
		}

		$this->_check_soft_delete();

		foreach ($where as $wkey => $wvalue)
		{
			if ( is_array($wvalue) )
			{
				$this->db->where_in($wkey, $wvalue);
			}
			else
			{
				$this->db->where($wkey, $wvalue);
			}
		}

		$this->db->set($data);

		$sql = $this->db->get_compiled_update($this->_table, TRUE);
		if($this->_return_sql === TRUE)
		{
			return $sql;
		}

		$result = $this->db->query($sql);
		
		$this->trigger('after_update', $data);

		return $result;
	}


	/**
	 * Delete a row from the table by the primary value
	 * 
	 * @param int|string $id
	 * @param boolean $sql_string
	 * @return boolean
	 * @author A H Abid
	 */
	public function delete($id)
	{
		return $this->delete_by(array($this->_primary_key => $id));
	}

	/**
	 * Delete a row from the database table by an arbitrary WHERE clause
	 * 
	 * @param array $where
	 * @param boolean $sql_string
	 * @return boolean
	 * @author A H Abid
	 */
	public function delete_by($where)
	{
		foreach ($where as $wkey => $wvalue)
		{
			if(is_array($wvalue))
			{
				$this->db->where_in($wkey, $wvalue);
			}
			else
			{
				$this->db->where($wkey, $wvalue);
			}
		}

		if ($this->_soft_delete)
		{
			$this->db->set($this->_soft_delete_key, TRUE);
			$sql = $this->db->get_compiled_update($this->_table, TRUE);
		}
		else
		{
			$sql = $this->db->get_compiled_delete($this->_table, TRUE);
		}

		if($this->_return_sql === TRUE)
		{
			return $sql;
		}

		$result = $this->db->query($sql);

		return ( $this->db->affected_rows() > 0 || $result );
	}
  
  
	/**
	 * Delete rows by joining with other tables. use the join(), join_by()
	 * to join tables.
	 *
	 * @param array $where
	 * @param string $alias
	 * @param boolean $sql_string
	 * @return boolean
	 * @todo  Fix Bugs
	 */
	public function delete_join($where = array(), $alias = '')
	{
		$this->db->from($this->_table.' '.$alias);
		if(count($this->join_stores) === 0)
		{
			trigger_error('Cannot proceed join delete. No join statement found ', E_USER_ERROR);
			return FALSE;
		}
		$this->_join_stores();

		if ($this->soft_delete)
		{
			$this->db->set($this->soft_delete_key_full, TRUE);
			$sql = $this->db->update_join('','',$where, TRUE, TRUE);
		}
		else
		{
			$sql = $this->db->delete_join('',$where,TRUE, TRUE);
		}

		if($this->_return_sql === TRUE)
		{
			return $sql;
		}

		$result = $this->db->query($sql, FALSE, FALSE);
		
		return ( $this->db->affected_rows() > 0 || $result ); 
	}

	// ======================================================================================================
	// UTILITIES
	// ======================================================================================================

	/**
	 * Get Fields denifed in model in blank value for form
	 * 
	 * return array|object
	 */
	public function get_blank_fields($return_type = 'array')
	{
		$data = array();
		if( isset($this->fields) && count($this->fields) > 0)
		{
			foreach($this->fields as $itemkey => $itemval )
			{
				$data[$this->fields[$itemkey]['field']] = '';
			}
		}
		return ($return_type == 'object') ? (object) $data : $data ;
	}

	/**
	 * Get Status of $_soft_delete
	 * @return bool
	 */
	public function soft_delete()
	{
		return $this->_soft_delete;
	}

	/**
	 * Get Key for $_soft_delete_key
	 * @return string
	 */
	public function soft_delete_key()
	{
		return $this->_soft_delete_key;
	}

	/**
	 * Getter for the $_temporary_with_deleted
	 * 
	 * @return bool
	 */
	public function get_temporary_with_deleted()
	{
		return $this->_temporary_with_deleted;
	}

	/**
	 * Getter for the $_temporary_only_deleted
	 * 
	 * @return bool
	 */
	public function get_temporary_only_deleted()
	{
		return $this->_temporary_only_deleted;
	}

	/**
	 * Fetch a count of rows based on an arbitrary WHERE.
	 * 
	 * @param array $where;
	 * @return int
	 */
	public function count_by($where)
	{
		$this->db->where($where);
		return $this->db->count_all_results($this->_table);
	}

	/**
	 * Fetch a total count of rows, with possible option
	 * to reset previous queries.
	 * 
	 * @param boolean $reset
	 * @return int
	 */
	public function count_all($reset = FALSE)
	{
		if ($reset === TRUE) $this->db->reset_query();
		return $this->db->count_all_results($this->_table);
	}

	/**
	 * Count by Search
	 * 
	 * @param string $type
	 * @param string $string
	 * @return int
	 * @author A H Abid
	 * @deprecated 2013-Jun-19, Use $this->db->count_last_query instead
	 */
	public function count_by_search($type, $string)
	{
		$this->db->select('COUNT(*) AS count');
		$this->db->from($this->_table);
		$this->db->where($type,$string);
		$this->db->or_where($type.' LIKE','%'.$string.'%');

		$query = $this->db->get();
		return $query->row()->count;
	}


	// ======================================================================================================
	// OPTIONS
	// ======================================================================================================
	
	/**
	 * Set Return type as array
	 * @return CI_Model
	 */
	public function as_array()
	{
		$this->_return_type = 'array';
		return $this;
	}

	/**
	 * Set Return type as object
	 * @return CI_Model
	 */
	public function as_object()
	{
		$this->_return_type = 'object';
		return $this;
	}

	/**
	 * Set Return type as query_object
	 * @return CI_Model
	 */
	public function as_query_object($bool = TRUE)
	{
		$this->_return_query_object = $bool;
		return $this;
	}

	/**
	 * Set Return type as key:value array pair
	 * @return CI_Model
	 */
	public function as_pair($key_field, $value_field)
	{
		$this->_return_pair = array('key' => $key_field, 'value' => $value_field);
		return $this;
	}

	/**
	 * Set Return pair type ['simple','group']
	 * @return CI_Model
	 */
	public function pair_type($type)
	{
		if (!in_array($type, array('simple','group')))
			show_error('Invalid Pair Type Selected');

		$this->_return_pair_type = $type;
		return $this;
	}

	/**
	 * Set Return the sql only
	 * @return CI_Model
	 */
	public function as_sql($bool = TRUE)
	{
		$this->_return_sql = $bool;
		return $this;
	}

	/**
	 * Set Return type as array
	 * @return CI_Model
	 */
	public function as_json($bool = TRUE)
	{
		$this->_return_json = $bool;
		return $this;
	}

	/**
	 * Don't care about soft deleted rows on the next call
	 */
	public function with_deleted()
	{
		$this->_temporary_with_deleted = TRUE;
		return $this;
	}

	/**
	 * Only get deleted rows on the next call
	 */
	public function only_deleted()
	{
		$this->_temporary_only_deleted = TRUE;
		return $this;
	}

	/**
	 * Reset all states after query
	 * @param  bool $bool
	 * @return CI_Model
	 */
	public function reset_after_query($bool)
	{
		$this->_reset_after_query = $bool;
		return $this;
	}

	/**
	 * Tell the class to skip the insert validation
	 *
	 * @bool bool - default TRUE
	 * @return CI_Model
	 */
	public function skip_validation($bool = TRUE)
	{
		$this->_skip_validation = $bool;
		return $this;
	}

	/**
	 * Set if validate all fields given in $fields or
	 * only validate fields given in data
	 * @param  boolean $bool [description]
	 * @return [type]        [description]
	 */
	public function validate_all_fields($bool = TRUE)
	{
		$this->_validate_all_fields = $bool;
		return $this;
	}


	// ======================================================================================================
	// GENERATORS
	// ======================================================================================================


	/**
	 * Runs the prefix based SQL ID and returns the result
	 * 
	 * @param string $prefix
	 * @param int $padding
	 * @param string $field
	 * @param string $table
	 * @param boolean $sql_string
	 * @return string
	 * @author A H Abid
	 */
	public function generate_prefix_code($prefix = '', $padding = '', $field = '', $table ='', $sql_string = FALSE)
	{
		$prefix		= ($prefix == '')  ? $this->prefix_pk_name : $prefix;
		$padding	= ($padding == '') ? $this->prefix_pk_padding : $padding;
		$table		= ($table == '')   ? $this->db->dbprefix . $this->_table : $this->db->dbprefix . $table;
		$field		= ($field == '')   ? $this->get_primary_key() : $field;

		$select_sql   = $this->prefix_code_sql($table, $field, $prefix, $padding);
		$full_sql     = "SELECT {$select_sql} AS `code` FROM `{$table}` WHERE `{$table}`.`{$field}` LIKE '{$prefix}%' ";    
		if($sql_string === TRUE)
		{
			return $full_sql;
		}

		$query       = $this->db->query($full_sql);
		$row         = $query->row();
		$query->free_result();

		return isset($row->code) ? $row->code : '';
	}


	// ======================================================================================================
	// INFORMATION
	// ======================================================================================================


	/**
	 * Return the next auto increment of the table. Only tested on MySQL.
	 */
	public function get_next_id()
	{
		return (int) $this->db->select('AUTO_INCREMENT')
			->from('information_schema.TABLES')
			->where('TABLE_NAME', $this->_table)
			->where('TABLE_SCHEMA', $this->db->database)->get()->row()->AUTO_INCREMENT;
	}

	/**
	 * Get the Table Key
	 * 
	 * @return string
	 */
	public function get_table_key()
	{
		return $this->_tbl_key;
	}

	/**
	 * Get the Table Name
	 * 
	 * @return string
	 */
	public function get_table_name()
	{
		return $this->_table;
	}

	/**
	 * Get the Primary Key(s)
	 * 
	 * @return string|array
	 */
	public function get_primary_key()
	{
		return $this->_primary_key;
	}

	// ======================================================================================================
	// DB Query Builder Wrappers
	// ======================================================================================================

	public function select($select, $escape = NULL)
	{
		if(!isset($this->_qb_container['selects']))
			$this->_qb_container['selects'] = array();

		$this->_qb_container['selects'][] = array('select' => $select, 'escape' => $escape);
		return $this;
	}

	public function join($table, $cond, $type = '', $escape = NULL)
	{
		if(!isset($this->_qb_container['joins']))
			$this->_qb_container['joins'] = array();

		$this->_qb_container['joins'][] = array('table' => $table, 'cond' => $cond, 'type' => $type, 'escape' => $escape);
		return $this;
	}

	public function where($where, $value = '', $escape = NULL)
	{
		if(!isset($this->_qb_container['wheres']))
			$this->_qb_container['wheres'] = array();

		if(is_array($where))
		{
			foreach ($where as $wkey => $wvalue) {
				$this->where($wkey,$wvalue);
			}
		}
		else
		{
			$this->_qb_container['wheres'][] = array('where' => $where, 'value' => $value,  'escape' => $escape);
		}

		return $this;
	}

	public function or_where($where, $value = '', $escape = NULL)
	{
		if(!isset($this->_qb_container['or_wheres']))
			$this->_qb_container['or_wheres'] = array();

		if(is_array($where))
		{
			foreach ($where as $wkey => $wvalue) {
				$this->or_where($wkey,$wvalue);
			}
		}
		else
		{
			$this->_qb_container['or_wheres'][] = array('where' => $where, 'value' => $value,  'escape' => $escape);
		}
		return $this;
	}

	public function where_in($where_in, $values, $escape = NULL)
	{
		if(!isset($this->_qb_container['where_ins']))
			$this->_qb_container['where_ins'] = array();

		$this->_qb_container['where_ins'][] = array('where_in' => $where_in, 'values' => $values,  'escape' => $escape);
		return $this;
	}

	public function or_where_in($where_in, $values, $escape = NULL)
	{
		if(!isset($this->_qb_container['or_where_ins']))
			$this->_qb_container['or_where_ins'] = array();

		$this->_qb_container['or_where_ins'][] = array('where_in' => $where_in, 'values' => $values,  'escape' => $escape);
		return $this;
	}


	public function where_not_in($where_not_in, $values, $escape = NULL)
	{
		if(!isset($this->_qb_container['where_not_ins']))
			$this->_qb_container['where_not_ins'] = array();

		$this->_qb_container['where_not_ins'][] = array('where_not_in' => $where_not_in, 'values' => $values,  'escape' => $escape);
		return $this;
	}

	public function or_where_not_in($where_not_in, $values, $escape = NULL)
	{
		if(!isset($this->_qb_container['or_where_not_ins']))
			$this->_qb_container['or_where_not_ins'] = array();

		$this->_qb_container['or_where_not_ins'][] = array('where_not_in' => $where_not_in, 'values' => $values,  'escape' => $escape);
		return $this;
	}


	public function like($like, $value, $side = 'both', $escape = NULL)
	{
		if(!isset($this->_qb_container['likes']))
			$this->_qb_container['likes'] = array();

		$this->_qb_container['likes'][] = array('like' => $like, 'value' => $value,  'side' => $side, 'escape' => $escape);
		return $this;
	}

	public function or_like($like, $value, $side = 'both', $escape = NULL)
	{
		if(!isset($this->_qb_container['or_likes']))
			$this->_qb_container['or_likes'] = array();

		$this->_qb_container['or_likes'][] = array('like' => $like, 'value' => $value,  'side' => $side, 'escape' => $escape);
		return $this;
	}



	public function group_by($group_by, $escape = NULL)
	{
		if(!isset($this->_qb_container['group_bys']))
			$this->_qb_container['group_bys'] = array();

		$this->_qb_container['group_bys'][] = array('group_by' => $group_by, 'escape' => $escape);
		return $this;
	}


	public function having($key, $value = NULL, $escape = NULL)
	{
		if(!isset($this->_qb_container['havings']))
			$this->_qb_container['havings'] = array();

		$this->_qb_container['havings'][] = array('key' => $key, 'value' => $value, 'escape' => $escape);
		return $this;
	}


	public function order_by($criteria, $order = '', $escape = NULL)
	{
		if(!isset($this->_qb_container['order_bys']))
			$this->_qb_container['order_bys'] = array();

		$this->_qb_container['order_bys'][] = array('criteria' => $criteria, 'order' => $order, 'escape' => $escape);
		return $this;		
	}

	public function limit($limit,$offset = 0)
	{
		if(!isset($this->_qb_container['limit']))
			$this->_qb_container['limit'] = array();

		$this->_qb_container['limit'] = array('limit'=>$limit,'offset'=>$offset);
		return $this;		
	}


	public function offset($offset)
	{
		if(!isset($this->_qb_container['limit']))
			$this->_qb_container['limit'] = array();
		
		$this->_qb_container['limit']['offset'] = $offset;
		return $this;		
	}


	public function reset()
	{
		$this->_qb_container = array();
		$this->_return_pair = array();
		$this->_return_sql = FALSE;
		$this->_return_json = FALSE;
		$this->_return_query_object = FALSE;
		$this->_return_ = FALSE;
	}


	// ======================================================================================================
	// OBSERVERS
	// ======================================================================================================

	/**
	 * Get MySQL DateTime
	 *
	 * @param	int		$timestamp
	 * @return	string
	 */
	public function get_mysql_datetime($timestamp = 0)
	{
		if ($timestamp == 0) $timestamp = time();

		return date('Y-m-d H:i:s', $timestamp);
	}


	/**
	 * MySQL DATETIME for created_on
	 * 
	 * @param object|array $row
	 * @return object|array
	 */
	public function created_on($row)
	{
		if (is_object($row))
		{
			$row->{$this->_created_on_key} = $this->get_mysql_datetime();
		}
		else
		{
			$row[$this->_created_on_key] = $this->get_mysql_datetime();
		}
		return $row;
	}

	/**
	 * MySQL DATETIME for updated_on
	 * 
	 * @param object|array $row
	 * @return object|array
	 */
	public function updated_on($row)
	{
		if (is_object($row))
		{
			$row->{$this->_updated_on_key} = $this->get_mysql_datetime();
		}
		else
		{
			$row[$this->_updated_on_key] = $this->get_mysql_datetime();
		}

		return $row;
	}

	// ======================================================================================================
	// CUSTOM DB FUNCTIONS
	// ======================================================================================================

	public function select_age($field, $as = '')
	{
		$this->select('FLOOR(DATEDIFF (NOW(), `'.$field.'`)/365) '. $as,FALSE);
		return $this;
	}

	/**
	 * Retuns a SQL string for prefix based identifier row insertion
	 * 
	 * @param string $table_name
	 * @param string $column_name
	 * @param string $prefix
	 * @param int $padding
	 * @return string
	 * @author A H Abid
	 */
	public function prefix_code_sql($table_name, $column_name, $prefix, $padding)
	{
		// SUBSTR has a higher index position, so added 1 more
		$prefix_length = strlen($prefix) + 1;

		// Set Proper Table Column in var
		$field = "{$table_name}.{$column_name}";

		// the SQL String
		$sql  = "IF( COUNT({$field}) = 0, ";
		$sql .= "CONCAT('{$prefix}',LPAD('1',{$padding},'0') ), ";
		$sql .= "CONCAT( '{$prefix}', LPAD( MAX( CONVERT( ";
		$sql .= "SUBSTR({$field},{$prefix_length},{$padding}), UNSIGNED INTEGER) ) + 1, {$padding}, '0') ) ) ";

		return $sql;
	}


	// ==========================================================================================================
	// HELPERS
	// ==========================================================================================================

	/**
	 * Trigger an event and call its observers. Pass through the event name
	 * (which looks for an instance variable $this->event_name), an array of
	 * parameters to pass through and an optional 'last in interation' boolean
	 */
	public function trigger($event, $data = FALSE, $last = TRUE)
	{
		$event = '_'.$event;

		if (isset($this->$event) && is_array($this->$event))
		{
			foreach ($this->$event as $method)
			{
				if (strpos($method, '('))
				{
					preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);

					$method = $matches[1];
					$this->callback_parameters = explode(',', $matches[3]);
				}

				$data = call_user_func_array(array($this, $method), array($data, $last));
			}
		}

		return $data;
	}

	/**
	 * Run validation on the passed data
	 */
	public function validate($data)
	{
		if ($this->_skip_validation)
		{
			return $data;
		}

		if (empty($this->fields))
		{
			return $data;
		}

		$this->form_validation->reset_validation();

		$data = $this->trigger('before_validation_rules', $data);

		// Whether to validate all fields or only $data fields
		$rules = array();
		if($this->_validate_all_fields === TRUE)
		{
			$rules = $this->fields;
		}
		else
		{
			foreach($data as $dkey => $dvalue)
			{
				if(isset($this->fields[$dkey]))
					$rules[$dkey] = $this->fields[$dkey];
			}
		}
		
		$this->form_validation->set_rules($rules);

		$data = $this->trigger('after_validation_rules', $data);

		$data = $this->trigger('before_validation', $data);

		$this->form_validation->set_data( (array) $data);

		if ($this->form_validation->run() === FALSE) $data = FALSE;
		
		$data = $this->trigger('after_validation', $data);

		return $data;
	}


	protected function _set_grid_limit_offset()
	{
		if ($this->input->get('dl') !== NULL && $this->input->get('dl') > 0)
		{
			get_instance()->display_limit = $this->input->get('dl');
		}

		$page	= $this->input->get('page') !== NULL && $this->input->get('page') > 0
						? (int) $this->input->get('page') : 1;
		$offset	= ($page - 1) * $this->display_limit;

		$this->db->limit($this->display_limit);
		$this->db->offset($offset);
	}

	// ==========================================================================================================
	// INTERNALS
	// ==========================================================================================================


	protected function _extract_qb()
	{
		if(count($this->_qb_container) < 1) return;

		if(isset($this->_qb_container['selects']))
		{
			foreach ($this->_qb_container['selects'] as $key => $value) {
				$this->db->select($value['select'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['joins']))
		{
			foreach ($this->_qb_container['joins'] as $key => $value) {
				$this->db->join($value['table'],$value['cond'],$value['type'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['wheres']))
		{
			foreach ($this->_qb_container['wheres'] as $key => $value) {
				$this->db->where($value['where'],$value['value'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['or_wheres']))
		{
			foreach ($this->_qb_container['or_wheres'] as $key => $value) {
				$this->db->or_where($value['where'],$value['value'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['where_ins']))
		{
			foreach ($this->_qb_container['where_ins'] as $key => $value) {
				$this->db->where_in($value['where_in'],$value['values'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['or_where_ins']))
		{
			foreach ($this->_qb_container['or_where_ins'] as $key => $value) {
				$this->db->or_where_in($value['where_in'],$value['values'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['where_not_ins']))
		{
			foreach ($this->_qb_container['where_not_ins'] as $key => $value) {
				$this->db->where_not_in($value['where_not_in'],$value['values'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['or_where_not_ins']))
		{
			foreach ($this->_qb_container['or_where_not_ins'] as $key => $value) {
				$this->db->or_where_not_in($value['where_not_in'],$value['values'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['likes']))
		{
			foreach ($this->_qb_container['likes'] as $key => $value) {
				$this->db->like($value['like'],$value['value'], $value['side'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['or_likes']))
		{
			foreach ($this->_qb_container['or_likes'] as $key => $value) {
				$this->db->or_like($value['like'],$value['value'], $value['side'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['group_bys']))
		{
			foreach ($this->_qb_container['group_bys'] as $key => $value) {
				$this->db->group_by($value['group_by'],$value['escape']);
			}
		}

		if(isset($this->_qb_container['havings']))
		{
			foreach ($this->_qb_container['havings'] as $key => $value) {
				$this->db->having($value['key'],$value['value'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['order_bys']))
		{
			foreach ($this->_qb_container['order_bys'] as $key => $value) {
				$this->db->order_by($value['criteria'],$value['order'], $value['escape']);
			}
		}

		if(isset($this->_qb_container['limit']))
		{
			if (isset($this->_qb_container['limit']['limit']))
				$this->db->limit($this->_qb_container['limit']['limit']);
			if (isset($this->_qb_container['limit']['offset']))
				$this->db->offset($this->_qb_container['limit']['offset']);
		}

	}


	/**
	 * Get the data as select
	 * @param  string $alias
	 * @param  string $row_result 'row' or 'result'
	 * @return array
	 */
	protected function _get_data($alias = '', $row_result = 'result')
	{
		$this->_check_soft_delete($alias);
		
		$this->_extract_qb();

		$this->trigger('before_get');

		$sql = $this->db->get_compiled_select();

		if ($this->_return_sql)
		{
			// Return an SQL statement as a result.
			return $sql;
		}

		$query = $this->db->query($sql);

		if ($this->_return_query_object)
		{
			// Return an SQL statement as a result.
			if($this->_reset_after_query)
			{
				$this->reset();
			}

			return $query;
		}

		$data = $query->{$row_result.'_'.$this->_return_type}();
		
		$data = $this->trigger('after_get', $data);

		if (count($this->_return_pair) > 0)
		{
			// Return the value as pair
			$data = $this->{'_return_'.$row_result.'_as_'.$this->_return_pair_type.'_pair'}($data);
		}

		if ($this->_return_json)
		{
			// Return the value as pair
			$data = json_encode($data);
		}

		if($this->_reset_after_query)
		{
			$this->reset();
		}

		return $data;
	}


	/**
	 * Return row as simple pair
	 * 
	 * @param  array $row
	 * @return array
	 */
	protected function _return_row_as_simple_pair($row)
	{
		if(is_array($row))
		{
			return array(
				$row[$this->_return_pair['key']] => $row[$this->_return_pair['value']]
			);	
		}
		elseif (is_object($row))
		{
			return array(
				$row->{$this->_return_pair['key']} => $row->{$this->_return_pair['value']}
			);	
		}
		else return NULL;
	}

	/**
	 * Return result as simple pair
	 * 
	 * @param  array $result
	 * @return array
	 */
	protected function _return_result_as_simple_pair($result)
	{
		if(count($result) < 1) return NULL;

		$pairs = array();
		foreach ($result as $row)
		{
			if(is_array($row))
			{
				$pairs[$row[$this->_return_pair['key']]] = $row[$this->_return_pair['value']];
			}
			elseif (is_object($row))
			{
				$pairs[$row->{$this->_return_pair['key']}] = $row->{$this->_return_pair['value']};
			}
		}
		return $pairs;
	}

	/**
	 * Return row as group pair
	 * 
	 * @param  array $row
	 * @return array
	 */
	protected function _return_row_as_group_pair($row)
	{
		$pairs = array();
		$pairs[] = $this->_return_row_as_simple_pair($row);
		return $pairs;
	}

	/**
	 * Return result as group pair
	 * 
	 * @param  array $result
	 * @return array
	 */
	protected function _return_result_as_group_pair($result)
	{
		if(count($result) < 1) return NULL;

		$pairs = array();
		foreach ($result as $row)
		{
			$pairs[] = $this->_return_row_as_simple_pair($row);
		}
		return $pairs;
	}


	protected function _check_soft_delete($alias = '')
	{
		if ($this->_soft_delete && $this->_temporary_with_deleted !== TRUE)
		{
			$full_key = ($alias != '' ? $alias : $this->_table).'.'.$this->_soft_delete_key;
			$this->where($full_key, (bool) $this->_temporary_only_deleted);
			return TRUE;
		}
		else
			return FALSE;
	}

}
/* End of file MY_Model.php */
/* Location: ./application/core/Modelp */