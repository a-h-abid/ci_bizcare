<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Extended URI Class
 *
 * Parses URIs and determines routing
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	URI
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/uri.html
 */
class MY_URI extends CI_URI {

	/**
	 * Get Current URI LangCode
	 * 
	 * @var string
	 */
	public $lang_code = '';


	/**
	 * Set URI String
	 *
	 * @param 	string	$str
	 * @return	void
	 */
	protected function _set_uri_string($str)
	{
		// Filter out control characters and trim slashes
		$this->uri_string = trim(remove_invisible_characters($str, FALSE), '/');

		// Get And Set Lang Code
		$languages = $this->config->item('languages');
		$this->lang_code = $this->config->item('default_lang_code');

		if ($this->uri_string === '')
			return;
		
		// Remove the URL suffix, if present
		if (($suffix = (string) $this->config->item('url_suffix')) !== '')
		{
			$slen = strlen($suffix);

			if (substr($this->uri_string, -$slen) === $suffix)
			{
				$this->uri_string = substr($this->uri_string, 0, -$slen);
			}
		}

		$this->segments[0] = NULL;
		
		// Populate the segments array
		foreach (explode('/', trim($this->uri_string, '/')) as $val)
		{
			// Filter segments for security
			$val = trim($this->filter_uri($val));

			if ($val !== '')
			{
				$this->segments[] = $val;
			}
		}

		// Remove Null Reference
		unset($this->segments[0]);
		
		// No More Segments Found, Return
		if (count($this->segments) < 1)
		{
			return;
		}

		// Separate Lang Code from rest of URI
		if ( in_array($this->segments[1], array_keys($languages)) )
		{
			$this->lang_code = array_shift($this->segments);

			// If Default lang hide is true, redirect
			if ( $this->config->item('hide_default_lang_uri') === TRUE
				&& $this->lang_code == $this->config->item('default_lang_code'))
			{
				header('Location: '. SITEURL . trim(implode('/', $this->segments),'/'), TRUE, 302);
				exit;
			}

			// Set This Language in Config
			$this->config->set_item( 'language', $languages[$this->lang_code] );
		}

		// Now combine the $uri_string
		$this->uri_string = implode('/', $this->segments);
	}

}
/* End of file MY_URI.php */
/* Location: ./application/core/MY_URI.php */