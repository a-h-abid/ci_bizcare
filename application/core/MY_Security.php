<?php if(!defined('BASEPATH')) die('no access');

/**
 * Extended Security Class
 */
class MY_Security extends CI_Security {
	
	function __construct()
	{
		// CSRF config
		foreach (array('csrf_expire', 'csrf_token_name', 'csrf_cookie_name') as $key)
		{
			if (FALSE !== ($val = config_item($key)))
			{
				$this->{'_'.$key} = $val;
			}
		}

		// Append application specific cookie prefix
		if (config_item('cookie_prefix'))
		{
			$this->_csrf_cookie_name = config_item('cookie_prefix').$this->_csrf_cookie_name;
		}

		// Set the CSRF hash
		$this->_csrf_set_hash();
	}

	/**
	 * Override Show CSRF Error
	 *
	 * For development, will show error and stop
	 * For others, will redirect if HTTP_REFERER is available.
	 * 
	 * @return	void
	 */
	public function csrf_show_error()
	{
		if(ENVIRONMENT == 'development')
		{
			show_error('The action you have requested is not allowed.');
		}
		else
		{
			if(isset($_SERVER['HTTP_REFERER']))
			{
				header('Location: '.$_SERVER['HTTP_REFERER']);
				exit();
			}
			else
				show_error('The action you have requested is not allowed.');
		}
	}


}

/* End of file MY_Security.php */
/* Location: ./application/core/MY_Security.php */