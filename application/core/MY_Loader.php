<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extended CI_Loaded class
 * 
 * @package     CodeIgniter
 * @subpackage  Loader
 * @category    Loader
 * @author      Ellislab 
 */
class MY_Loader extends CI_Loader {

	/**
	 * Database Loader
	 *
	 * @param  mixed  $params    Database configuration options
	 * @param  bool  $return   Whether to return the database object
	 * @param  bool  $query_builder  Whether to enable Query Builder
	 *          (overrides the configuration setting)
	 *
	 * @return  void|object|bool  Database object if $return is set to TRUE,
	 *          FALSE on failure, void in any other case
	 * @link     https://github.com/EllisLab/CodeIgniter/wiki/Extending-Database-Drivers
	 */
	public function database($params = '', $return = FALSE, $query_builder = NULL)
	{
		// Grab the super object
		$CI =& get_instance();

		// Do we even need to load the database class?
		if ($return === FALSE && $query_builder === NULL && isset($CI->db) && is_object($CI->db) && ! empty($CI->db->conn_id))
		{
			return FALSE;
		}

		require_once(BASEPATH.'database/DB.php');
		//require_once(APPPATH.'database/'.config_item('subclass_prefix').'DB.php');
		$db = DB($params, $query_builder);

		// Load extended DB driver if exists
		$custom_db_driver = config_item('subclass_prefix') . 'DB_' . $db->dbdriver . '_driver';
		$custom_db_driver_file = APPPATH.'database/drivers/'.$db->dbdriver.'/'.$custom_db_driver.'.php';
		if (file_exists($custom_db_driver_file))
		{
			require_once($custom_db_driver_file);
			$db = new $custom_db_driver(get_object_vars($db));
		}

		// Return DB instance
		if ($return === TRUE)
		{
			return $db;
		}

		// Initialize the db variable. Needed to prevent reference errors with some configurations
		$CI->db = '';
		$CI->db =& $db;
	}
 
}
/* End of file MY_Loader.php */
/* Location: ./application/core/MY_Loader.php */ 