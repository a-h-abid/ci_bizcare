<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Extented Router Class
 *
 * Parses URIs and determines routing
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/routing.html
 */
class MY_Router extends CI_Router {

	/**
	 * Contants for route index
	 */
	CONST ARR_ROUTE_USES	= 'uses';
	CONST ARR_ROUTE_NAME	= 'as';
	CONST ARR_ROUTE_REGEX	= 'regex';
	CONST ARR_ROUTE_CSRF	= 'csrf';
	CONST ARR_ROUTE_VERB	= 'verb';

	
	/**
	 * Reverse Routes are all assigned here
	 */
	private $_reverse_routes = NULL;

	/**
	 * Current Route Name
	 * 
	 * @var string
	 */
	public $current_route_name = '';

	/**
	 * Current Route Index
	 * 
	 * @var string
	 */
	public $current_route_uri = '';

	// --------------------------------------------------------------------

	/**
	 * Parse Routes
	 *
	 * Matches any routes that may exist in the config/routes.php file
	 * against the URI to determine if the class/method need to be remapped.
	 *
	 * @return	void
	 */
	protected function _parse_routes()
	{
		// Turn the segment array into a URI string
		$uri = implode('/', $this->uri->segments);
		
		// Get HTTP verb
		$http_verb = isset($_SERVER['REQUEST_METHOD']) ? strtolower($_SERVER['REQUEST_METHOD']) : 'cli';

		// Is there a literal match?  If so we're done
		if (isset($this->routes[$uri]))
		{
			// Check default routes format
			if (is_string($this->routes[$uri]))
			{
				$this->_set_request(explode('/', $this->routes[$uri]));
				$this->current_route_uri = $uri;
				return;
			}
			// Is there a matching http verb?
			elseif (is_array($this->routes[$uri]))
			{
				if (isset($this->routes[$uri][$http_verb]))
				{
					$this->_set_request(explode('/', $this->routes[$uri][$http_verb]));
					$this->current_route_uri = $uri;
					return;
				}
				elseif (isset($this->routes[$uri][self::ARR_ROUTE_USES]))
				{
					if( isset($this->routes[$uri][self::ARR_ROUTE_VERB]) 
						&& $http_verb !== strtolower($this->routes[$uri][self::ARR_ROUTE_VERB]) )
					{
						show_404();
					}

					// Check for CSRF
					if (isset($val[self::ARR_ROUTE_CSRF]) && $val[self::ARR_ROUTE_CSRF] === TRUE)
					{
						$this->_csrf_verify();
					}

					$this->_set_request( explode('/', $this->routes[$uri][self::ARR_ROUTE_USES]) );
					$this->current_route_uri = $uri;
					if (isset($this->routes[$uri][self::ARR_ROUTE_NAME]))
						$this->current_route_name = $this->routes[$uri][self::ARR_ROUTE_NAME];
					return;
				}
			}
		}

		// Loop through the route array looking for wildcards
		foreach ($this->routes as $key => $val)
		{
			// Save the current loop index key
			$route_index = $key;

			// CSRF Setting default for loop start
			$csrf = FALSE;

			// Check if route format is using http verb
			if (is_array($val))
			{
				if (isset($val[$http_verb]))
				{
					$val = $val[$http_verb];
				}
				elseif (isset($val[self::ARR_ROUTE_USES]))
				{
					// Check for Regex
					if (isset($val[self::ARR_ROUTE_REGEX]))
					{
						foreach($val[self::ARR_ROUTE_REGEX] as $paramKey => $paramRegex)
						{
							$key = str_replace(':'.$paramKey, $paramRegex, $key);
						}
					}

					// Check for CSRF
					if ( isset($val[self::ARR_ROUTE_CSRF]) )
					{
						$csrf = $val[self::ARR_ROUTE_CSRF];
					}

					if (isset($val[self::ARR_ROUTE_NAME]))
						$this->current_route_name = $val[self::ARR_ROUTE_NAME];

					$val = $val[self::ARR_ROUTE_USES];
				}
				else
				{
					continue;
				}
			}

			// Convert wildcards to RegEx
			$key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);
			
			// Does the RegEx match?
			if (preg_match('#^'.$key.'$#', $uri, $matches))
			{
				// Are we using callbacks to process back-references?
				if ( ! is_string($val) && is_callable($val))
				{
					// Remove the original string from the matches array.
					array_shift($matches);

					// Execute the callback using the values in matches as its parameters.
					$val = call_user_func_array($val, $matches);
				}
				// Are we using the default routing method for back-references?
				elseif (strpos($val, '$') !== FALSE && strpos($key, '(') !== FALSE)
				{
					$val = preg_replace('#^'.$key.'$#', $val, $uri);
				}

				// Check for CSRF
				if ($csrf === TRUE)
				{
					$this->_csrf_verify();
				}

				$this->_set_request(explode('/', $val));
				$this->current_route_uri = $route_index;
				return;
			}
		}

		// Stop Autoguess Controller Method Route if set to TRUE
		if ($this->config->item('allow_autoguess_routes') === FALSE)
		{
			show_404();
		}

		// If we got this far it means we didn't encounter a
		// matching route so we'll set the site default route
		$this->current_route_uri = $uri;
		$this->_set_request(array_values($this->uri->segments));
	}

	// --------------------------------------------------------------------
	
	/**
	 * Build reverse routes
	 * 
	 * @return void
	 * @author Kenny Katzgrau
	 * @link http://codefury.net/2010/09/easy-reverse-routing-with-codeigniter/
	 */
	protected function _build_reverse_routes()
	{
		$reverse_routes = array();

		foreach($this->routes as $route => $info)
		{
			// If this is a default route or scaffolding key, ignore it
			if(!is_array($info) || !isset($info[self::ARR_ROUTE_NAME])) continue;

			$reverse_routes[$info[self::ARR_ROUTE_NAME]] = $route;
		}

		return $this->_reverse_routes =& $reverse_routes;
	}

	// --------------------------------------------------------------------

	/**
	 * Get list of all reverse routes
	 * 
	 * @return array
	 */
	public function get_reverse_routes()
	{
		if($this->_reverse_routes === NULL)
			$this->_build_reverse_routes();

		return $this->_reverse_routes;
	}

	// --------------------------------------------------------------------

	/**
	 * Reverse routes
	 * 
	 * @param string $route_name
	 * @param array $args_keyval
	 * @return void
	 * @author Kenny Katzgrau
	 * @link http://codefury.net/2010/09/easy-reverse-routing-with-codeigniter/
	 */
	public function reverse_route($route_name, $args_keyval = array())
	{
		if($this->_reverse_routes === NULL)
			$this->_build_reverse_routes();

		if(!array_key_exists($route_name, $this->_reverse_routes))
		{
			trigger_error("No reverse route found for '$route_name'", E_USER_WARNING);
			return NULL;
		}

		$route = $this->_reverse_routes[$route_name];

		if(is_array($args_keyval) && count($args_keyval) > 0)
		{
			foreach($args_keyval as $key => $val)
			{
				$route = str_replace("(:$key)", $val, $route);
			}
		}

		// Remove missed arguments
		return preg_replace('/\(:[a-zA-Z]+\)/', '', $route); 
	}

	// --------------------------------------------------------------------

	/**
	 * Get Current Params values
	 * 
	 * @return array|NULL
	 */
	public function route_params()
	{
		// Turn the segment array into a URI string
		$uri = implode('/', $this->uri->segments);

		// Get the current route
		if( !isset($this->routes[$this->current_route_uri]) )
			return NULL;

		$route = $this->routes[$this->current_route_uri];
		$key = $this->current_route_uri;

		// Convert wild-cards to RegEx
		if(is_array($route) && isset($route[self::ARR_ROUTE_REGEX]))
		{
			foreach($route[self::ARR_ROUTE_REGEX] as $paramKey => $paramRegex)
			{
				$key = str_replace(':'.$paramKey, $paramRegex, $key);
			}
		}
		elseif (strpos($key, ':num') || strpos($key, ':any') )
		{
			$key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);
		}
		else
		{
			$key = preg_replace('/\:[a-z_]+/', '[^/]+', $key);
		}

		// Regex with current route and get params
		if(isset($route[self::ARR_ROUTE_REGEX]) && preg_match('#^'.$key.'$#', $uri, $matches))
		{
			$params = array();
			$i = 1;

			foreach($route[self::ARR_ROUTE_REGEX] as $paramKey => $paramRegex)
			{
				if(isset($matches[$i]))
					$params[$paramKey] = $matches[$i];
				$i++;
			}

			return $params;
		}
		else
		{
			return NULL;
		}

	}

	// --------------------------------------------------------------------
	
	/**
	 * Get the Current Route Name
	 * 
	 * @return string
	 */
	public function route_name()
	{
		return $this->current_route_name;
	}

	// --------------------------------------------------------------------

	/**
	 * Verify CSRF
	 */
	protected function _csrf_verify()
	{
		$SEC =& load_class('Security', 'core');

		$SEC->csrf_verify();
	}

}
/* End of file MY_Router.php */
/* Location: ./application/core/MY_Router.php */