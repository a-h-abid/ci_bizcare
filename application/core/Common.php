<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|------------------------------------------------------------------------
| Common Funcions
|------------------------------------------------------------------------
|
*/

if (!function_exists('dump'))
{
	/**
	 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
	 * 
	 * @param mixed $var
	 * @param string $label
	 * @param boolean $return
	 * @author Joost van Veen
	 * @version 1.0
	 */
	function dump($var, $label = 'Dump', $return = FALSE)
	{
		// Store dump in variable 
		ob_start();
		var_dump($var);
		$output = ob_get_clean();

		// Add formatting
		$output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
		$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

		// Output
		if ($return === FALSE)
		{
			echo $output;
		}
		else
		{
			return $output;
		}
	}
}


if (!function_exists('dd'))
{
	/**
	 * Dump Data and exit
	 *
	 * @param mixed $var
	 * @param string $label
	 * @author Joost van Veen
	 * @version 1.0
	 */
	function dd($var, $label = 'Dump')
	{
		dump($var,$label,FALSE);
		exit();
	}
}


if (!function_exists('convert_en_numbers_to_bn'))
{
	/**
	 * Convert Englist Numbers to Bangla Numbers
	 * 
	 * @param  string|int $number
	 * @return string
	 */
	function convert_en_numbers_to_bn($number)
	{
		$bn_nums = array('০','১','২','৩','৪','৫','৬','৭','৮','৯');
		$en_nums = array('0','1','2','3','4','5','6','7','8','9');
		return str_replace($en_nums, $bn_nums, $number);
	}
}

if (!function_exists('convert_bn_numbers_to_en'))
{
	/**
	 * Convert Bangla Numbers to Englist Numbers
	 * 
	 * @param  string|int $number
	 * @return string
	 */
	function convert_bn_numbers_to_en($number)
	{
		$bn_nums = array('০','১','২','৩','৪','৫','৬','৭','৮','৯');
		$en_nums = array('0','1','2','3','4','5','6','7','8','9');
		return str_replace($bn_nums, $en_nums, $number);
	}
}

/* End of file Common.php */
/* Location: ./application/core/Common.php */