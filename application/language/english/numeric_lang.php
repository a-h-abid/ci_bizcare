<?php
/**
 * @author      Ahmedul Haque Abid <a_h_abid@hotmail.com>, 2014.
 * @license     The MIT License (MIT), http://opensource.org/licenses/MIT
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['numeric.0'] = '0';
$lang['numeric.1'] = '1';
$lang['numeric.2'] = '2';
$lang['numeric.3'] = '3';
$lang['numeric.4'] = '4';
$lang['numeric.5'] = '5';
$lang['numeric.6'] = '6';
$lang['numeric.7'] = '7';
$lang['numeric.8'] = '8';
$lang['numeric.9'] = '9';
