<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @package		CodeIgniter
 * @subpackage	Public Controller
 * @category	Controller
 * @author		Phil Sturgeon
 * @source		http://philsturgeon.co.uk/blog/2010/02/CodeIgniter-base-Classes-Keeping-it-DRY
 */
class Public_Controller extends MY_Controller {

	/**
	 * Name of theme
	 *
	 * @var string
	 */
	protected $_theme = 'bizcare';

	/**
	 * Master Layout Location
	 *
	 * @var string
	 */
	protected $_layout = '_layout/master';

	/**
	 * Set Default Display Limit
	 *
	 * @var integer
	 */
	public $display_limit = 15;

	/**
	 * Session Key
	 *
	 * @var string
	 */
	protected $_auth_key = '';

	/**
	 * __construct()
	 */
	function __construct()
	{
		parent::__construct();

        if (CI_ENV != 'development')
        {
            $this->output->cache(900); // Webpage Cache set to 15 minutes
        }
	}

}
/* End of file Public_Controller.php */
/* Location: ./application/base/Public_Controller.php */