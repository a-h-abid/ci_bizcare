<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Public_Controller {

	/**
	 * __construct()
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Pages Page
	 */
	public function route($slug)
	{
		$this->validate($slug);
		
		$this->metas($slug);

		if (!isset($this->data['page_title']))
		{
			$this->data['page_title'] = 'Unset Title';
		}

		$this->render('public/pages/'.$slug,$this->data['page_title']);
	}


	protected function validate($slug)
	{
		$path = VIEWPATH.$this->_theme.DS.'public'.DS.'pages'.DS.$slug.'.php';
		
		if (!file_exists($path))
		{
			show_404();
		}
	}


	protected function metas($slug)
	{
		$path = VIEWPATH.$this->_theme.DS.'public'.DS.'pages'.DS.$slug.'.json';

		if (!file_exists($path))
		{
			return;
		}

		$json = json_decode(file_get_contents($path),TRUE);

		if ($json !== NULL)
			$this->data = array_merge($this->data,$json);
	}


}
/* End of file Pages.php */
/* Location: ./application/controllers/Pages.php */