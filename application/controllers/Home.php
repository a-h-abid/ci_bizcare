<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller {

	/**
	 * __construct()
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Home Page
	 */
	public function index()
	{
		$this->metas('home');

		$this->extra_scripts($this->load->view('partials/scripts/facebook-timeline','',TRUE));

		$this->render('public/home',$this->data['page_title']);
	}


	protected function metas($slug)
	{
		$path = VIEWPATH.$this->_theme.DS.'public'.DS.$slug.'.json';

		if (!file_exists($path))
		{
			return;
		}

		$json = json_decode(file_get_contents($path),TRUE);

		if ($json !== NULL)
			$this->data = array_merge($this->data,$json);
	}


}
/* End of file Home.php */
/* Location: ./application/controllers/Home.php */