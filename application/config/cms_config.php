<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| CMS Configuration
| -------------------------------------------------------------------------
| All your CMS Configurations are found here
|
| @author A H Abid
| @date 2013-Jul-02
|
| !! Note
| --------
| This file gets updated from the CMS Panel directly, so do not mess with the
| structure or layout. else if will get broken.
|
| Helper file related to this config file is -> helpers/site_config_helper.php
|    - Functions can be found there to update config values in available here.
|    - When using the function to save a config value, make sure u use
|      quotes properly (if needed) by your module.
|    - Only supported values are Integer, Boolean(TRUE or FALSE) and Strings.
| 	 - Every value must be in their own line. Break it, it will break the file during save.
|
|
*/


/*
| -------------------------------------------------------------------------
| Site Online
| -------------------------------------------------------------------------
| Site online or offine can be set from here. Value is boolean.
|
*/
$config['site_online'] = TRUE;

/*
| -------------------------------------------------------------------
|  Site Name
| -------------------------------------------------------------------
|
| The name of this website. This can be later use by using $this->config->item('site_name');
|
*/
$config['site_name'] = 'BizCareBD.com';

/*
| -------------------------------------------------------------------
|  Site Alternate Name
| -------------------------------------------------------------------
|
| Alternate name of this website. Used in the copyright section. This can be later use by using $this->config->item('site_alter_name');
|
*/
$config['site_alter_name'] = 'BizCare';


/*
| -------------------------------------------------------------------------
| Slogan
| -------------------------------------------------------------------------
| Site slogan usually found in the top of home page can be edited here.
|
*/
$config['slogan'] = "";


/*
| -------------------------------------------------------------------------
| Meta Keywords & Description
| -------------------------------------------------------------------------
| For Seo, write down your keywords and site description
|
*/
$config['meta_description'] = "BizCare, a consulting firm based in Dhaka and engaged in three broad areas - Communication, Corporate Responsibility and Human Resource";
$config['meta_keywords'] = "Bizcare, Bangladesh, Dhaka, Strategic, Communications, Firm, Business, Consultancy";
$config['meta_author'] = "Ahmedul Haque Abid";


/*
| -------------------------------------------------------------------------
| Feedback Messages Email Address
| -------------------------------------------------------------------------
| Email address to send feedback messages.
|
*/
$config['feedback_email'] = "abid@runnercyberlink.com";


/*
| -------------------------------------------------------------------------
| Error Messages Email Address
| -------------------------------------------------------------------------
| Email address to send fatal error messages.
|
*/
$config['errors_email'] = "abid@runnercyberlink.com";

/* End of file cms_config.php */
/* Location: ./application/config/cms_config.php */