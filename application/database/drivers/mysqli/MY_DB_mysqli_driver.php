<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Extended MySQLi Database Adapter Class
 *
 * @package     CodeIgniter
 * @subpackage  Drivers
 * @category    Database
 * @author      A H Abid
 * @link        https://github.com/EllisLab/CodeIgniter/wiki/Extending-Database-Drivers
 */
class MY_DB_mysqli_driver extends CI_DB_mysqli_driver {

	/**
	 * Count the total rows from current compiled selects
	 * 
	 * @return int
	 * @author A H Abid
	 */
	public function count_total($table = '')
	{
		if ($table !== '')
		{
			$this->_track_aliases($table);
			$this->from($table);
		}

		$sql = ($this->qb_distinct === TRUE)
			? $this->_count_string.$this->protect_identifiers('numrows')."\nFROM (\n".$this->_compile_select()."\n) CI_count_all_results"
			: $this->_compile_select($this->_count_string.$this->protect_identifiers('numrows'));

		$sql = preg_replace('/LIMIT.+$/i','', $sql);
		
		$result = $this->query($sql);
		$this->_reset_select();

		if ($result->num_rows() === 0)
		{
			return 0;
		}

		$row = $result->row();
		return (int) $row->numrows;
	}

	// =====================================================================================
	
	
	/**
	 * Select FLOOR
	 * 
	 * @param  string $value
	 * @param  string $alias
	 * @return CI_DB_query_builder
	 */
	public function floor($value, $alias = '')
	{
		return $this->select($this->_floor($value).' '.$alias);
	}


	// -------------------------------------------------------------------
	
	/**
	 * Floor
	 * 
	 * @param  string $date_from - field_name, date, mysql function
	 * @param  string $date_to   - field_name, date, mysql function
	 * @param  string $alias     - alias for the field name
	 * @return string
	 */
	public function _floor($value)
	{
		return 'FLOOR('.$this->_escape_str($value).')';
	}


	// =====================================================================================

	/**
	 * Select Date Diff
	 * 
	 * @param  string $date_from - field_name, date, mysql function
	 * @param  string $date_to   - field_name, date, mysql function
	 * @param  string $alias     - alias for the field name
	 * @return CI_DB_query_builder
	 */
	public function select_datediff($date_from, $date_to, $alias = '')
	{
		$this->select(trim($this->_date_diff($date_from,$date_to).' '.$alias));
		return $this;
	}


	// -------------------------------------------------------------------
	

	/**
	 * Date Diff
	 *
	 * @param	string $from_date
	 * @param	string $to_date
	 * @return	string
	 */
	public function _datediff($from_date, $to_date)
	{
		return 'DATEDIFF('.$this->_escape_str($from_date).', '.$this->_escape_str($to_date).')';

	}



	// =====================================================================================

	/**
	 * Insert On Duplicate Key Update with choosing the update fields (For Single Item)
	 * 
	 * @param string $table
	 * @param array $update "fields name to update on duplicate key"
	 * @param array $insert "Main data set"
	 * @param boolean $sql_string "return sql as string"
	 * @return boolean
	 * @author Nithin Meppurathu
	 * @author A H Abid (for the Insert Batch on duplicate update)
	 * @link http://nitmedia.com/post/18132504068/codeigniter-on-duplicate-update-db-function
	 */
	public function on_duplicate_update($table = '', $update = NULL, $insert = NULL, $sql_string = FALSE)
	{
		if ( ! is_null($insert))
		{
			// Check if array multidimentional or single item array
			// Hopefully if 1st item is array then it is probably a multi-dimentional array
			$multi_duimentional = (isset($insert[0]) && is_array($insert[0])) ? TRUE : FALSE;
			$this->set($insert);
		}

		if (count($this->qb_set) == 0)
		{
			if ($this->db_debug)
			{
				return $this->display_error('db_must_use_set');
			}
			return NULL;
		}

		if ($table == '')
		{
			if ( ! isset($this->qb_from[0]))
			{
				if ($this->db_debug)
				{
					return $this->display_error('db_must_set_table');
				}
				return NULL;
			}

			$table = $this->qb_from[0];
		}

		if($multi_duimentional === FALSE)
		{
			$sql = $this->_insert_on_duplicate_update($this->protect_identifiers($table, TRUE, NULL, FALSE),
			$this->protect_identifiers($update),
			$this->qb_set);
		}
		else
		{
			$sql = $this->_insert_on_duplicate_update_batch($this->protect_identifiers($table, TRUE, NULL, FALSE),
			$this->protect_identifiers($update),
			$this->qb_set);    
		}

		$this->_reset_write();
		if ($sql_string === TRUE)
		{
			return $sql;
		}
		else 
		{
			if($this->save_queries === TRUE)
			{
				return $this->query($sql, FALSE, FALSE);
			}
			else
			{
				return $this->simple_query($sql);
			}
		}
	}

	// ---------------------------------------------------------------------------------------------------

	/**
	 * SQL Statement for Insert On Duplicate Key Update with choosing the update fields 
	 * 
	 * @param string $table
	 * @param array $update "fields name to update on duplicate key"
	 * @param array $insert "Main data set"
	 * @return string
	 * @author Nithin Meppurathu
	 * @link http://nitmedia.com/post/18132504068/codeigniter-on-duplicate-update-db-function
	 */
	protected function _insert_on_duplicate_update($table, $update, $insert)
	{
		$insert_fields = array();
		$update_fields = array();
		foreach($insert as $key => $value)
		{
			$insert_fields[] = $key . ' = ' . $value;
			if( in_array($key, $update) )
			{
				$update_fields[] = $key . ' = ' . $value;
			}
		}
		return "INSERT INTO ".$table." SET ".implode(', ', $insert_fields)." ON DUPLICATE KEY UPDATE ".implode(', ', $update_fields);
	}

	// ---------------------------------------------------------------------------------------------------

	/**
	 * SQL Statement for Insert On Duplicate Key Update Batch Data with choosing the update fields 
	 * 
	 * @param string $table
	 * @param array $update "fields name to update on duplicate key"
	 * @param array $insert "Main data set"
	 * @return string
	 * @author A H Abid
	 */
	protected function _insert_on_duplicate_update_batch($table, $update, $insert)
	{
		$insert_columns = array();
		$insert_values = array();
		$update_fields = array();

		foreach($insert as $sub_insert)
		{
			$sub_insert_values = array();
			foreach($sub_insert as $key => $val)
			{
				$key		= '`' . $key . '`';
				$value		= (is_string($val) || is_null($val)) ? "'" . $val . "'" : $val;

				$sub_insert_values[] = $value;
				if(!in_array($key, $insert_columns))
				{
					$insert_columns[] = $key;
				}

				$update_str = $key . ' = VALUES(' . $key . ')';
				if(in_array($key, $update) && !in_array($update_str, $update_fields))
				{
					$update_fields[] = $update_str;
				}
			}
			$insert_values[] = implode(', ', $sub_insert_values);
		}

		$sql  = "INSERT INTO {$table} ";
		$sql .= "(" . implode(', ', $insert_columns) . ") ";
		$sql .= "VALUES (" . implode('), (', $insert_values) . ") ";
		$sql .= "ON DUPLICATE KEY UPDATE " . implode(', ', $update_fields);

		return $sql;
	}

	// ===================================================================================================

	/**
	* Get INSERT BATCH query string
	*
	* Compiles insert batch query and returns the sql
	*
	* @param   string $table - the table to insert into
	* @param   bool   $reset - TRUE: reset QB values; FALSE: leave QB values alone
	* @return  string
	* @author  A H Abid
	*/  
	public function get_compiled_insert_batch($table = '', $reset = TRUE)
	{
		if ($this->_validate_insert($table) === FALSE)
		{
			return FALSE;
		}
		$sql = '';
		for ($i = 0, $total = count($this->qb_set); $i < $total; $i += 100)
		{
			$sql .= $this->_insert_batch(
								$this->protect_identifiers($table, TRUE, TRUE, FALSE),
								$this->qb_keys,
								array_slice($this->qb_set, $i, 100)
							).'; ';
		}

		if ($reset === TRUE)
		{
			$this->_reset_write();
		}

		return $sql;
	}

	// ===================================================================================================

	/**
	 * Get UPDATE BATCH query string
	 *
	 * Compiles update batch query and returns the sql
	 *
	 * @param   string $table - the table to update
	 * @param   string $index - update field key
	 * @param   bool   $reset - reset query data
	 * @return  string
	 * @author  A H Abid
	 */
	public function get_compiled_update_batch($table = '', $index, $reset = TRUE)
	{
		// Combine any cached components with the current statements
		$this->_merge_cache();

		if ($this->_validate_update($table) === FALSE)
		{
			return FALSE;
		}

		$sql = '';
		for ($i = 0, $total = count($this->qb_set); $i < $total; $i += 100)
		{
			$sql .= $this->_update_batch(
				$this->protect_identifiers($table, TRUE, NULL, FALSE),
				array_slice($this->qb_set, $i, 100),
				$this->protect_identifiers($index)
			).'; ';
			$this->qb_where = array();
		}

		if ($reset === TRUE)
		{
			$this->_reset_write();
		}

		return $sql;
	}


  // ===================================================================================================

	/**
	 * Delete rows by joined tables. Use join() to join tables
	 *
	 * @param   string  $table
	 * @param   array   $where
	 * @param   bool    $reset_data - reset query after execution
	 * @param   bool    $sql_string - return sql as string
	 * @return  string|boolean
	 * @author  A H Abid
	 */
	public function delete_join($table = '', $where = array(), $reset_data = TRUE, $sql_string = FALSE)
	{
		// Combine any cached components with the current statements
		$this->_merge_cache();

		if ($table === '')
		{
			if ( ! isset($this->qb_from[0]))
			{
				return ($this->db_debug) ? $this->display_error('db_must_set_table') : FALSE;
			}

			$table = $this->qb_from[0];
		}
		else
		{
			$table = $this->protect_identifiers($table, TRUE, NULL, FALSE);
		}

		if (count($this->qb_join) === 0)
		{
			return ($this->db_debug) ? $this->display_error('db_del_set_join_syntax') : FALSE;
		}

		if (is_array($where) && count($where) > 0)
		{
			$this->where($where);
		}

		if (count($this->qb_where) === 0)
		{
			return ($this->db_debug) ? $this->display_error('db_del_must_use_where') : FALSE;
		}

		$sql = $this->_delete_join($table);
		if ($reset_data)
		{
			$this->_reset_write();
		}

		if ($sql_string === TRUE)
		{
			return $sql;
		}
		else 
		{
			if($this->save_queries === TRUE)
			{
				return $this->query($sql, FALSE, FALSE);
			}
			else
			{
				return $this->simple_query($sql);
			}
		}
	}

	// ---------------------------------------------------------------------------------------------------

	/**
	 * SQL statement for delete_join() syntax
	 * 
	 * @param string $table
	 * @return string
	 * @author A H Abid
	 */
	protected function _delete_join($table)
	{
		// Set Tables 
		$main_table = explode(' ', $table);
		$from_tables = array(isset($main_table[1]) ? $main_table[1] : $main_table[0] );

		// Init vars
		$sql = '';
		$join_sql = '';

		if (count($this->qb_join) > 0)
		{
			$current_join_sql = "\n".implode("\n", $this->qb_join);
			if(preg_match('/(?<=JOIN )[`0-9a-zA-Z_ ]+(?= ON)/i', $current_join_sql, $matches))
			{
				$join_tables_split = explode(' ', $matches[0]);
				$from_tables[] = isset($join_tables_split[1]) ? $join_tables_split[1] : $join_tables_split[0];
			}
			$join_sql .= $current_join_sql;
		}
		$where_sql = $this->_compile_wh('qb_where');

		$sql .= "\n".'DELETE FROM ' . implode(', ',$from_tables) . ' USING '. $table;

		return $sql . $join_sql . $where_sql;
	}

  // ===================================================================================================

	/**
	 * Update rows by joined tables. Use join() to join tables
	 *
	 * @param   string  $table
	 * @param   array   $where
	 * @param   bool    $reset_data - reset query after execution
	 * @param   bool    $sql_string - return sql as string
	 * @return  string|boolean
	 * @author  A H Abid
	 */
	public function update_join($table = '', $update = NULL, $where = array(), $reset_data = TRUE, $sql_string = FALSE)
	{
		// Combine any cached components with the current statements
		$this->_merge_cache();

		// Set Data
		if (count($this->qb_set) == 0 && ! is_null($update))
		{
			$this->set($update);
		}

		if (count($this->qb_set) == 0)
		{
			if ($this->db_debug)
			{
				return $this->display_error('db_must_use_set');
			}
			return NULL;
		}

		if ($table === '')
		{
			if ( ! isset($this->qb_from[0]))
			{
				return ($this->db_debug) ? $this->display_error('db_must_set_table') : FALSE;
			}

			$table = $this->qb_from[0];
		}
		else
		{
			$table = $this->protect_identifiers($table, TRUE, NULL, FALSE);
		}

		if (count($this->qb_join) === 0)
		{
			return ($this->db_debug) ? $this->display_error('db_upd_set_join_syntax') : FALSE;
		}

		if (is_array($where) && count($where) > 0)
		{
			$this->where($where);
		}

		if (count($this->qb_where) === 0)
		{
			return ($this->db_debug) ? $this->display_error('db_upd_must_use_where') : FALSE;
		}

		$sql = $this->_update_join($table, $this->qb_set);
		if ($reset_data)
		{
			$this->_reset_write();
		}

		if ($sql_string === TRUE)
		{
			return $sql;
		}
		else 
		{
			if($this->save_queries === TRUE)
			{
				return $this->query($sql, FALSE, FALSE);
			}
			else
			{
				return $this->simple_query($sql);
			}
		}
	}

	// ---------------------------------------------------------------------------------------------------

	/**
	 * SQL statement for update_join() syntax
	 * 
	 * @param string $table
	 * @param array $values
	 * @return string
	 * @author A H Abid
	 */
	protected function _update_join($table, $values)
	{
		// Prep Values
		foreach ($values as $key => $val)
		{
			$valstr[] = $key.' = '.$val;
		}

		$sql = 'UPDATE '.$table
						." \n".implode("\n", $this->qb_join)
						.' SET '. implode(', ', $valstr)
						.$this->_compile_wh('qb_where')
						.($this->qb_limit ? ' LIMIT '.$this->qb_limit : '');
		return $sql ;
	}

	// ===================================================================================================


}
/* End of file MY_DB_mysqli_driver.php */
/* Location: ./application/database/drivers/mysqli/MY_DB_mysqli_driver.php */