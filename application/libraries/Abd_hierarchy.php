<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Simple class to make heirarchy for things like Navigations, dropdownlist, table, comments etc. 
 * This will work under the condition that you provided your whole data. That is for example if you
 * had a navigation menu database table. you will have to include all the rows.
 * 
 * Requirement
 * -----------
 *    - PHP 5.2.4
 *    - CI  3.0
 * 
 * @package    Codeigniter
 * @subpackage Libraries
 * @category   Extensions
 * @version    1.0
 * @author     A H Abid <a_h_abid@hotmail.com>
 */
class Abd_hierarchy {

    private $_error_label = 'HIERARCHY ERROR :: ';
    
    /**
     * Generate the Hierarchy into single or multi-child dimension.
     *  
     * A single type will return a single dimension array with primary_id
     * and column_name as pair. 
     *
     * A multi type will return multidimension array with each item having a 
     * 'prefix' index for dept level and 'child' index which array of 
     * those items child if exists. Will also add 'child_count' index for
     * getting no. of childs.
     * 
     * Example 1 ::
     * A returned array for $type = 'single' will look something like this:
     *  array(  [1]  => 'Home'
     *          [2]  => 'About'
     *          [3]  => '-> Company'
     *          [5]  => '-> -> History'
     *          [4]  => '-> Mission'
     *          [6]  => 'Contact');
     *  where the key are the primary_id and value are the string item you like to view. 
     *  
     *
     *  Example 2 ::
     *  A returned array for $type = 'multi' will look something like this:
     *  array(  [1] => array(
     *                  'primary_id' => 1,
     *                  'name'       => 'Home',
     *                  'prefix'     => '',
     *                 ),
     *          [2] => array(
     *                  'primary_id'  => 2,
     *                  'name'        => 'About',
     *                  'prefix'      => '',
     *                  'child_count' => 1,
     *                  'child'       => array(
     *                                   'primary_id'  => 3,
     *                                   'name'        => 'Company',
     *                                   'prefix'      => '->',
     *                                   'child_count' => 1,
     *                                   'child'       => array(
     *                                                    'primary_id' => 5,
     *                                                    'name'       => 'Mission',
     *                                                    'prefix'     => '-> ->',
     *                                                   ),
     *                                  ),
     *                 ),
     *  );
     *  Here each item has added the 'prefix' index and 'child','child_count' index when child items exists
     *  
     * @param string $type            - Options 'single'|'multi'. Default 'multi'
     * @param array  $data            - the multidimension array. Array similar to $this->db->get()->result();
     * @param int    $start_parent_id - start number of the parent
     * @param string $primary_id_name - key name for primary_id
     * @param string $parent_id_name  - key name for parent_id
     * @param bool   $show_root       - show the root item name if exists
     * @param bool   $column          - name of column if using 'single' type
     * @param string $prefix          - text to append with every item
     * @param string $child_symbol    - Character for child items
     * @return array
     */
    public function generate($type = '', $data, $start_parent_id, $primary_id_name, $parent_id_name, $show_root = FALSE, $column = '', $prefix = '', $child_symbol = '&rarr; ')
    {
        // 1st to make it prepared for heirarchy
        $data = $this->prepare($data, $primary_id_name, $parent_id_name);
        if(count($data) === 0) return array();
    
        // Now make the hierarchy array
        if($type == '') $type = 'multi';
        switch ($type) {
            case 'single': return $this->make_single($start_parent_id, $data, $column, $show_root, $prefix, $child_symbol);
                           break;
            case 'multi' : return $this->make($start_parent_id, $data, $show_root, $prefix, $child_symbol);
                           break;
                 default : show_error($this->_error_label.'Type defined is incorrect. Your denifed type='.$type);
                           break;
        }
    }


    /**
     * Prerape Data for parent child tree
     *  
     * @param array  $data             - multidimension array where inner items could be an array or object
     * @param string $primary_id_name  - key name of primary_id
      * @param string $parent_id_name   - key name of parent_id
     * @return array
     */
    public function prepare($data, $primary_id_name, $parent_id_name)
    {
        // parameters validation
        if(!is_array($data))             die($this->_error_label.'Provided data must be an array. Given datatype='.gettype($data));
        if(!is_string($primary_id_name)) die($this->_error_label.'Provided primary_id_name must be a string. Given datatype='.gettype($primary_id_name));
        if(!is_string($parent_id_name))  die($this->_error_label.'Provided parent_id_name must be a string. Given datatype='.gettype($parent_id_name));
    
        if(count($data) < 1) return array();
        
        if(is_array($data[0]))
            $new_data = $this->_prepare_array($data,$primary_id_name,$parent_id_name);
        elseif(is_object($data[0]))
            $new_data = $this->_prepare_object($data,$primary_id_name,$parent_id_name);
        else
            show_error($this->_error_label.'Data given is not a valid multi-dimension formated array/object. -- Type='.gettype($data));
    
        return $new_data;
    }
    
    
    /**
     * Prepare data for array type
     * 
     * @param array $data
     * @param string $primary_id
     * @param string $parent_id
     * @return array
     */
    private function _prepare_array($data, $primary_id, $parent_id)
    {
        // Check if those parent and primary key exists
        if( array_key_exists($primary_id, $data[0]) === FALSE )
            show_error($this->_error_label.'Primary_id not found in provided data array. Given primary_id='.$primary_id);
        if( array_key_exists($parent_id, $data[0]) === FALSE )
            show_error($this->_error_label.'Parent_id not found in provided data array. Given parent_id='.$parent_id); 
        
        $new_data = array();
        foreach($data as $key => $items)
        {
            $new_data['items'][$items[$primary_id]] = $items;
            $new_data['parents'][$items[$parent_id]][] = $items[$primary_id];
        }
    
        return $new_data;            
    }
    

    /**
     * Prepare data for object type
     * 
     * @param object $data
     * @param string $primary_id
     * @param string $parent_id
     * @return array
     */
    private function _prepare_object($data, $primary_id, $parent_id)
    {
        // Check if those parent and primary key exists
        if( isset($data[0]->$primary_id) === FALSE )
          show_error($this->_error_label.'Primary_id not found in provided data array. Given primary_id='.$primary_id);
        if( isset($data[0]->$parent_id) === FALSE )
          show_error($this->_error_label.'Parent_id not found in provided data array. Given parent_id='.$parent_id); 
        
        $new_data = array();
        foreach($data as $key => $items)
        {
            $new_data['items'][$items->$primary_id] = json_decode(json_encode($items), true); // Convert to array
            $new_data['parents'][$items->$parent_id][] = $items->$primary_id;
        }
    
        return $new_data;
    }
    

    /**
     * Make a single dimention array with the heirarchy data with a 
     * particular column/field set.
     * 
     * @param int $parent_id       - start with parent_id
     * @param array $prep_data     - array data from the prepare() method
     * @param string column        - array item to set
     * @param bool $show_root      - show root item if exists
     * @param string $prefix       - text to be added with every item.
     * @param string $child_symbol - character to be used as child item(s)
     * @return array
     */
    public function make_single($parent_id, $prep_data, $column, $show_root = FALSE, $prefix = '', $child_symbol = '&rarr; ')
    {
        if(trim($column) == '')
        {
          show_error($this->_error_label.'Column name for single hierarchy not given.');
        }

        $func = (__FUNCTION__);
        $my_hierarchy = array();
    
        if($show_root === TRUE && isset($prep_data['items'][$parent_id][$column]))
        {
          $my_hierarchy[$parent_id] = $prefix . $prep_data['items'][$parent_id][$column];
          $prefix = $child_symbol . $prefix;
        }

        if(isset($prep_data['parents'][$parent_id]))
        {
            foreach ($prep_data['parents'][$parent_id] as $itemId)
            {
                $my_hierarchy[$itemId] = $prefix . $prep_data['items'][$itemId][$column];
                
        // Check if item has child
        if(isset($prep_data['parents'][$itemId]))
                {
                    $my_hierarchy = $my_hierarchy + $this->$func($itemId, $prep_data, $column, FALSE, $child_symbol.$prefix, $child_symbol);
                }
            }
        }
    
        return $my_hierarchy;
    }
    

    /**
     * Make the multidimension hierarchy, where childs are enclosed in 'child' array
     * 
     * @param int $parent_id       - start with parent_id
     * @param array $prep_data     - array data from the prepare() method
     * @param bool $show_root      - show root item if exists
     * @param string $prefix       - text to be added with every item.
     * @param string $child_symbol - character to be used as child item(s)
     * @return array
     */
    public function make($parent_id, $prep_data, $show_root = FALSE, $prefix = '', $child_symbol = '&rarr; ')
    {
        $func = (__FUNCTION__);
        $my_hierarchy = array();
  
        if($show_root === TRUE && isset($prep_data['items'][$parent_id]))
        {
            $root = array();
            $root[$parent_id] = $prep_data['items'][$parent_id];
            $root[$parent_id]['prefix'] = $prefix;
            $prefix = $child_symbol . $prefix;
        }
  
        if(isset($prep_data['parents'][$parent_id]))
        {
            foreach ($prep_data['parents'][$parent_id] as $itemId)
            {
                $my_hierarchy[$itemId] = $prep_data['items'][$itemId];
                $my_hierarchy[$itemId]['prefix'] = $prefix;
              
                // Check if item has child
                if(isset($prep_data['parents'][$itemId]))
                {
                    $my_hierarchy[$itemId]['child'] = $this->$func($itemId, $prep_data, FALSE, $child_symbol . $prefix, $child_symbol);
                    $my_hierarchy[$itemId]['child_count'] = count($my_hierarchy[$itemId]['child']);
                }
            }
        }
      
        if($show_root === TRUE && isset($prep_data['items'][$parent_id]))
        {
            $root[$parent_id]['child'] = $my_hierarchy;
            $my_hierarchy = $root;
        }
  
        return $my_hierarchy;
    }

}
/* End of file Abd_hierarchy.php */
/* Location: ./application/libraries/Abd_hierarchy.php */