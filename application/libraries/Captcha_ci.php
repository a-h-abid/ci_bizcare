<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Simple Library to Manage Capchas
 * 
 * @package     CodeIgniter
 * @subpackage  Library
 * @category    Captcha
 * @author      A H Abid
 * @version     1.0
 * @date		2014-06-10
 */
class Captcha_ci {

	protected $CI;

	public $width				= 130;
	public $height				= 30;
	public $expiration			= 300; // 5 minutes
	public $directory_name		= 'captchas'; // Relative File Path After FCPATH

	protected $captcha;


	function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->helper('captcha');
		$this->CI->load->driver('session');
	}


	/**
	 * Create a new Captcha
	 * @param  array  $options [description]
	 * @return mixed
	 */
	function create($options = array())
	{
		if ($this->captcha !== NULL)
		{
			return $this->captcha;
		}

		// Delete captcha if found in temp session
		$this->_delete_temp_captchas();

		if (count($options) > 0)
		{
			$this->set_options($options);
		}

		$this->captcha = create_captcha(array(
			'img_path'		=> $this->get_filepath(),
			'img_url'		=> $this->get_url(),
			'img_width'		=> $this->width,
			'img_height'	=> $this->height,
			'expiration'	=> $this->expiration,
		));

		// Set to Temp Session
		if ($this->captcha != FALSE && isset($this->captcha['image'])) 
			unset($this->captcha['image']);
		$this->CI->session->set_tempdata('captcha', $this->captcha, $this->expiration);

		return $this->captcha;
	}

	/**
	 * Set Captcha Options
	 */
	function set_options($options)
	{
		foreach ($options as $key => $value)
		{
			if (property_exists($this, $key) && !in_array($key,array('CI','captcha')))
			{
				$this->$key = $value;
			}
		}
	}


	/**
	 * Check if captcha word matches or not
	 * 
	 * @param  string	$word
	 * @return bool
	 */
	public function verify($word)
	{
		$captcha = $this->CI->session->tempdata('captcha');
		if ($captcha == NULL) return FALSE;

		$this->_delete_temp_captchas();

		return strtolower($word) == strtolower($captcha['word']);
	}

	/** 
	 * Get the Full File Path to directory
	 * @return string
	 */
	function get_filepath()
	{
		return 	FCPATH . $this->directory_name . DIRECTORY_SEPARATOR;
	}

	/** 
	 * Get the Full URL Path to directory
	 * @return string
	 */
	function get_url()
	{
		return 	$this->CI->config->base_url($this->directory_name);
	}

	/**
	 * Delete Temp Capchas Files, mainly which are already used
	 * @return bool
	 */
	protected function _delete_temp_captchas()
	{
		$captcha = $this->CI->session->tempdata('captcha');
		if ($captcha == NULL || !isset($captcha['filename'])) return FALSE;

		if ( file_exists( $this->get_filepath(). $captcha['filename'] ))
		{
			return unlink($this->get_filepath(). $captcha['filename']);
		}

		return FALSE;
	}

}
/* End of file Captcha_ci.php */
/* Location: ./application/library/Captcha_ci.php */