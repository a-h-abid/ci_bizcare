<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extend CI_Migration
 *
 * @package		CodeIgniter
 * @subpackage	Library
 * @category	MY_Migration
 * @author		A. H. Abid <a_h_abid@hotmail.com>
 * @link		http://github.com/abdmaster/
 */
class MY_Migration extends CI_Migration {


	/**
	 * Get current version
	 * 
	 * @return string
	 */
	public function get_current()
	{
		return $this->_get_version();
	}

}
/* End of file MY_Migration.php */
/* Location: ./application/libraries/MY_Migration.php */