<?php 

/**
 * Translate text from eng to bengali
 */
class Translate_datetime_bangla {

	protected $en_digits = array('0','1','2','3','4','5','6','7','8','9');
	protected $bn_digits = array('০','১','২','৩','৪','৫','৬','৭','৮','৯');

	protected $en_months_short		= array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	protected $en_months_long		= array('January','February','March','April','May','June','July','August','September','October','November','December');
	protected $bn_months_short		= array('জান','ফেব্রু','মার্চ','এপ','মে','জুন','জুল','অগ','সেপ্ট','অক্টো','নভ','ডিসেম');
	protected $bn_months_long		= array('জানুয়ারী','ফেব্রুয়ারী','মার্চ','এপ্রিল','মে','জুন','জুলাই','অগাস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');

	protected $en_weekdays_short	= array('Sat','Sun','Mon','Tue','Wed','Thu','Fri');
	protected $en_weekdays_long		= array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
	protected $bn_weekdays_short	= array('শনি','রবি','সোম','মঙ্গল','বুধ','বৃহ','শুক্র');
	protected $bn_weekdays_long		= array('শনিবার','রবিবার','সোমবার','মঙ্গলবার','বুধবার','বৃহস্পতিবার','শুক্রবার');


	/**
	 * Returns translated string.
	 * 
	 * @param  [type] $string [description]
	 * @return [type]         [description]
	 */
	public function convert($string)
	{
		$string = str_replace($this->en_digits, $this->bn_digits, $string);
		$string = str_replace($this->en_months_short, $this->bn_months_short, $string);
		$string = str_replace($this->en_months_long, $this->bn_months_long, $string);
		$string = str_replace($this->en_weekdays_short, $this->bn_weekdays_short, $string);
		$string = str_replace($this->en_weekdays_long, $this->bn_weekdays_long, $string);

		return $string;
	}

}