<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extend CI_Form_validation
 *
 * @package		CodeIgniter
 * @subpackage	Library
 * @category	MY_Form_validation
 * @author		A. H. Abid <a_h_abid@hotmail.com>
 * @link		http://github.com/abdmaster/
 */
class MY_Form_validation extends CI_Form_validation {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}
	

	/**
	 * Override Executes the Validation routines
	 *
	 * Added Option to check for custom error messages by fields
	 *
	 * @param  array
	 * @param  array
	 * @param  mixed
	 * @param  int
	 * @return  mixed
	 * @author A H Abid <a_h_abid@hotmail.com>
	 */
	protected function _execute($row, $rules, $postdata = NULL, $cycles = 0)
	{
		// If the $_POST data is an array we will run a recursive call
		if (is_array($postdata))
		{
			foreach ($postdata as $key => $val)
			{
				$this->_execute($row, $rules, $val, $key);
			}

			return;
		}

		// If the field is blank, but NOT required, no further tests are necessary
		$callback = FALSE;

		// Check for any required fields
		$required_extra = preg_match("/required_[^|]+/", implode('|', $rules), $req_match);
		if ( ( (!in_array('required', $rules)) && $required_extra === 0 ) && ($postdata === NULL OR $postdata === ''))
		{
			// Before we bail out, does the rule contain a callback?
			if (preg_match('/(callback_\w+(\[.*?\])?)/', implode(' ', $rules), $match))
			{
				$callback = TRUE;
				$rules = array(1 => $match[1]);
			}
			else
			{
				return;
			}
		}

		// Isset Test. Typically this rule will only apply to checkboxes.
		if (($postdata === NULL OR $postdata === '') && $callback === FALSE)
		{
			if ( in_array('isset', $rules, TRUE) || in_array('required',$rules) )
			{
				// Set the message type
				$type = in_array('required', $rules) ? 'required' : 'isset';

				// Check if a custom message defined, else use default one
				if (isset($this->_field_data[$row['field']]['errors'][$type]))
				{
					$line = $this->_field_data[$row['field']]['errors'][$type];
				}
				elseif (isset($this->_error_messages[$type]))
				{
					$line = $this->_error_messages[$type];
				}
				elseif (FALSE === ($line = $this->CI->lang->line('form_validation_'.$type)))
				{
					$line = 'The field was not set';
				}

				// Build the error message
				$message = $this->_build_error_msg($line, $this->_translate_fieldname($row['label']));

				// Save the error message
				$this->_field_data[$row['field']]['error'] = $message;

				if ( ! isset($this->_error_array[$row['field']]))
				{
					$this->_error_array[$row['field']] = $message;
				}

				return;
			}
			elseif($required_extra === 1)
			{
				// Extract Params if any
				$param = FALSE;
				if (preg_match('/(.*?)\[(.*)\]/', $req_match[0], $req_match_params))
				{
					$req_rule  = $req_match_params[1];
					$req_param = $req_match_params[2];
				}
			
				if(method_exists($this, $req_rule))
				{
					if($this->{$req_rule}($postdata, $req_param) === FALSE)
					{
						// Check if a custom message defined, else use default one
						if (isset($this->_field_data[$row['field']]['errors'][$req_rule]))
						{
							$line = $this->_field_data[$row['field']]['errors'][$req_rule];
						}
						elseif (isset($this->_error_messages[$req_rule]))
						{
							$line = $this->_error_messages[$req_rule];
						}
						elseif (FALSE === ($line = $this->CI->lang->line('form_validation_'.$req_rule))
						// DEPRECATED support for non-prefixed keys
						&& FALSE === ($line = $this->CI->lang->line($req_rule, FALSE)))
						{
							$line = 'The field was not set';
						}            

						// Get label if exists
						if($req_rule == 'required_when')
						{
							$label_field = explode('=', $req_param);
							$label_field = $label_field[0];
						}
						else
						{
							$label_field = $req_param;
						}
						$label = (isset($this->_field_data[$label_field]['label']))
									? $this->_field_data[$label_field]['label'] : $label_field;

						// Build the error message
						$message = $this->_build_error_msg($line, $this->_translate_fieldname($row['label']),$label);

						// Save the error message
						$this->_field_data[$row['field']]['error'] = $message;

						if ( ! isset($this->_error_array[$row['field']]))
						{
							$this->_error_array[$row['field']] = $message;
						}
					}
				} // endif method_exists()
			
				return ;
			}

			return ;
		}

		// --------------------------------------------------------------------

		// Cycle through each rule and run it
		foreach ($rules as $rule)
		{
			$_in_array = FALSE;
			
			// We set the $postdata variable with the current data in our master array so that
			// each cycle of the loop is dealing with the processed data from the last cycle
			if ($row['is_array'] === TRUE && is_array($this->_field_data[$row['field']]['postdata']))
			{
				// We shouldn't need this safety, but just in case there isn't an array index
				// associated with this cycle we'll bail out
				if ( ! isset($this->_field_data[$row['field']]['postdata'][$cycles]))
				{
					continue;
				}

				$postdata = $this->_field_data[$row['field']]['postdata'][$cycles];
				$_in_array = TRUE;
			}
			else
			{
				// If we get an array field, but it's not expected - then it is most likely
				// somebody messing with the form on the client side, so we'll just consider
				// it an empty field
				$postdata = is_array($this->_field_data[$row['field']]['postdata'])
					? NULL
					: $this->_field_data[$row['field']]['postdata'];
			}

			// Is the rule a callback?
			$callback = FALSE;
			if (strpos($rule, 'callback_') === 0)
			{
				$rule = substr($rule, 9);
				$callback = TRUE;
			}

			// Strip the parameter (if exists) from the rule
			// Rules can contain a parameter: max_length[5]
			$param = FALSE;
			if (preg_match('/(.*?)\[(.*)\]/', $rule, $match))
			{
				$rule = $match[1];
				$param = $match[2];
			}

			// Call the function that corresponds to the rule
			if ($callback === TRUE)
			{
				if ( ! method_exists($this->CI, $rule))
				{
					log_message('debug', 'Unable to find callback validation rule: '.$rule);
					$result = FALSE;
				}
				else
				{
					// Run the function and grab the result
					$result = $this->CI->$rule($postdata, $param);
				}

				// Re-assign the result to the master data array
				if ($_in_array === TRUE)
				{
					$this->_field_data[$row['field']]['postdata'][$cycles] = is_bool($result) ? $postdata : $result;
				}
				else
				{
					$this->_field_data[$row['field']]['postdata'] = is_bool($result) ? $postdata : $result;
				}

				// If the field isn't required and we just processed a callback we'll move on...
				if ( ( !in_array('required', $rules, TRUE) || $required_extra === 0) && $result !== FALSE)
				{
					continue;
				}
			}
			elseif ( ! method_exists($this, $rule))
			{
				// If our own wrapper function doesn't exist we see if a native PHP function does.
				// Users can use any native PHP function call that has one param.
				if (function_exists($rule))
				{
					$result = ($param !== FALSE) ? $rule($postdata, $param) : $rule($postdata);

					if ($_in_array === TRUE)
					{
						$this->_field_data[$row['field']]['postdata'][$cycles] = is_bool($result) ? $postdata : $result;
					}
					else
					{
						$this->_field_data[$row['field']]['postdata'] = is_bool($result) ? $postdata : $result;
					}
				}
				else
				{
					log_message('debug', 'Unable to find validation rule: '.$rule);
					$result = FALSE;
				}
			}
			else
			{
				$result = $this->$rule($postdata, $param);

				if ($_in_array === TRUE)
				{
					$this->_field_data[$row['field']]['postdata'][$cycles] = is_bool($result) ? $postdata : $result;
				}
				else
				{
					$this->_field_data[$row['field']]['postdata'] = is_bool($result) ? $postdata : $result;
				}
			}
		
			// Did the rule test negatively? If so, grab the error.
			if ($result === FALSE)
			{
				// Check if a custom message defined, else use default one
				if(isset($this->_field_data[$row['field']]['errors'][$rule]))
				{
					$line = $this->_field_data[$row['field']]['errors'][$rule];
				}
				elseif ( ! isset($this->_error_messages[$rule]))
				{
					$line = $this->CI->lang->line('form_validation_'.$rule);
					if($line === FALSE)
					{
						$line = 'Unable to access an error message corresponding to your field name.';
					} 
				}
				else
				{
					$line = $this->_error_messages[$rule];
				}

				// Is the parameter we are inserting into the error message the name
				// of another field? If so we need to grab its "field label"
				if (isset($this->_field_data[$param], $this->_field_data[$param]['label']))
				{
					$param = $this->_translate_fieldname($this->_field_data[$param]['label']);
				}

				// Build the error message
				$message = $this->_build_error_msg($line, $this->_translate_fieldname($row['label']), $param);

				// Save the error message
				$this->_field_data[$row['field']]['error'] = $message;

				if ( ! isset($this->_error_array[$row['field']]))
				{
					$this->_error_array[$row['field']] = $message;
				}

				return;
			}
		}
	}


	// =============================================================================================================
	/**
	 * Require a field when another field meets a condition
	 *
	 * @param string $str
	 * @param string $field_condition
	 * @return bool
	 * @author A H Abid <a_h_abid@hotmail.com>
	 */
	public function required_when($str, $field_condition)
	{
		list($field, $equal_to) = explode('=', $field_condition);

		// Check operator
		$operator = '=';
		if(substr($field, -1) == '!')
		{
			$field = substr($field, 0, strlen($field));
			$operator = '!=';
		}

		// No need to process if the field not exist
		if(!isset($this->_field_data[$field])) return TRUE;

		// Get field data and if (not) equal, IGNORE
		if(  ($operator == '=' && $this->_field_data[$field]['postdata'] != $equal_to)
			|| ($operator == '!=' && $this->_field_data[$field]['postdata'] == $equal_to)
			) return TRUE;

		// Now check if the main field is set
		return ($str != '');
	}

	/**
	 * Require a field when another field is set
	 *
	 * @param string $str
	 * @param string $field
	 * @return bool
	 * @author A H Abid <a_h_abid@hotmail.com>
	 */
	public function required_if_set($str, $field)
	{
		// No need to process if the field not exist
		if(!isset($this->_field_data[$field])) return TRUE;

		// if field blank, IGNORE
		if($this->_field_data[$field]['postdata'] == '') return TRUE;

		// Now check if the main field is set
		return ($str != '');
	}


	/**
	 * Require a field when another field is null
	 *
	 * @param string $str
	 * @param string $field
	 * @return bool
	 * @author A H Abid <a_h_abid@hotmail.com>
	 */
	public function required_if_null($str, $field)
	{
		// If field not exists and main field is empty
		if(!isset($this->_field_data[$field]) && $str == '') return FALSE;

		// if field set & is not blank, FALSE
		if(isset($this->_field_data[$field]) && $this->_field_data[$field]['postdata'] == '' && ($str == '')) return FALSE;

		// Now check if the main field is set
		return ($str != '');
	}


	/**
	 * Check if string is lowercase or not
	 * 
	 * @param string $str
	 * @return bool
	 * @author A H Abid
	 */
	public function is_lowercase($str)
	{
		// Return string is lowercase or not
		if ( !ctype_lower($str) )
		{
			$this->CI->form_validation->set_message('is_lowercase', 'Characters in %s field must be in lower case.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


	/**
	 * Check if string is uppercase or not
	 * 
	 * @param string $str
	 * @return bool
	 * @author A H Abid
	 */
	public function is_uppercase($str)
	{
		// Return string is lowercase or not
		if ( !ctype_upper($str) )
		{
			$this->CI->form_validation->set_message('is_uppercase', 'Characters in %s field must be in upper case.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	/**
	 * Check for Valid full name, accepts a-z, A-Z & 'spaces'
	 */
	public function valid_fullname($str)
	{
		// Checks for alphabets with spaces and dots allowed
		if ( !preg_match('/^[a-zA-Z][a-zA-Z \.]+$/',$str) )
		{
			$this->CI->form_validation->set_message('valid_fullname', '%s field can only have alphabets, spaces & dots.');
			return FALSE;
		}
		else return TRUE;		
	}

	/**
	 * Check for valid username
	 */
	public function valid_username($str)
	{
		// Check for username validation with alphanumeric and underscore
		if (!preg_match('/^[a-z0-9_]+$/',$str))
		{
			$this->CI->form_validation->set_message('valid_username', '%s field can only have lowercase letters, numbers and underscores ');
			return FALSE;
		}
		else return TRUE;
	}


	/**
	 * Check for valid Mysql Date Format
	 */
	public function valid_mysql_date($str)
	{
		$mysql_date_format = '/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))$/';
		
		if( !preg_match($mysql_date_format,$str) )
		{
			$this->CI->form_validation->set_message('valid_mysql_date', '%s is not in valid Mysql Date Format.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


	/**
	 * Check for valid Mysql DateTime Format
	 */
	public function valid_mysql_datetime($str)
	{
		$mysql_datetime_format = '/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/';
		
		if( !preg_match($mysql_datetime_format,$str) )
		{
			$this->CI->form_validation->set_message('valid_mysql_datetime', '%s is not in valid Mysql DateTime Format.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


	/**
	 * Validate URL
	 */
	public function valid_url($str)
	{
		$pattern = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
		return (preg_match($pattern, $str) === 0) ? FALSE : TRUE;
	}

	/**
	* Validate Password pattern
	*/
	public function valid_password($str)
	{
	$pattern = '/^((?=.*\d)(?=.*[a-zA-Z]).+)$/';
	return (preg_match($pattern, $str) === 1);
	}


	/**
	* Validate Strong Password pattern
	*/
	public function valid_strong_password($str)
	{
	$pattern = '/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@!#$_\-%]).+)$/';
	return (preg_match($pattern, $str) === 1);
	}

	/**
	 * Match one field to another
	 * This is the method from version 2.1.3
	 *
	 * @access	public
	 * @param	string
	 * @param	field
	 * @return	bool
	 */
	public function matches_old($str, $field)
	{
		//$this->CI->form_validation->set_message('matches_old', 'The %s field is not mathced.');		
		if ( ! isset($_POST[$field]))	return FALSE;
		return ($str === $_POST[$field]);
	}


	/**
	 * Overriding is_unique to validate with model/tabe, field and also checks if user posted
	 * with primary key field or not 
	 *
	 * @param string $str
	 * @param string $params - {model/table name}.fieldname.{ignore for field}
	 * @return boolean
	 */
	public function is_unique($str, $params)
	{
		list($model, $field, $except_key) = explode(".", $params);

		if( file_exists(APPPATH.'models'.DS.ucfirst(str_replace('/', DS, $model)).'.php' ))
		{
			$table = $this->CI->model($model)->get_table_name();
		}
		else // Hopefully it means its a table
		{
			$table = $model;
		}

		$this->CI->db->from($table);
		$this->CI->db->where($field, $str);
		
		$post = NULL;
		if(isset($this->_field_data[$except_key]['postdata']))
			$post = $this->_field_data[$except_key]['postdata'];

		if( $post !== NULL)
			$this->CI->db->where($except_key.' !=', $post);

		$query = $this->CI->db->get();
		return ($query->num_rows() > 0) ? FALSE : TRUE;
	}


	/**
	 * Validate date and/or time format. 
	 *
	 * Note: Required 5.3 
	 * 
	 * @param  [type] $str    [description]
	 * @param  [type] $format [description]
	 * @return [type]         [description]
	 */
	public function valid_datetime_format($str, $format)
	{
		$validated = date_parse_from_format($format,$str);

		if($validated['error_count'] > 0)
		{
			$this->CI->form_validation->set_message('valid_datetime_format', 'Format in %s is not valid.');
			return FALSE;
		}
		else
			return TRUE;
	}


	/**
	 * Validate if string is in these values
	 * 
	 * @param  string  $str    [description]
	 * @param  string  $values [description]
	 * @return boolean         [description]
	 */
	public function is_in($str, $values)
	{
		$values = explode(',', $values);
		return in_array($str, $values);
	}

	/**
	 * Validate if string of set (comma "," separated) is in these values
	 * 
	 * @param  string  $str    [description]
	 * @param  string  $values [description]
	 * @return boolean         [description]
	 */
	public function is_in_set($str, $values)
	{
		$input = explode(',', $str);
		$haystack = explode(',', $values);
		return arrays_in_array($input, $haystack);
	}

	/**
	 * Validate if valid youtube link
	 * @param  string  $str
	 * @return boolean
	 */
	public function valid_youtube_link($str)
	{
		$rx = '~
			    ^(?:https?://)?              # Optional protocol
			     (?:www\.)?                  # Optional subdomain
			     (?:youtube\.com|youtu\.be)  # Mandatory domain name
			     /watch\?v=([^&]+)           # URI with video id as capture group 1
			     ~x';

		return preg_match($rx, $str) ? TRUE : FALSE;
	}

	/**
	 * Check if provided captcha valid
	 * @param  string $str [description]
	 * @return boolean
	 */
	public function valid_captcha($str)
	{
		// Validate if the libary is set to CI object, else fail
		if (!isset($this->CI->captcha_ci))
			return FALSE;

		return $this->CI->captcha_ci->verify($str);
	}

}
/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */ 	