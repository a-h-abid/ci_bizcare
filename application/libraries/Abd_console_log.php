<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Browser console log library. Currently uses FirePHP & ChromePHP.
 * Loads based on browser.
 * 
 * @package     CodeIgniter
 * @subpackage  Library
 * @category    Console Log
 * @author      A H Abid
 * @version     1.0
 */
class Abd_console_log {
  
    protected $CI = NULL;
  
    /**
     * Febug tools parameters
     */
    protected $_browser = NULL;
    protected $_debug_name = NULL;
    protected $_debug_obj = NULL;
  
    /**
     * Show filenames and line no. on label
     */
    protected $_show_label_file_line = TRUE;


    public function __construct()
    {
        $this->CI =& get_instance();
    
        if (CI_DEBUG === FALSE)
        {
        	return FALSE;
        }

        // Load user_agent library
        if(!class_exists('CI_User_agent'))
            $this->CI->load->library('user_agent');
    
        // Load debug tool based on browser.
        $this->_browser = $this->CI->agent->browser();
        switch ($this->_browser)
        {
            case 'Chrome':
                require_once( APPPATH . 'third_party/chromephp/chromephp.php' );
                $this->_debug_obj = ChromePhp::getInstance();
                $this->_debug_name = 'ChromePhp';
                log_message('debug', 'Loading Debug Lib  "ChromePhp" ');
                break;
              
            case 'Firefox':
                require_once ( APPPATH . 'third_party/firephp/FirePHPCore-0.3.2/lib/FirePHP.class.php');
                $this->_debug_obj = FirePHP::getInstance(TRUE);
                $this->_debug_name = 'FirePHP';
                log_message('debug', 'Loading Debug Lib  "FirePHP" ');
                break;
      
            default:
                log_message('debug', 'Console_log lib does not support browser "'.$this->_browser.'" ');
                return FALSE;
                break;
        }
    
    }

    /**
     * Check if console log allowed for current browser
     *
     * @return boolean
     */
    public function is_allowed()
    {
        return ( $this->_debug_obj !== NULL && CI_DEBUG === TRUE );
    }

    /**
     * Returns the filename and line no from where the log is called.
     * 
     * @return A H Abid
     */
    protected function _get_file_line_no()
    {
        if($this->_show_label_file_line === FALSE) return '';
    
        $backtrace = debug_backtrace(FALSE, 2);
        return (isset($backtrace[1]['file']))
                ? str_replace( SITEROOT  ,'',$backtrace[1]['file']) . ':' . $backtrace[1]['line'] .' ' : '';
    }


    /**
     * Log with console.log
     * 
     * @param mixed $value - any type of data you want.
     * @param string $label - text used for indication
     * @return void|boolean
     */
    public function log($value, $label = '')
    {
        if($this->is_allowed() === FALSE) return FALSE;
        
        $label = $this->_get_file_line_no() . $label;
    
        switch ($this->_browser)
        {
            case 'Chrome':
                return $this->_debug_obj->log($label, $value);
                break;
            
            case 'Firefox':
                return $this->_debug_obj->log($value, $label);
                break;
      
            default:
                return FALSE;
                break;
        }
    }

    /**
     * Log with console.warn
     * 
     * @param mixed $value - any type of data you want.
     * @param string $label - text used for indication
     * @return void|boolean
     */
    public function warn($value, $label = '')
    {
        if($this->is_allowed() === FALSE) return FALSE;
    
        $label = $this->_get_file_line_no() . $label;
    
        switch ($this->_browser)
        {
            case 'Chrome':
                return $this->_debug_obj->warn($label, $value);
                break;
            
            case 'Firefox':
                return $this->_debug_obj->warn($value, $label);
                break;
            
            default:
                return FALSE;
                break;
        }
    }

    /**
     * Log with console.error
     * 
     * @param mixed $value - any type of data you want.
     * @param string $label - text used for indication
     * @return void|boolean
     */
    public function error($value, $label = '')
    {
        if($this->is_allowed() === FALSE) return FALSE;
  
        $label = $this->_get_file_line_no() . $label;
  
        switch ($this->_browser)
        {
            case 'Chrome':
                return $this->_debug_obj->error($label, $value);
                break;
            
            case 'Firefox':
                return $this->_debug_obj->error($value, $label);
                break;
      
            default:
                return FALSE;
                break;
        }
    }

    /**
     * Log with console.info
     * 
     * @param mixed $value - any type of data you want.
     * @param string $label - text used for indication
     * @return void|boolean
     */
    public function info($value, $label = '')
    {
        if($this->is_allowed() === FALSE) return FALSE;
    
        $label = $this->_get_file_line_no() . $label;
    
        switch ($this->_browser)
        {
            case 'Chrome':
                return $this->_debug_obj->info($label, $value);
                break;
            
            case 'Firefox':
                return $this->_debug_obj->info($value, $label);
                break;
      
            default:
                return FALSE;
                break;
        }
    }

    /**
     * Start Grouping next log entries
     * 
     * @param string $label - text used for indication
     * @param string $collapse - start group in collapse
     * @param string $hexcolor - text color for the gorup label
     * @return void|boolean
     */
    public function group($label, $collapse = FALSE, $hexcolor = '#666666')
    {
        if($this->is_allowed() === FALSE) return FALSE;
    
        switch ($this->_browser)
        {
            case 'Chrome':
                return ($collapse === TRUE) ? $this->_debug_obj->groupCollapsed($label) : $this->_debug_obj->group($label);
                break;
            
            case 'Firefox':
                if(!preg_match('/^[#][1-9a-fA-F]{3,6}$/', $hexcolor)) $hexcolor = '#666666';
                return $this->_debug_obj->group($label, array('Collapsed' => $collapse, 'Color' => $hexcolor));
                break;
      
            default:
                return FALSE;
                break;
        }
    }

    /**
     * End current group
     * 
     * @return void
     */
    public function group_end()
    {
        if($this->is_allowed() === FALSE) return FALSE;
    
        switch ($this->_browser)
        {
            case 'Chrome':
                return $this->_debug_obj->groupEnd();
                break;
            
            case 'Firefox':
                return $this->_debug_obj->groupEnd();
                break;
      
            default:
                return FALSE;
                break;
        }
    }

}
/* End of file Abd_console_log.php */
/* Location: ./application/library/Abd_console_log.php */