module.exports = function(grunt) {

	// General Compatibility and
	// Absolute Nessessary Libs
	var js_general = [
		'./www/assets/libs/modernizr/modernizr-2.8.2.min.js',
		'./www/assets/libs/jquery/dist/jquery.min.js',
		'./www/assets/libs/jquery.bc/bc.js',
		'./www/assets/libs/jquery.bc.mobile/jquery.bc.mobile.min.js',
		'./www/assets/libs/bootstrap/v3.2.0/dist/js/bootstrap.min.js',
		'./www/assets/libs/jquery.easing/jquery.easing.1.3.min.js'
	];

	// Common Libs Across Apps
	var common_js_libs = js_general.concat([
		'./www/assets/libs/fancybox/source/jquery.fancybox.pack.js'
	]);

	// Custom Common JS for all Apps
	var common_custom_js = common_js_libs.concat([]);

	// -------------------------------------------------------------------------------------------------

	// Frontend Libs
	var frontend_js_libs = common_custom_js.concat([]);

	// Frontend Custom JS
	var frontend_custom_js = frontend_js_libs.concat([
        './www/assets/app/public/js/custom-public.js',
		'./www/assets/app/public/js/events/conf-expo-2017.js'
	]);

	// All Frontend JS Packed
	var frontend_js_pack = frontend_custom_js;

	// -------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------

	//Initializing the configuration object
	grunt.initConfig({

		// Task configuration
		clean : {
			development : ['./www/assets/compiled/css/*.css','./www/assets/compiled/js/*.js']
		},
		less: {
			frontend : {
				options: {
					compress: false
				},
				files: {
					"./www/assets/compiled/css/frontend.css" : "./www/assets/app/public/less/frontend.less"
				}
			}
		},
		cssmin: {
			options : {
				keepSpecialComments : 0
			},
			frontend: {
				expand: false,
				src: ['www/assets/compiled/css/frontend.css'],
				dest: 'www/assets/compiled/css/frontend.min.css'
			}
		},
		concat : {
			options: {
				separator: ';',
			},
			frontend_js : {
				src : frontend_js_pack,
				dest: './www/assets/compiled/js/frontend.js',
			}
		},
		uglify: {
			frontend : {
				options: {
					mangle: false,  // True if you want the names of your functions and variables changed
					compress : true,
					preserveComments : false
				},
				files: {
					'./www/assets/compiled/js/frontend.min.js': ['./www/assets/compiled/js/frontend.js']
				}
			}
		}
	});

	// Plugin loading
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Task definition
	grunt.registerTask('default', ['clean','less','cssmin','concat','uglify']);
	grunt.registerTask('frontend_css', ['less:frontend','cssmin:frontend']);
	grunt.registerTask('frontend_js', ['concat:frontend_js','uglify:frontend']);

};